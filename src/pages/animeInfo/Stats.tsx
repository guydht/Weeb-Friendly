import React from "react";
import ListGroup from "react-bootstrap/ListGroup";
import ProgressBar from "react-bootstrap/ProgressBar";
import { loadingComponent } from "../../components/LoadingComponent";
import MALUtils from "../../utils/MAL";
import { AnimeInfoProps } from "../AnimeInfo";

const statusToVariantMapping: Record<string, string> = {
	"Plan to Watch": "info",
	Dropped: "danger",
	Completed: "success",
	"On-Hold": "warning",
	Watching: "primary"
},
	statuses = Object.keys(statusToVariantMapping);

export default loadingComponent(props => {
	return (
		<div className="mt-3">
			<ListGroup>
				<h3>
					Members Stats:
				</h3>

				<div style={{ float: "right" }}>
					<ProgressBar style={{ height: "2rem" }}>
						{
							statuses.map(status => {
								const value = parseFloat((props.stats as any)?.[status]?.match(/[0-9]/g)?.join("")),
									percentage = value / parseFloat(props.stats?.Total?.match(/[0-9]/g)?.join("") ?? "") * 100;
								return <ProgressBar title={status} key={status} variant={statusToVariantMapping[status]} now={percentage} label={`${status}`} />;
							})
						}
					</ProgressBar>
				</div>

				{
					statuses.map(status => (
						<ListGroup.Item key={status}>
							<b>{status}</b>: {(props.stats as any)?.[status]}
						</ListGroup.Item>
					))
				}

			</ListGroup>

			<ListGroup>
				<h3>
					Scores
				</h3>

				{
					Object.entries(props.stats?.scores ?? {}).map(([key, value]) => (
						<ListGroup.Item key={key}>
							<span style={{ position: "absolute", transform: "translateY(-50%)", top: "50%", left: "0.3rem" }}>{key}</span>
							<ProgressBar now={parseFloat(value)} label={value} striped />
						</ListGroup.Item>
					))
				}

			</ListGroup>
		</div>
	);
}, async (props: AnimeInfoProps) => ({ stats: await MALUtils.animeStats(props.anime as any) }));
