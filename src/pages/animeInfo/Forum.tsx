import React, { Component } from "react";
import Accordion from "react-bootstrap/Accordion";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import Jumbotron from "react-bootstrap/Jumbotron";
import Modal from "react-bootstrap/Modal";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Pagination from "react-bootstrap/Pagination";
import Row from "react-bootstrap/Row";
import Spinner from "react-bootstrap/Spinner";
import Tooltip from "react-bootstrap/Tooltip";
//@ts-ignore
import { LazyLoadImage } from "react-lazy-load-image-component";
import PageTransition from "../../components/PageTransition";
import styles from "../../css/pages/Forum.module.css";
import MALUtils, { ForumEntry, ForumTopic } from "../../utils/MAL";
import { hasInternet } from "../../utils/OutsourcedGeneral";
import { AnimeInfoProps } from "../AnimeInfo";
import { displayToast } from "../global/ToastMessages";
import Home from "../Home";

export class DisplayForumEntry extends Component<{
	forumEntry: ForumEntry;
}> {
	render() {
		return this.props.forumEntry.messages.map(message => {
			return (
				<Modal.Dialog id={message.id.toString()} className="my-1" size="xl" key={message.id}>
					<Accordion as={Modal.Header} className="mx-4">
						<Row className="py-2"><Col>{message.user.name}</Col><Col>{message.time.toLocaleString()}</Col></Row>
						<Container className="d-flex">
							<Row>
								<Col>Joined: {message.user.joined}</Col>
								<Col>Status: {message.user.status}</Col>
								<Col>Posts: {message.user.posts}</Col>
							</Row>
							<Row className="ml-auto">
								<LazyLoadImage className="maluserimg" src={message.user.imageURL} />
							</Row>
						</Container>
					</Accordion>
					<Modal.Body dangerouslySetInnerHTML={{ __html: message.messageHTML }}>
					</Modal.Body>
				</Modal.Dialog>
			);
		});
	}
}

export default class Forum extends Component<AnimeInfoProps> {

	state: {
		topics?: ForumTopic[], forumEntry?: ForumEntry, loading: boolean, currentPageNumber: number, totalPages: number;
	} = {
			loading: true,
			currentPageNumber: 0,
			totalPages: 0
		};

	transitionController = React.createRef<PageTransition>();
	savedPages: Map<number, ForumTopic[]> = new Map();

	componentDidMount() {
		this.loadPageNumber(0);
	}

	loadPageNumber(pageNumber: number) {
		if (pageNumber < 0) return;
		if (this.savedPages.has(pageNumber))
			return this.setState({ topics: this.savedPages.get(pageNumber), loading: false, currentPageNumber: pageNumber });;
		this.setState({ loading: true });
		MALUtils.animeForum(this.props.anime as any, pageNumber).then(([topics, numOfPages]) => {
			if (topics && topics.length) {
				this.savedPages.set(pageNumber, topics);
				this.setState({ topics, loading: false, totalPages: numOfPages, currentPageNumber: pageNumber });
			}
			else throw new Error("MyAnimeList didn't respond as expected!");
		}).catch((e) => {
			displayToast({
				body: `Error Message: ${e.toString()}`,
				title: "Couldn't load forum topics from MyAnimeList. Something went wrong!"
			});
			this.setState({ loading: false });
		});
	}

	render() {
		return (
			<PageTransition style={{ overflow: "visible" }} ref={this.transitionController} className="mt-5" >
				<Container>
					{this.state.totalPages > 1 &&
						<Pagination >
							<Pagination.First onClick={() => this.loadPageNumber(1)} />
							<Pagination.Prev onClick={() => this.loadPageNumber(this.state.currentPageNumber - 1)} />
							{
								Array.from(new Array(this.state.totalPages)).map((_, i) => {
									return (
										<Pagination.Item
											key={i}
											onClick={() => this.loadPageNumber(i)}
											active={this.state.currentPageNumber === i}>
											{i + 1}
										</Pagination.Item>
									);
								})
							}
							<Pagination.Next onClick={() => this.loadPageNumber(this.state.currentPageNumber + 1)} />
							<Pagination.Last onClick={() => this.loadPageNumber(this.state.totalPages)} />
						</Pagination>
					}
					{
						this.state.topics && this.state.topics.map(topic => {
							return (
								<Modal.Dialog className="my-1" size="xl" key={topic.url}>
									<div className={styles.modalWrapper}>
										<Modal.Header onClick={() => this.loadForum(topic)}
											className={"ml-3 my-auto " + styles.modalHeader}>
											{topic.title}
										</Modal.Header>
										<Modal.Body className={styles.modalBody}>
											<Row>
												<Col>Replies: {topic.replies}</Col>
												<Col>Author: {topic.author}</Col>
												<Col>Posted: {topic.posted}</Col>
											</Row>
										</Modal.Body>
									</div>
								</Modal.Dialog>
							);
						})
					}
					{
						hasInternet() && this.state.loading ? (
							<div className={styles.loadingContainer} style={{ position: this.state.topics ? "absolute" : "initial" }}>
								<Modal.Dialog>
									<Modal.Header>
										Loading....
									</Modal.Header>
									<Modal.Body>
										<Spinner animation="border" />
									</Modal.Body>
								</Modal.Dialog>
							</div>
						) : !hasInternet() && Home.noInternetComponent("Forums")
					}
				</Container>
				<Container>
					{this.state.forumEntry && (
						<Jumbotron>
							<OverlayTrigger trigger={["hover", "focus"]} overlay={<Tooltip id="placement-right">Go Back</Tooltip>} placement="right">
								<span style={{ position: "absolute", top: "1.75rem", cursor: "pointer" }}
									className="carousel-control-prev-icon"
									onClick={() => this.transitionController.current!.moveTo(0)} />
							</OverlayTrigger>
							<h1>
								{this.state.forumEntry.title}
							</h1>
							<DisplayForumEntry forumEntry={this.state.forumEntry} />
						</Jumbotron>
					)}
				</Container>
				<span></span>
			</PageTransition >
		);
	}
	forumEntries: Map<ForumTopic, ForumEntry> = new Map();
	loadForum(topic: ForumTopic) {
		if (this.forumEntries.has(topic)) {
			this.setState({
				forumEntry: this.forumEntries.get(topic),
				loading: false
			});
			if (this.transitionController.current)
				this.transitionController.current.moveTo(1);
			return;
		}
		this.setState({
			loading: true
		});
		MALUtils.forumEntry(topic).then(forumEntry => {
			this.forumEntries.set(topic, forumEntry);
			this.setState({
				forumEntry,
				loading: false
			});
			if (this.transitionController.current)
				this.transitionController.current.moveTo(1);
		}).catch(e => {
			displayToast({
				body: `Error Message: ${e.toString()}`,
				title: "Couldn't load forum topics from MyAnimeList. Something went wrong!"
			});
			this.setState({ loading: false });
		});
	}
}

