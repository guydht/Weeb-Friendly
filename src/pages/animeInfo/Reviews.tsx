import React, { Component } from "react";
import Col from "react-bootstrap/Col";
import Modal from "react-bootstrap/Modal";
import Row from "react-bootstrap/Row";
import { LazyLoadImage } from "react-lazy-load-image-component";
import AnimeEntry from "../../classes/AnimeEntry";
import { loadingComponent } from "../../components/LoadingComponent";
import MALUtils from "../../utils/MAL";
import { AnimeInfoProps } from "../AnimeInfo";
import styles from "../../css/pages/Reviews.module.css";

type ThenArg<T> = T extends PromiseLike<infer U> ? U : T;
type ReviewData = Exclude<ThenArg<ReturnType<typeof MALUtils.animeReviews>>, undefined>[0];

class DisplayReview extends Component<{ review: ReviewData, anime: AnimeInfoProps["anime"], active?: boolean; } & React.ComponentPropsWithRef<'div'>> {

	render() {
		const review = this.props.review,
			props: any = { ...this.props };
		delete props.anime;
		delete props.review;
		delete props.active;
		return (
			<Modal.Dialog size="xl" {...props} className={styles.container}>
				<Modal.Header>
					<div>
						<Row>
							<Col><LazyLoadImage width={80} src={review.reviewer.image_url} /></Col>
							<Col className="m-2">{review.reviewer.username}</Col>
						</Row>
					</div>
					<div style={{ float: "right", textAlign: "right" }}>
						<div>{new Date(review.date!).toLocaleDateString()}</div>
						<div>{review.reviewer.episode_seen} of {this.props.anime.totalEpisodes} episodes seen</div>
						<div>Reviewer's Rating: {review.reviewer.rating}</div>
						<Row>
							{
								Object.entries(review.reactions).map(([reaction, numOfPeople]) => (
									<Col style={{ whiteSpace: "nowrap" }} key={reaction}>{reaction}: <strong>{numOfPeople}</strong></Col>
								))
							}
						</Row>
					</div>
				</Modal.Header>
				<div className={styles.content_wrapper}>
					<div className={styles.content + " modal-content"}>
						{review.content}
					</div>
				</div>
			</Modal.Dialog>
		);
	}
}

export default loadingComponent(class Reviews extends Component<AnimeInfoProps & { reviews: ReviewData[]; }> {
	render() {
		return this.props.reviews?.map(review => {
			return <DisplayReview review={review} key={review.mal_id} anime={this.props.anime} />;
		}) ?? <div>No reviews 4 u :(</div>;
	}

}, async ({ anime }) => ({ reviews: (await MALUtils.animeReviews(anime as AnimeEntry & { malId: number; })) }), true);
