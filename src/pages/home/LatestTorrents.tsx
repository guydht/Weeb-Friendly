import React, { Component } from "react";
import Carousel from "react-bootstrap/Carousel";
import OverlayTrigger from "react-bootstrap/OverlayTrigger";
import Spinner from "react-bootstrap/Spinner";
import Tooltip from "react-bootstrap/Tooltip";
//@ts-ignore
import { LazyLoadComponent } from "react-lazy-load-image-component";
import { Link } from "react-router-dom";
import { reloadPage } from "../../App";
import { ReactComponent as DownloadIcon } from "../../assets/download.svg";
import { ReactComponent as DownloadingIcon } from "../../assets/Downloading.svg";
import AnimeEntry from "../../classes/AnimeEntry";
import Consts from "../../classes/Consts";
import { MALStatuses } from "../../classes/MalStatuses";
import TorrentManager from "../../classes/TorrentManager";
import ChooseSource from "../../components/ChooseSource";
import HasSeen from "../../components/HasSeen";
import SearchBar from "../../components/SearchBar";
import downloadedAnimeStyle from "../../css/pages/DownloadedAnime.module.css";
import styles from "../../css/pages/SeasonalCarousel.module.css";
import { groupByEpisode } from "../../utils/general";
import MALUtils from "../../utils/MAL";
import { chunkArray, Confirm } from "../../utils/OutsourcedGeneral";
import TorrentUtils, { DownloadStatus, SearchResult, Sources } from "../../utils/torrents";
import SeasonalCarousel from "./SeasonalCarousel";

type GroupedSearchResult = ReturnType<typeof groupByEpisode>[0];

export class DisplayTorrentEntry extends Component<{ searchResult: GroupedSearchResult; }> {
	render() {
		const downloadStatus = this.props.searchResult.downloadStatus;
		if (this.props.searchResult.animeEntry.malId)
			return (
				<div className="position-relative">
					{
						this.props.searchResult.seenThisEpisode() ? <HasSeen className={styles.downloadIcon} hasSeen={true} />
							: downloadStatus === DownloadStatus.downloaded ?
								<OverlayTrigger overlay={<Tooltip id={this.props.searchResult.animeEntry.malId.toString()}>Already Downloaded</Tooltip>} placement="auto">
									<DownloadIcon className={styles.downloadIcon} style={{ cursor: "not-allowed", opacity: 0.4 }} />
								</OverlayTrigger>
								: downloadStatus === DownloadStatus.currentlyDownloading ?
									<OverlayTrigger overlay={<Tooltip id={this.props.searchResult.animeEntry.malId.toString()}>Currently Downloading Episode</Tooltip>}>
										<DownloadingIcon className={styles.downloadIcon} style={{ cursor: "progress" }} />
									</OverlayTrigger>
									: <OverlayTrigger overlay={<Tooltip id={this.props.searchResult.animeEntry.malId.toString()}>Download Episode</Tooltip>}>
										<DownloadIcon className={styles.downloadIcon} onClick={() => DisplayTorrentEntry.downloadNow(this.props.searchResult)} />
									</OverlayTrigger>
					}
					<span className={styles.upperTitle}>
						{
							this.props.searchResult.episodeData.episodeOrMovieNumber ? `Episode ${this.props.searchResult.episodeData.episodeOrMovieNumber}` : (
								this.props.searchResult.episodeData.isBatch ? "Batch Episodes" : "Unknown Episode	"
							)
						}

					</span>
					<Link to={{
						pathname: "/anime/" + this.props.searchResult.animeEntry.malId,
						state: {
							animeEntry: this.props.searchResult.animeEntry
						}
					}}
						className={styles.link}>
						<img alt={this.props.searchResult.name} src={this.props.searchResult.animeEntry.imageURL}
							className={styles.image} />
						<div className={styles.cover}></div>
						<OverlayTrigger overlay={<Tooltip id="" >{this.props.searchResult.name}</Tooltip>} trigger={["hover", "focus"]}>
							<span className={styles.title}>{this.props.searchResult.animeEntry.name}</span>
						</OverlayTrigger>
					</Link>
				</div>
			);
		return (
			<div className={styles.emptyTorrentResult + " position-relative pb-4"}>
				<div style={{ overflowY: "hidden" }}>
					<SearchBar
						showImage
						placeholder="Search in MAL"
						gotoAnimePageOnChoose={false}
						onInputClick={e => {
							const target = e.target as HTMLInputElement,
								prevValue = target.value;
							target.value = this.props.searchResult.episodeData.anime_title;
							if (!prevValue)
								e.doSearch();
						}}
						onItemVisible={async (entry) => {
							await MALUtils.getAnimeInfo(entry as any);
							this.props.searchResult.reloadAnimeEntry();
							reloadPage();
						}}
						clickOnStart
						onItemClick={entry => DisplayTorrentEntry.setMALLink(this.props.searchResult, entry)} />
				</div>
				<OverlayTrigger overlay={<Tooltip id="" >{this.props.searchResult.name}</Tooltip>} trigger={["hover", "focus"]}>
					<span style={{ flex: "0 1" }} className={styles.title}>{this.props.searchResult.animeEntry.name}</span>
				</OverlayTrigger>
			</div>
		);
	}
	static setMALLink(searchResult: SearchResult, animeEntry: AnimeEntry) {
		animeEntry.syncGet();
		animeEntry.synonyms.add(searchResult.episodeData.anime_title);
		searchResult.animeEntry = animeEntry;
		animeEntry.syncPut();
		reloadPage();
	}
	static downloadNow(searchResult: GroupedSearchResult, promptDownload: boolean = true) {
		if (!searchResult.searchResults.length) return;
		const downloadName = searchResult.episodeData.name,
			doDownload = () => TorrentManager.add({ magnetURI: searchResult.searchResults[0]?.link.magnet });
		if (promptDownload)
			return new Promise<void>(resolve => {
				Confirm(`Download ${downloadName}?`, async (ok: boolean) => {
					if (ok && searchResult.searchResults[0]?.link.magnet)
						await doDownload();
					resolve();
				});
			});
		else
			return doDownload();
	}
}
class DisplayLatestTorrents extends Component<{ source?: Sources; }> {

	state: { torrents: GroupedSearchResult[], nextPageToLoad: number; } = {
		torrents: [],
		nextPageToLoad: 1
	};
	private isUnloaded: boolean = false;

	componentDidMount() {
		this.loadMoreUpdated();
	}
	componentWillUnmount(): void {
		this.isUnloaded = true;
	}

	async loadMoreUpdated() {
		if (this.props.source === undefined) return;

		const latest = await TorrentUtils.latest(this.state.nextPageToLoad, this.props.source);
		if (this.isUnloaded) return;
		this.state.torrents.push(...groupByEpisode(latest));
		this.setState({ torrents: this.state.torrents, nextPageToLoad: this.state.nextPageToLoad + 1 });

		if (Consts.AUTO_DOWNLOAD_NEW_EPISODES_OF_WATCHED_SERIES && Consts.DOWNLOADS_FOLDER.trim().length)
			this.autoDownloadNewEpisodes();
		return latest;
	}

	async autoDownloadNewEpisodes() {
		const filtered = this.state.torrents.filter(torrentEntry => (torrentEntry.animeEntry.myMalStatus === MALStatuses.Watching || torrentEntry.animeEntry.myMalStatus === MALStatuses["Plan To Watch"])
			&& !torrentEntry.seenThisEpisode() && torrentEntry.downloadStatus === DownloadStatus.notDownloaded
			&& !Consts.REMOVED_TORRENTS_IDS.includes(torrentEntry.link.magnet.slice(20, 60)));
		for (const filteredTorrentEntry of groupByEpisode(filtered))
			await DisplayTorrentEntry.downloadNow(filteredTorrentEntry, false);
	}

	render() {
		if (this.props.source === undefined) return null;
		if (!this.state.torrents.length)
			return (
				<div>
					<small className="d-block">Loading Torrents From {this.props.source}...</small>
					<Spinner animation="grow" />
				</div>
			);
		const handleSelect = (selectedIndex: number) => {
			if (selectedIndex + 1 >= this.state.torrents.length / (SeasonalCarousel.GRID_SIZE_X * SeasonalCarousel.GRID_SIZE_Y)) {
				this.loadMoreUpdated();
			}
		};
		return (
			<div>
				<h1>
					Latest {this.props.source} Uploads:
				</h1>
				<Carousel interval={null} className="px-5 mx-5 mt-5" onSelect={handleSelect}>
					{
						chunkArray(this.state.torrents, SeasonalCarousel.GRID_SIZE_X * SeasonalCarousel.GRID_SIZE_Y)
							.map((arrayChunk: GroupedSearchResult[]) => {
								return (
									<Carousel.Item key={arrayChunk[0].name} className={styles.carousel}>
										<div className={downloadedAnimeStyle.grid}>
											{
												arrayChunk.map(searchResult =>
													<LazyLoadComponent key={searchResult.name}>
														<DisplayTorrentEntry searchResult={searchResult} />
													</LazyLoadComponent>
												)
											}
										</div>
									</Carousel.Item>
								);
							})
					}
				</Carousel>
			</div >
		);
	}
}

export default class LatestTorrents extends Component {
	render() {
		return (
			<div className="mx-auto mt-5">
				<ChooseSource lazyLoad>
					<DisplayLatestTorrents />
				</ChooseSource>
			</div>
		);
	}
}
