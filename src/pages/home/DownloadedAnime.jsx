import React, { Component } from "react";
import Button from "react-bootstrap/Button";
import ButtonGroup from "react-bootstrap/ButtonGroup";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/FormGroup";
import InputGroup from "react-bootstrap/InputGroup";
import Row from "react-bootstrap/Row";
import { withRouter } from "react-router";
import Consts from "../../classes/Consts";
import ChangableText from "../../components/ChangableText";
import DownloadedFileThumbnail from "../../components/DownloadedFileThumbnail";
import styles from "../../css/pages/DownloadedAnime.module.css";
import { waitFor } from "../../jsHelpers/jifa";
import MALUtils from "../../utils/MAL";
import { displayToast } from "../global/ToastMessages";

export class DisplayDownloadedAnime extends Component {
	render() {
		let props = { ...this.props };
		delete props.style;
		delete props.downloadedItems;
		return (
			<div className={styles.grid} style={this.props.style || {}} >
				{
					this.props.downloadedItems.map(downloadedItem => {
						return (
							<DownloadedFileThumbnail {...props} key={downloadedItem.absolutePath} downloadedItem={downloadedItem} />
						);
					})
				}
			</div>
		);;
	}
}

export default withRouter(class DownloadedAnime extends Component {

	state = {
		sortOptions: [
			{
				displayName: 'File Name',
				active: false,
				reverse: true,
				sortFunction(a, b) { return a.episodeData.name.localeCompare(b.episodeData.name, navigator.language, { numeric: true }); }
			},
			{
				displayName: 'Download Time',
				active: true,
				default: true,
				reverse: false,
				sortFunction(a, b) { return b.lastUpdated - a.lastUpdated; }
			}
		],
		filters: []
	};
	componentDidMount() {
		if (Consts.DOWNLOADED_ITEMS.every(ele => !ele.malId) || MALUtils.storageSize === 0)
			waitFor(() => MALUtils.storageSize > 0, () => {
				this.setState({});
			});
	}

	render() {
		return (
			<div>
				<h1>
					Downloaded Anime:
                </h1>
				<Container>
					<Row>
						<InputGroup className="mx-5 mb-2 d-block">
							<ButtonGroup style={{ float: "left" }}>
								{
									this.state.sortOptions.map((props, i) => {
										return (
											<Button key={props.displayName} onClick={() => this.setSort(i)} className={props.active ? "active" : ""}>
												<span className={props.active ? props.reverse ? styles.arrowUp : styles.arrowDown : ""}>{props.displayName}</span>
											</Button>
										);
									})
								}
							</ButtonGroup>
							<ButtonGroup>
								<Button onClick={() => this.deleteWatched()}>
									Delete Watched
								</Button>
							</ButtonGroup>
							<Form style={{ float: "right", marginBottom: 0 }}>
								{
									Consts.DOWNLOADED_ITEMS_FILTER.map((filter, i) => {
										return (
											<ChangableText text={filter} key={filter}
												onChange={text => {
													if (text.trim() === "")
														Consts.DOWNLOADED_ITEMS_FILTER.splice(i, 1);
													else
														Consts.DOWNLOADED_ITEMS_FILTER[i] = text;
													Consts.setDownloadedItemsFilter(Consts.DOWNLOADED_ITEMS_FILTER);
													this.forceUpdate();
												}} />
										);
									})
								}
								<ChangableText key={Consts.DOWNLOADED_ITEMS_FILTER.length} defaultValue="Add Filter" removeButton={false}
									onClick={e => e.target.innerHTML = ""}
									onChange={text => {
										if (text === "") return;
										Consts.DOWNLOADED_ITEMS_FILTER.push(text);
										Consts.setDownloadedItemsFilter(Consts.DOWNLOADED_ITEMS_FILTER);
										this.forceUpdate();
									}} />
							</Form>
						</InputGroup>
					</Row>
					<Row>
						<DisplayDownloadedAnime downloadedItems={this.sortedItems} />
					</Row>
				</Container>
			</div>
		);
	}
	deleteWatched() {
		Consts.FILTERED_DOWNLOADED_ITEMS.forEach(downloadedItem => {
			if (downloadedItem.seenThisEpisode())
				downloadedItem.delete().then(error => error && displayToast({
					body: error.message,
					title: 'Couldn\'t delete file!'
				}));
		});
	}
	setSort(propIndex) {
		let options = this.state.sortOptions.map((prop, i) => {
			if (i !== propIndex) {
				prop.active = false;
				prop.reverse = true;
			}
			else {
				prop.reverse = !prop.reverse;
				prop.active = true;
			}
			return prop;
		});
		this.setState({
			sortOptions: options
		});
	}
	get sortedItems() {
		const current = this.currentOption;
		const sorted = Consts.FILTERED_DOWNLOADED_ITEMS.sort(current.sortFunction);
		if (current.reverse)
			sorted.reverse();
		return sorted;
	}
	get currentOption() {
		return this.state.sortOptions.find(ele => ele.active);
	}
});
