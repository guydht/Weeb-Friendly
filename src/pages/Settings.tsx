import React, { Component } from "react";
import Col from "react-bootstrap/Col";
import Container from "react-bootstrap/Container";
import FormCheck from "react-bootstrap/FormCheck";
import FormControl from "react-bootstrap/FormControl";
import Jumbotron from "react-bootstrap/Jumbotron";
import Row from "react-bootstrap/Row";
import { ReactSortable } from "react-sortablejs";
import { ThumbnailManager } from "../classes/AnimeStorage";
import Consts from "../classes/Consts";
import changableTextStyles from "../css/components/ChangableText.module.css";
import styles from "../css/pages/Settings.module.css";
import { Sources } from "../utils/torrents";

export default class Settings extends Component {
	render() {
		return (
			<Jumbotron className="text-center px-5">
				<h2>Settings</h2>
				<Container className={styles.container}>
					<Row className="mb-5">
						<Col>
							Sources Preferences:
						</Col>
						<Col>
							<ReactSortable list={Consts.SOURCE_PREFERENCE.map(source => ({ id: source }))} className={styles.backgroundGreenToRed + " py-1"} setList={list => this.setSourcesPriority(list.map(ele => ele.id))}>
								{
									Consts.SOURCE_PREFERENCE_ENTRIES.filter(ele => ele[1] !== Sources.Any).map(([sourceName, source]) => {
										return (
											<span key={source} className={changableTextStyles.textWrapper + " ml-1"}>{sourceName}</span>
										);
									})
								}
							</ReactSortable>
						</Col>
					</Row>
					<Row className="mb-5">
						<Col>
							Quality Priority:
						</Col>
						<Col>
							<ReactSortable list={Consts.QUALITY_PREFERENCE.map(quality => ({ id: quality }))} className={styles.backgroundGreenToRed + " py-1"} setList={qualities => this.setQualityPreference(qualities.map(ele => ele.id))}>
								{
									Consts.QUALITY_PREFERENCE.map(quality => {
										return (
											<span key={quality} className={changableTextStyles.textWrapper}>{quality}</span>
										);
									})
								}
							</ReactSortable>
						</Col>
					</Row>
					<Row className="mb-5">
						<Col>
							Toggle Middle Click Functionallity:
						</Col>
						<Col>
							<FormCheck
								className={styles.bigSwitch}
								type="switch"
								id="middleClickToggle"
								checked={Consts.MIDDLE_CLICK}
								onChange={(e: React.ChangeEvent) => this.setMiddleClickToggle((e.target as HTMLInputElement).checked)}
								label="" custom />
						</Col>
					</Row>
					<Row className="mb-5">
						<Col>Save anime thumbnail photos locally</Col>
						<Col>
							<FormCheck
								className={styles.bigSwitch}
								type="switch"
								id="thumbnailStorage"
								checked={ThumbnailManager.SAVED_THUMBNAILS_STATE}
								onChange={(e: React.ChangeEvent) => this.setThumbnailStorage((e.target as HTMLInputElement).checked)}
								label="" custom />
						</Col>
					</Row>
					<Row className="mb-5">
						<Col>Auto download new episodes of shows I'm watching</Col>
						<Col>
							<FormCheck
								className={styles.bigSwitch}
								type="switch"
								id="autoDownloadNewEpisodes"
								checked={Consts.AUTO_DOWNLOAD_NEW_EPISODES_OF_WATCHED_SERIES}
								onChange={(e: React.ChangeEvent) => this.setAutoDownloadNewEpisode((e.target as HTMLInputElement).checked)}
								label="" custom />
						</Col>
					</Row>
					<Row className="mb-5">
						<Col>Auto update MAL when finishing an episode (set as "watched" when you've watched 95% of the episode) </Col>
						<Col>
							<FormCheck
								className={styles.bigSwitch}
								type="switch"
								id="autoUpdateInMal"
								checked={Consts.AUTO_UPDATE_IN_MAL}
								onChange={(e: React.ChangeEvent) => this.setAutoUpdateInMal((e.target as HTMLInputElement).checked)}
								label="" custom />
						</Col>
					</Row>
					<Row className="mb-5">
						<Col>Blur Unwatched Episodes</Col>
						<Col>
							<FormCheck
								className={styles.bigSwitch}
								type="switch"
								id="blurUnwatched"
								checked={Consts.BLUR_UNWATCHED_EPISODES}
								onChange={(e: React.ChangeEvent) => this.setBlurUnwatchedEpisodes((e.target as HTMLInputElement).checked)}
								label="" custom />
						</Col>
					</Row>
					<Row className="mb-5">
						<Col>Max number of Simultanious Torrent Downloads</Col>
						<Col>
							<FormControl
								type="number"
								style={{ width: "4em" }}
								value={Consts.MAX_NUMBER_OF_SIMULTANIOUS_TORRENTS}
								onChange={(e: React.ChangeEvent) => this.setMaxNumberOfSimultaniousDownloads((e.target as HTMLInputElement).valueAsNumber)} />
						</Col>
					</Row>
				</Container>
			</Jumbotron>
		);
	}
	setSourcesPriority(sources: Sources[]) {
		Consts.setSourcesPreference(sources);
		this.forceUpdate();
	}
	setQualityPreference(qualities: number[]) {
		Consts.setQualityPreference(qualities.map(Number));
		this.forceUpdate();
	}
	setMiddleClickToggle(activated: boolean) {
		Consts.setMiddleClick(activated);
		this.forceUpdate();
	}
	setThumbnailStorage(activated: boolean) {
		ThumbnailManager.setThumbnailStorageState(activated);
		this.forceUpdate();
	}
	setAutoUpdateInMal(activated: boolean) {
		Consts.setAutoUpdateInMal(activated);
		this.forceUpdate();
	}
	setBlurUnwatchedEpisodes(activated: boolean) {
		Consts.setBlurUnwatchedEpisodes(activated);
		this.forceUpdate();
	}
	setAutoDownloadNewEpisode(activated: boolean) {
		Consts.setAutoDownloadNewEpisodeOfWatchedSeries(activated);
		this.forceUpdate();
	}
	setMaxNumberOfSimultaniousDownloads(numOfSimTorr: number) {
		Consts.setMaxNumberOfSimultaniousTorrents(numOfSimTorr);
		this.forceUpdate();
	}
}
