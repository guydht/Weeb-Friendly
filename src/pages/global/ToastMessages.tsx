import React, { Component } from "react";
import Toast from "react-bootstrap/Toast";

declare type toast = {
	body: string,
	title: string,
	opacity?: number,
	id?: string;
};

export function displayToast(toast: toast, timeout: number = ToastMessage.DEFAULT_TOAST_TIMEOUT) {
	(window as any).displayToast(toast);
}

export function uuid() {
	// eslint-disable-next-line
	return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, (c: any) =>
		// eslint-disable-next-line
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	);
}

export default class ToastMessage extends Component<{ toasts?: toast[]; }> {
	static DEFAULT_TOAST_TIMEOUT = 3500;
	static TOAST_FADE_DURATION = 300;

	state: { toasts: toast[]; } = {
		toasts: this.props.toasts ?? [],
	};

	componentDidMount() {
		(window as any).displayToast = this.displayToast.bind(this);
	}

	render() {
		return (
			<div
				style={{
					position: 'fixed',
					top: "5vh",
					right: 0,
					zIndex: 9999999
				}}
			>
				{this.state.toasts.map(toast => {
					return (
						<Toast onClose={() => this.fadeToastOut(toast)} key={toast.id} style={{ transition: `opacity ${ToastMessage.TOAST_FADE_DURATION}ms`, opacity: toast.opacity }}>
							<Toast.Header>
								<strong className="mr-auto">{toast.title}</strong>
							</Toast.Header>
							<Toast.Body>{toast.body}.</Toast.Body>
						</Toast>
					);
				})}
			</div>
		);
	}

	getToastIndex(toast: toast): number {
		if (toast.id)
			return this.state.toasts.findIndex(ele => ele.id === toast.id);
		return this.state.toasts.findIndex(ele => ele.title === toast.title && ele.body === toast.body);
	}

	displayToast(toast: toast, timeout: number = ToastMessage.DEFAULT_TOAST_TIMEOUT) {
		//for persistent toast just pass null through "timeout" variable.
		toast.id = uuid();
		this.setState({
			toasts: this.state.toasts.concat({ ...toast, opacity: 1 })
		});
		if (timeout)
			setTimeout(() => this.fadeToastOut(toast), timeout);
	}

	fadeToastOut(toast: toast) {
		let toasts = this.state.toasts;
		if (this.getToastIndex(toast) in toasts) {
			toasts[this.getToastIndex(toast)].opacity = 0;
			this.setState({});
			setTimeout(() => {
				this.state.toasts.splice(this.getToastIndex(toast), 1);
				this.setState({});
			}, ToastMessage.TOAST_FADE_DURATION);
		}
	}
};
