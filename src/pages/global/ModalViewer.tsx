import React, { Component } from "react";
import Modal from "react-bootstrap/Modal";

declare type ModalData = {
	body: React.ReactChild;
	title: React.ReactChild;
	onClose: () => void;
};


export function displayModal(modal: Omit<ModalData, "onClose">) {
	return new Promise(resolve => {
		(modal as ModalData).onClose = resolve as any;
		(window as any).displayModal(modal);
	});
}

export default class ModalViewer extends Component {

	state = {
		show: false,
		modalBody: null,
		modalTitle: null,
		onClose: function () { }
	};

	componentDidMount() {
		(window as any).displayModal = this.displayModal.bind(this);
	}

	displayModal(modalData: ModalData) {
		this.setState({
			show: true,
			modalTitle: modalData.title,
			modalBody: modalData.body,
			onClose: modalData.onClose
		});
	}


	render() {
		const hide = () => {
			this.setState({ show: false });
			this.state.onClose?.();
		};
		return (
			<Modal show={this.state.show} onHide={hide}>
				<Modal.Header closeButton>
					<Modal.Title>{this.state.modalTitle}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{this.state.modalBody}
				</Modal.Body>
			</Modal>
		);
	}

};
