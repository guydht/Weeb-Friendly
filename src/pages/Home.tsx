import React, { Component } from "react";
import Jumbotron from "react-bootstrap/Jumbotron";
import Toast from "react-bootstrap/Toast";
import Consts from "../classes/Consts";
import { hasInternet } from "../utils/OutsourcedGeneral";
import CurrentlyWatching from "./home/CurrentlyWatching";
import DownloadedAnime from "./home/DownloadedAnime";
import LatestTorrents from "./home/LatestTorrents";
import SeasonalCarousel from "./home/SeasonalCarousel";


export default class Home extends Component {

	static noInternetComponent(componentDiscription: string) {
		return (
			<Toast className="mx-auto mt-5">
				<Toast.Header closeButton={false}>
					<span>You don't have internet connection!</span>
				</Toast.Header>
				<Toast.Body>
					To see {componentDiscription}, Please connect to the internet!
                </Toast.Body>
			</Toast>
		);
	}

	render() {
		return (
			<Jumbotron style={{ textAlign: "center" }}>
				{
					Consts.DOWNLOADS_FOLDER &&
					<DownloadedAnime />
				}
				{
					Consts.MAL_USER.isLoggedIn ?
						<CurrentlyWatching /> : Consts.MAL_USER.isLoggedIn && Home.noInternetComponent("Currently Watching")
				}
				{
					hasInternet() && (
						<div>
							<SeasonalCarousel />
							<LatestTorrents />
						</div>
					)
				}
			</Jumbotron>
		);
	}
}
