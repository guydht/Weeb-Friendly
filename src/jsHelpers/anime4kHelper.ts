import {
  Anime4KPipeline,
  Anime4KPipelineDescriptor,
  CNNM,
  CNNUL,
  CNNVL,
  CNNx2M,
  CNNx2VL,
  ClampHighlights,
  GANUUL,
  GANx4UUL,
  Original
} from "anime4k-webgpu";
import { onElementRemoved } from "../utils/general";

const SHADER = `
struct VertexOutput {
  @builtin(position) Position : vec4<f32>,
  @location(0) fragUV : vec2<f32>,
}

@vertex
fn vert_main(@builtin(vertex_index) VertexIndex : u32) -> VertexOutput {
  const pos = array(
    vec2( 1.0,  1.0),
    vec2( 1.0, -1.0),
    vec2(-1.0, -1.0),
    vec2( 1.0,  1.0),
    vec2(-1.0, -1.0),
    vec2(-1.0,  1.0),
  );

  const uv = array(
    vec2(1.0, 0.0),
    vec2(1.0, 1.0),
    vec2(0.0, 1.0),
    vec2(1.0, 0.0),
    vec2(0.0, 1.0),
    vec2(0.0, 0.0),
  );

  var output : VertexOutput;
  output.Position = vec4(pos[VertexIndex], 0.0, 1.0);
  output.fragUV = uv[VertexIndex];
  return output;
}`;
const FRAGMENT_SHADER = `
@group(0) @binding(1) var mySampler: sampler;
@group(0) @binding(2) var myTexture: texture_2d<f32>;

@fragment
fn main(@location(0) fragUV : vec2f) -> @location(0) vec4f {
  return textureSampleBaseClampToEdge(myTexture, mySampler, fragUV);
}`;

if (!window.GPUTextureUsage || !window.GPUShaderModule || !window.GPUShaderStage) window.location.reload();

async function configureWebGPU(canvas: HTMLCanvasElement) {
  const adapter = await navigator.gpu.requestAdapter();
  const device = await adapter!.requestDevice();

  const context = canvas.getContext('webgpu') as GPUCanvasContext;
  const presentationFormat = navigator.gpu.getPreferredCanvasFormat();

  context.configure({
    device,
    format: presentationFormat,
    alphaMode: 'premultiplied',
  });

  return { device, context, presentationFormat };
}

async function bindCanvasToVideo(canvas: HTMLCanvasElement, videoElement: HTMLVideoElement) {
  const WIDTH = videoElement.videoWidth;
  const HEIGHT = videoElement.videoHeight;
  const { device, context, presentationFormat } = await configureWebGPU(canvas);
  const bindGroupLayout = device.createBindGroupLayout({
    entries: [
      {
        binding: 1,
        visibility: window.GPUShaderStage?.FRAGMENT ?? 2,
        sampler: {},
      },
      {
        binding: 2,
        visibility: window.GPUShaderStage?.FRAGMENT ?? 2,
        texture: {},
      },
    ],
  });

  let videoFrameTexture = device.createTexture({
    size: [WIDTH, HEIGHT, 1],
    format: 'rgba16float',
    usage: window.GPUTextureUsage ? window.GPUTextureUsage.TEXTURE_BINDING
      | window.GPUTextureUsage.COPY_DST
      | window.GPUTextureUsage.RENDER_ATTACHMENT : 22,
  });
  let pipeline: [Anime4KPipeline, ...Anime4KPipeline[],];
  const setEffect = (effect: 'Original' |
    'Lightweight' |
    'Lightweight+' |
    'Medium' |
    'Medium+' |
    'Heavy' |
    'Heavy+' |
    'GPU Hurts',
    includeClamp: boolean = true,
  ) => {
    let initialPipelineParams: Anime4KPipelineDescriptor = {
      device,
      inputTexture: videoFrameTexture,
    }, includeClampPipeline;
    if (includeClamp) {
      includeClampPipeline = new ClampHighlights(initialPipelineParams);
      initialPipelineParams = { device, inputTexture: includeClampPipeline.getOutputTexture() };
    }
    let pipeline1: Anime4KPipeline, pipeline2: Anime4KPipeline;
    switch (effect) {
      case 'Original':
      default:
        pipeline = [new Original(initialPipelineParams)];
        break;
      case 'Lightweight':
        pipeline1 = new CNNM(initialPipelineParams);
        pipeline = [pipeline1];
        break;
      case "Lightweight+":
        pipeline1 = new CNNM(initialPipelineParams);
        pipeline2 = new CNNx2M({ device, inputTexture: pipeline1.getOutputTexture() });
        pipeline = [pipeline1, pipeline2];
        break;
      case 'Medium':
        pipeline1 = new CNNVL(initialPipelineParams);
        pipeline2 = new CNNx2M({ device, inputTexture: pipeline1.getOutputTexture() });
        pipeline = [pipeline1, pipeline2];
        break;
      case 'Medium+':
        pipeline1 = new CNNVL(initialPipelineParams);
        pipeline2 = new CNNx2VL({ device, inputTexture: pipeline1.getOutputTexture() });
        pipeline = [pipeline1, pipeline2];
        break;
      case 'Heavy':
        pipeline1 = new CNNUL(initialPipelineParams);
        pipeline2 = new CNNx2M({ device, inputTexture: pipeline1.getOutputTexture() });
        pipeline = [pipeline1, pipeline2];
        break;
      case "Heavy+":
        pipeline1 = new CNNUL(initialPipelineParams);
        pipeline2 = new CNNx2VL({ device, inputTexture: pipeline1.getOutputTexture() });
        pipeline = [pipeline1, pipeline2];
        break;
      case 'GPU Hurts':
        pipeline1 = new GANUUL(initialPipelineParams);
        pipeline2 = new GANx4UUL({ device, inputTexture: pipeline1.getOutputTexture() });
        pipeline = [pipeline1, pipeline2];
        break;
    }
    if (includeClampPipeline)
      pipeline.unshift(includeClampPipeline);
    updateCanvasSize();
    updateRenderBindGroup();
    drawFrame();
  };
  function updateVideoFrameTexture() {
    device.queue.copyExternalImageToTexture(
      { source: videoElement },
      { texture: videoFrameTexture },
      [WIDTH, HEIGHT],
    );
  }
  function updateCanvasSize() {
    canvas.width = pipeline.slice(-1)[0].getOutputTexture().width;
    canvas.height = pipeline.slice(-1)[0].getOutputTexture().height;
  }

  let renderPipeline = device.createRenderPipeline({
    layout: device.createPipelineLayout({
      bindGroupLayouts: [bindGroupLayout],
    }),
    vertex: {
      module: device.createShaderModule({
        code: SHADER
      }),
      entryPoint: "vert_main",
    },
    fragment:
    {
      module: device.createShaderModule({
        code: FRAGMENT_SHADER
      }),
      entryPoint: "main",
      targets: [
        {
          format: presentationFormat,
        }
      ]
    },
    primitive: {
      topology: 'triangle-list',
    },
  });
  let renderBindGroup: GPUBindGroup;
  const sampler = device.createSampler();
  function updateRenderBindGroup() {
    renderBindGroup = device.createBindGroup({
      layout: bindGroupLayout,
      entries: [
        {
          binding: 1,
          resource: sampler,
        },
        {
          binding: 2,
          resource: pipeline.slice(-1)[0].getOutputTexture().createView(),
        }
      ]
    });
  }
  function drawFrame() {
    // fetch a new frame from video element into texture
    try {
      updateVideoFrameTexture();
    } catch (e) { return; }

    // initialize command encoder
    const commandEncoder = device.createCommandEncoder();

    // encode compute pipeline commands
    pipeline.forEach(pipelineElement => pipelineElement.pass(commandEncoder));

    // dispatch render pipeline
    const passEncoder = commandEncoder.beginRenderPass({
      colorAttachments: [
        {
          view: context.getCurrentTexture().createView(),
          clearValue: {
            r: 0.0, g: 0.0, b: 0.0, a: 1.0,
          },
          loadOp: 'clear',
          storeOp: 'store',
        },
      ],
    });
    passEncoder.setPipeline(renderPipeline);
    passEncoder.setBindGroup(0, renderBindGroup);
    passEncoder.draw(6);
    passEncoder.end();
    device.queue.submit([commandEncoder.finish()]);
  }
  setEffect(localStorage.getItem("upscale-algo") as any || "Original", localStorage.getItem("upscale-clamp") !== "false");
  if (videoElement.dataset.bindedToCanvas === "true")
    return;
  videoElement.dataset.bindedToCanvas = "true";
  videoElement.addEventListener("seeked", drawFrame);
  videoElement.requestVideoFrameCallback(function onFrame() {
    drawFrame();
    videoElement.requestVideoFrameCallback(onFrame);
  });
  onElementRemoved(videoElement).then(removed => {
    if (removed) {
      context.unconfigure();
      device.destroy();
      videoFrameTexture.destroy();
    }
  });
  return setEffect;
}
export { bindCanvasToVideo };

