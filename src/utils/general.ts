import { convertToASS } from "convert-srt-to-ass";
import { Address4, Address6 } from "ip-address";
import AnimeEntry from "../classes/AnimeEntry";
import Consts from "../classes/Consts";
import DownloadedItem from "../classes/DownloadedItem";
import { groupBy } from "./OutsourcedGeneral";
import { EpisodeData, SearchResult } from "./torrents";

const { parseDate } = window.require("chrono-node");

function parseBroadcastToDate(broadcastTime: string): Date {
	const split = broadcastTime.match(/^[^ ]+(?=s)|.+/gi) ?? [];
	broadcastTime = (split[0] + split[1]?.slice(1)) ?? broadcastTime;
	let nextBroadcastDate = parseDate(broadcastTime);
	if (nextBroadcastDate < new Date())
		nextBroadcastDate = new Date(Number(nextBroadcastDate) + 604800000);
	return nextBroadcastDate;
}

type AnimeRelations = Record<number, {
	rangeStart: number,
	rangeEnd: number,
	destination: {
		id: number;
		rangeStart: number;
		rangeEnd?: number;
	};
}[]>;


async function getAnimeRelations() {
	const fileContent = await fetch("https://raw.githubusercontent.com/erengy/anime-relations/master/anime-relations.txt").then(r => r.text()),
		lines = fileContent.split("\n").filter(ele => ele.match(/^- [0-9]/)),
		lineParser = (line: string) => {
			const id = Number(line.match(/[0-9]+/)),
				rangeStart = Number(line.match(/(?<=:)[0-9]+/)),
				rangeEnd = Number(line.match(/(?<=:[0-9]+-)[0-9]+/)) || rangeStart,
				destinationId = Number(line.match(/(?<=-> )[0-9]+/)) || id,
				destinationRangeStart = Number(line.match(/(?<=:)[0-9]+/g)?.[1]),
				destinationRangeEnd = Number(line.match(/(?<=:[0-9]+-)[0-9]+/g)?.[1]) || destinationRangeStart,
				repeat = line.endsWith("!");
			const returnObject = {
				id,
				rangeStart,
				rangeEnd,
				destination: {
					id: destinationId,
					rangeStart: destinationRangeStart,
					rangeEnd: destinationRangeEnd
				}
			};
			if (repeat)
				return [returnObject, {
					...returnObject,
					id: destinationId
				}];
			return returnObject;
		},
		parsedLines = lines.map(lineParser).flat(Infinity) as {
			id: number,
			rangeStart: number,
			rangeEnd: number,
			destination: {
				id: number,
				rangeStart: number,
				rangeEnd?: number;
			};
		}[];
	const asObject: AnimeRelations = {};
	parsedLines.forEach(parsedLine => {
		if (!(parsedLine.id in asObject))
			asObject[parsedLine.id] = [parsedLine];
		else
			asObject[parsedLine.id].push(parsedLine);
	});
	return asObject;
}


const handleRelations = (downloadedItem: { animeEntry: AnimeEntry, episodeData: EpisodeData; }) => {
	let didChange = false;
	if (downloadedItem.animeEntry.malId) {
		const relations = Consts.animeRelations[downloadedItem.animeEntry.malId];
		if (relations) {
			relations.forEach(relation => {
				if (relation.rangeStart <= downloadedItem.episodeData.episodeOrMovieNumber &&
					(relation.rangeEnd >= downloadedItem.episodeData.episodeOrMovieNumber)) {
					didChange = true;
					downloadedItem.animeEntry = new AnimeEntry({ malId: relation.destination.id });
					downloadedItem.episodeData.episodeOrMovieNumber -= relation.rangeStart - relation.destination.rangeStart;
					if (downloadedItem.animeEntry.name) {
						downloadedItem.episodeData.anime_title = downloadedItem.animeEntry.name;
						// Reset the name property according to new anime_title
						downloadedItem.episodeData.name = `${downloadedItem.episodeData.anime_title}${downloadedItem.episodeData.year ? ` (${downloadedItem.episodeData.year})` : ""}${!isNaN(downloadedItem.episodeData.episodeOrMovieNumber) ? ` Episode ${downloadedItem.episodeData.episodeOrMovieNumber}` : ''}${downloadedItem.episodeData.other ? " (" + downloadedItem.episodeData.other + ")" : ''}`;
					}
				}
			});
		}
	}
	return didChange;
};


let { ipcRenderer } = window.require("electron"),
	walkDir = async (path: string) => {
		if (path)
			return (await ipcRenderer.invoke("walkDir", path)).map((ele: [string, string, Date]) => new DownloadedItem(...ele));
	};

function parseStupidAmericanDateString(dateString: string) {
	return parseDate(dateString);
}

type GroupedEpisodes = SearchResult & { searchResults: SearchResult[]; };
function groupByEpisode(episodes: SearchResult[]): GroupedEpisodes[] {
	return groupBy(episodes.map(ele => {
		(ele as any).asd = ele.episodeData.anime_title + ele.episodeData.episodeOrMovieNumber;
		return ele;
	}) as GroupedEpisodes[], 'asd').map(grouped => {
		const mostMatching = getMostMatchingEpisodeInGroup(grouped);;
		grouped.forEach(group => {
			delete (group as any).asd;
			group.searchResults = grouped;
		});
		return mostMatching;
	});
}

const qualityPreferenceIndex = (ele: GroupedEpisodes) => {
	// return the index of the closest quality in QUALITY_PREFERENCE to this `quality` variable.
	const quality = ele.episodeData.quality;
	return Consts.QUALITY_PREFERENCE.reduce((closestIndex, qualityPreference, index, arr) =>
		Math.abs(arr[closestIndex] - quality) > Math.abs(qualityPreference - quality) ? index : closestIndex, 0);
};
const hasHEVC = (ele: GroupedEpisodes) => ele.episodeData.video_term === "HEVC";
function getMostMatchingEpisodeInGroup(group: GroupedEpisodes[]): GroupedEpisodes {
	return group.sort((a, b) => {
		return (qualityPreferenceIndex(a) - qualityPreferenceIndex(b)) | (Number(hasHEVC(b)) - Number(hasHEVC(a)));
	})[0];
}

function setTimeoutPromisified(delay: number) {
	return new Promise<void>(resolve => {
		setTimeout(() => {
			resolve();
		}, delay);
	});
}

function groupBySeries(episodes: SearchResult[]) {
	return groupBy(episodes.map(ele => {
		(ele as any).asd = ele.animeEntry.malId || ele.episodeData.anime_title;
		return ele;
	}), 'asd').map(grouped => {
		grouped.forEach(ele => {
			delete (ele as any).asd;
		});
		return grouped;
	});
}

function closestQualityInQualityPreference(quality: number) {
	return [...Consts.QUALITY_PREFERENCE].sort((qualityA, qualityB) => Math.abs(qualityA - quality) - Math.abs(qualityB - quality))[0];
}

async function convertSrtToASS(srt: string) {
	return convertToASS(srt);
}

const { networkInterfaces } = window.require('os');

function getLocalUrlForIp(visibleHost?: string) {
	if (!visibleHost)
		return "";
	if (visibleHost.match(/([0-9]{0,3}\.){3}[0-9]{0,3}/g))
		return getLocalIpv4(new Address4(visibleHost));
	return getLocalIpv6(new Address6(visibleHost));
}

function getLocalIpv6(visibleIp?: Address6) {
	const interfaces = networkInterfaces();
	const ipv6Entries = Object.entries(interfaces).map(([key, values]: any[]) => {
		return [key, values.find((ele: any) => ele.family === "IPv6")];
	});
	const foundIpv6 = ipv6Entries.find(([key, value]: any[]) => {
		if (value.internal) return false;
		if (!visibleIp)
			return true;
		const address = new Address6(`${value.address}/${netmaskToCidrIPv6(value.netmask)}`);
		return address.isInSubnet(visibleIp);
	}) ?? ipv6Entries.find(([key, value]: any[]) => !value.internal);
	const ipv4OfFoundIpv6 = interfaces[foundIpv6?.[0]].find((ele: any) => ele.family === "IPv4");
	return ipv4OfFoundIpv6.address;
}

function getLocalIpv4(visibleIp: Address4) {
	const interfaces = networkInterfaces();
	const ipv4Entries = Object.entries(interfaces).map(([key, values]: any[]) => {
		return [key, values.find((ele: any) => ele.family === "IPv4")];
	});
	return (ipv4Entries.find(([_, value]: any[]) => {
		if (!value.internal) {
			const address = new Address4(`${value.address}/${netmaskToCidrIPv4(value.netmask)}`);
			return visibleIp.isInSubnet(address);
		}
		return false;
	})?.[1] as any)?.address;
}

function netmaskToCidrIPv6(netmask: string) {
	const bytes = netmask.split(':');
	let cidr = 0;
	for (let i = 0; i < bytes.length; i++) {
		let byte = parseInt(bytes[i], 16);
		while (byte) {
			cidr += byte & 1;
			byte >>= 1;
		}
	}
	return cidr;
}

function netmaskToCidrIPv4(netmask: string) {
	let cidr = 0;
	let parts = netmask.split(".");
	for (let i = 0; i < parts.length; i++) {
		let part = parseInt(parts[i], 10);
		while (part) {
			cidr += part & 1;
			part >>= 1;
		}
	}
	return cidr;
}

function secondsToTimeDisplay(time: string | number, sendWithWords?: string) {
	var seconds = typeof time === "string" ? parseInt(time) : time;
	var years = Math.floor(seconds / 31536000),
		days = Math.floor((seconds % 31536000) / 86400),
		hours = Math.floor(((seconds % 31536000) % 86400) / 3600),
		minutes = Math.floor((((seconds % 31536000) % 86400) % 3600) / 60);
	seconds = Math.floor((((seconds % 31536000) % 86400) % 3600) % 60);
	var arr = [years, days, hours, minutes, seconds],
		output = ":";
	arr.forEach(function (e) {
		if ((e > 0 && e < 10) || (output.length > 1 && e === 0)) output += "0" + e + ":";
		else if (e > 0) output += e + ":";
	});
	output = output.slice(1, -1);
	if (!isNaN(Number(output))) output = "00:" + (output || "00");
	if (!sendWithWords)
		return output;
	var names: Record<string, number> = {
		y: years,
		d: days,
		h: hours,
		m: minutes,
		s: seconds
	};
	if (sendWithWords === "Full") names = {
		years: years,
		days: days,
		hours: hours,
		minutes: minutes,
		seconds: seconds
	};
	return arr.filter(function (a) {
		return a > 0;
	}).map(function (a, i, arr) {
		var letter = Object.keys(names).find(function (ele) {
			return names[ele] === a;
		});
		if (letter)
			delete names[letter];
		return a + (letter ?? "");
	}).join(", ");
}

function promisify<T>(callbackedFunction: (callback: (...args: T[]) => void) => void) {
	return new Promise<T | T[]>(resolve => {
		function middleWare(...output: T[]) {
			if (output.length > 1)
				resolve(output);
			else
				resolve(output);
		}
		callbackedFunction(middleWare);
	});
}

/**
 * Returns a generator for getting all the parents of a given element, up until the document's body.
 */
function* parentsOfElement(element: Node): Generator<Node> {
	if (element === null || element === document.body as Node) return;
	yield element;
	if (element.parentNode)
		yield* parentsOfElement(element.parentNode);
}

async function onElementRemoved(element: Element, timeout: number | undefined = undefined) {
	return await new Promise(resolve => {
		if (!element.parentElement) return resolve(false);
		let timeoutDestroyer: ReturnType<typeof setTimeout> | undefined = undefined;
		const elementParents = new Set(parentsOfElement(element));
		const observer = new MutationObserver((mutations) => {
			mutations.forEach(mutation => {
				if ([...mutation.removedNodes].some(removedElement => elementParents.has(removedElement))) {
					clearTimeout(timeoutDestroyer);
					observer.disconnect();
					resolve(true);
				}
			});
		});
		observer.observe(document.body, {
			childList: true,
			subtree: true,
		});
		if (timeout)
			timeoutDestroyer = setTimeout(() => {
				observer.disconnect();
				resolve(false);
			}, timeout);
	});
}

export { closestQualityInQualityPreference, convertSrtToASS, getAnimeRelations, getLocalUrlForIp as getLocalIp, groupByEpisode, groupBySeries, handleRelations, onElementRemoved, parseBroadcastToDate, parseStupidAmericanDateString, promisify, secondsToTimeDisplay, setTimeoutPromisified, walkDir };
export type { AnimeRelations };

