import { pantsu, si } from "nyaapi";
import AnimeEntry from "../classes/AnimeEntry";
import Consts from "../classes/Consts";
import DownloadedItem from "../classes/DownloadedItem";
import TorrentManager from "../classes/TorrentManager";
import { normalizeAnimeName } from "./OutsourcedGeneral";
import { handleRelations } from "./general";

const DEFAULT_MAX_RESULTS = 150;
const { parseSync } = window.require("anitomy-js");

function formatBytes(bytes: number, decimals = 2) {
	if (bytes === 0) return '0 Bytes';

	const k = 1024;
	const dm = decimals < 0 ? 0 : decimals;
	const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

	const i = Math.floor(Math.log(bytes) / Math.log(k));

	return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

export enum Sources {
	SubsPlease = "SubsPlease",
	"Erai-raws" = "Erai-raws",
	AnimeChap = "AnimeChap",
	ASW = "ASW",
	EMBER = "ember",
	YuiSubs = "YuiSubs",
	DKB = "DKB",
	Judas = "Judas",
	AnimeTime = "Anime Time",
	ToonsHub = "ToonsHub",
	Any = "Any"
}
export interface SearchResultExtraInfo {
	description: string;
	size: string;
	fileList: string[];
	comments: {
		text: string,
		author: string,
		authorImage: string,
		date: Date;
	}[];
}

const sourceToUploaderMapping = {
	[Sources.SubsPlease]: {
		si: "subsplease",
	},
	[Sources["Erai-raws"]]: {
		si: "Erai-raws"
	},
	[Sources.EMBER]: {
		si: "Ember_Encodes"
	},
	[Sources.ASW]: {
		si: "AkihitoSubsWeeklies"
	},
	[Sources.YuiSubs]: {
		si: "YuiSubs"
	},
	[Sources.AnimeChap]: {
		si: "AnimeChap",
	},
	[Sources.DKB]: {
		si: "DKB0512"
	},
	[Sources.Judas]: {
		si: "Judas"
	},
	[Sources.AnimeTime]: {
		si: "sff"
	},
	[Sources.ToonsHub]: {
		si: "[ToonsHub] ",
		isUser: false, // They upload 't upload from "annonymous" or don't upload from the same account every time.
	},
	[Sources.Any]: {
		si: "",
		isUser: false,
	}
};

const sourceToRegexMapping: { [key in Sources]?: RegExp } = {
	[Sources["Erai-raws"]]: /\[erai-raws\]/gi,
	[Sources.SubsPlease]: /\[SubsPlease\]/g,
	[Sources.YuiSubs]: /\[YuiSubs\] /gi,
	[Sources.ASW]: /\[ASW\]/gi,
	[Sources.EMBER]: /\[EMBER\]/gi,
	[Sources.AnimeChap]: /\[Anime Chap\]/gi,
	[Sources.DKB]: /\[DKB\]/gi,
	[Sources.Judas]: /\[Judas\]/gi,
	[Sources.AnimeTime]: /\[Anime Time\]/gi,
	[Sources.ToonsHub]: /\[ToonsHub\]/gi,
};
const siCategoryEnglish = "1_2";
export default class TorrentUtils {
	static async search(anime: AnimeEntry, fetchAll: boolean = false, source: Sources = Sources.Any) {
		const searchNames = anime.namesForTorrentSearch;
		let searchTerm = searchNames.map(name => `"${name}"`).join("|");
		let uploader: Record<string, any> = (sourceToUploaderMapping as any)[source] || {},
			results: SearchResult[] & { searchedName?: string; } = [];
		if (uploader.si !== undefined) {
			let siUploader = uploader.si,
				isUploaderAnActualUser = uploader.isUser !== false;
			if (!isUploaderAnActualUser)
				searchTerm = searchNames.map(name => `"${siUploader}${name}"`).join("|");
			if (fetchAll)
				try {
					if (isUploaderAnActualUser)
						results.push(...(await si.searchAllByUser(siUploader, searchTerm, { category: siCategoryEnglish })).map(siResultToSearchResult));
					else
						results.push(...((await si.searchAll(searchTerm, { category: siCategoryEnglish })).map(siResultToSearchResult)));
				} catch (e) { console.error(e); }
			else
				try {
					if (isUploaderAnActualUser)
						results.push(...(await si.searchByUser(siUploader, searchTerm, DEFAULT_MAX_RESULTS, { category: siCategoryEnglish })).map(siResultToSearchResult));
					else
						results.push(...(await si.search(searchTerm, DEFAULT_MAX_RESULTS, { category: siCategoryEnglish })).map(siResultToSearchResult));
				} catch (e) { console.error(e); }
		}
		results.searchedName = searchTerm;
		return results;
	}

	static async torrentExtraInfo(torrentPageUrl: string | number): Promise<SearchResultExtraInfo> {
		if (typeof torrentPageUrl === "number") {
			const info = (await pantsu.infoRequest(torrentPageUrl));
			return {
				...info,
				fileList: info.file_list.map(ele => (ele as any).path),
				size: formatBytes(info.filesize),
				comments: info.comments.map(comment => ({
					author: comment.username,
					authorImage: comment.user_avatar,
					date: new Date(comment.date),
					text: comment.content
				}))
			};
		}
		const response = await fetch(torrentPageUrl),
			responseHTML = new DOMParser().parseFromString(await response.text(), 'text/html');
		return {
			description: responseHTML.querySelector("#torrent-description")?.textContent ?? "",
			size: [...responseHTML.querySelectorAll(".panel .row .col-md-5")].find(element => element.previousElementSibling?.textContent === "File size:")?.textContent ?? "",
			fileList: [...responseHTML.querySelectorAll(".torrent-file-list .fa.fa-file")].map(ele => ele.parentNode?.textContent ?? ""),
			comments: [...responseHTML.querySelectorAll(".comment-panel")].map(element => ({
				text: element.querySelector(".comment-content")?.textContent ?? "",
				author: element.querySelector("a[title=\"User\"]")?.textContent ?? "",
				authorImage: element.querySelector("img")?.src ?? "",
				date: new Date(element.querySelector("[data-timestamp]")?.textContent ?? "")
			}))
		};
	}

	static async latest(page = 1, source: Sources = Sources.Any): Promise<SearchResult[]> {
		const results: SearchResult[] = [];
		switch (source) {
			case Sources.Any:
				results.push(...(await si.search(' ', DEFAULT_MAX_RESULTS, { p: page, category: siCategoryEnglish })).map(siResultToSearchResult)
					.filter(ele => !results.some(result => result.link.magnet === ele.link.magnet)));
				return results;
		}
		return await this.getLatestOfSource(page, source);
	}
	private static async getLatestOfSource(page: number, source: Sources): Promise<SearchResult[]> {
		let uploader: Record<string, any> = (sourceToUploaderMapping as any)[source] ?? {},
			results = [],
			isUploaderAnActualUser = uploader.isUser !== false;
		if (uploader.si)
			try {
				if (isUploaderAnActualUser)
					results.push(...(await si.searchByUserAndByPage(uploader.si, '', page, DEFAULT_MAX_RESULTS, { category: siCategoryEnglish })).map(siResultToSearchResult));
				else
					results.push(...(await si.searchPage(`"${uploader.si}"`, page, { category: siCategoryEnglish, n: DEFAULT_MAX_RESULTS })).map(siResultToSearchResult));
			} catch (e) { console.error(e); }
		return results;
	}
}
function siResultToSearchResult(siResult: any): SearchResult {
	const obj = pantsuResultToSearchResult(siResult); //They changed it in the new update to look just like pantsu result!
	obj.link.page = `https://nyaa.si/view/${siResult.id}`;
	return obj;
}

function humanFileSize(size: number) {
	const i = size === 0 ? 0 : Math.floor(Math.log(size) / Math.log(1024));
	return Number((size / Math.pow(1024, i)).toFixed(1)) + ' ' + ['B', 'KiB', 'MiB', 'GiB', 'TiB'][i];
};

function pantsuResultToSearchResult(pantsuResult: any) {
	let categoryMapping: any = {
		"1_2": { label: "English-translated" },
		"6": { label: "Raw" },
		"13": { label: "Non-English-Translated" }
	};
	let result = new SearchResult({
		category: categoryMapping[pantsuResult.sub_category],
		name: pantsuResult.name,
		link: {
			magnet: pantsuResult.magnet,
			page: pantsuResult.id,
			file: pantsuResult.torrent
		},
		fileSize: typeof pantsuResult.filesize === "number" ? humanFileSize(pantsuResult.filesize) : pantsuResult.filesize,
		seeders: Number(pantsuResult.seeders),
		leechers: Number(pantsuResult.leechers),
		timestamp: new Date(pantsuResult.date),
		nbDownload: pantsuResult.completed
	});
	return result;
}


export type EpisodeData = {
	episodeOrMovieNumber: number;
	isBatch?: boolean;
	year: number;
	quality: number;
	name: string;
	anime_title: string;
	video_resolution?: string;
	release_version?: string;
	episode_number?: string;
	episode_number_alt?: string;
	episode_title?: string;
	anime_year?: string;
	other?: string;
	release_information?: string;
	video_term?: string;
	season_prefix?: string;
	anime_type?: string;
	audio_term?: string;
	device_compatibility?: string;
	episode_prefix?: string;
	file_checksum?: string;
	file_extension?: string;
	file_name?: string;
	language?: string;
	release_group?: string;
	source?: string;
	subtitles?: string;
	volume_prefix?: string;
	unknown?: string;
	anime_season?: string;
	volume_number: number;
};

function normalizeAnimeSeason(animeSeason?: string | string[]): string | undefined {
	if (!animeSeason)
		return "";
	if (typeof animeSeason === "string") {
		const asNumber = Number(animeSeason);
		return !isNaN(asNumber) ? asNumber.toString() : animeSeason;
	}
	const numbers = new Set(animeSeason.map(Number));
	return numbers.values().next().value?.toString();
}

export function episodeDataFromFilename(name: string) {
	const obj: EpisodeData = parseSync(name, { parse_episode_title: false, parse_release_group: false, parse_file_extension: false, allowed_delimiters: " _+,|." }) as any;
	const seasonOfEpisode = normalizeAnimeSeason(obj.anime_season);
	obj.anime_season = seasonOfEpisode ? `S${seasonOfEpisode}` : "";
	obj.anime_title = normalizeAnimeName(obj.anime_title + " " + obj.anime_season).trimEnd();
	obj.episodeOrMovieNumber = parseFloat(obj.episode_number?.toString() ?? obj.episode_number_alt?.toString() ?? "");
	obj.volume_number = parseFloat(obj.volume_number?.toString() ?? "");
	obj.year = parseFloat(obj.anime_year?.toString() ?? "");
	obj.isBatch = obj.release_information === "Batch";
	obj.quality = obj.video_resolution?.includes("x") ? parseFloat(obj.video_resolution.split("x")[1]) : parseFloat(obj.video_resolution?.toString() ?? "");
	obj.name = `${obj.anime_title}${obj.year ? ` (${obj.year})` : ""}${!isNaN(obj.episodeOrMovieNumber) ? ` Episode ${obj.episodeOrMovieNumber}` : ''}${obj.other ? " (" + obj.other + ")" : ''}`;
	return obj;
}

export function findSourceFromFilename(fileName: string): Sources {
	for (const [source, regex] of Object.entries(sourceToRegexMapping)) {
		if (fileName.match(regex!))
			return source as Sources;
	}
	return Sources.Any;
}

export enum DownloadStatus {
	notDownloaded,
	currentlyDownloading,
	downloaded
}

export class SearchResult {
	constructor(data: Partial<SearchResult>) {
		this.fileSize = data.fileSize ?? "";
		this.leechers = data.leechers ?? -1;
		this.link = data.link ?? {} as any;
		this.name = data.name ?? "";
		this.nbDownload = data.nbDownload ?? -1;
		this.seeders = data.seeders ?? -1;
		this.timestamp = data.timestamp ?? new Date(undefined as any);
		this.episodeData = data.episodeData ?? episodeDataFromFilename(this.name);
		this.animeEntry = data.animeEntry ?? new AnimeEntry({
			name: this.episodeData.anime_title
		});
		this.category = data.category;
		handleRelations(this);
	}
	category?: {
		label: string;
		code: string;
	};
	fileSize: string;
	leechers: number;
	link: {
		page: string;
		file: string;
		magnet: string;
	};
	name: string;
	nbDownload: number;
	seeders: number;
	timestamp: Date;
	episodeData: EpisodeData;
	animeEntry: AnimeEntry;
	reloadAnimeEntry() {
		this.animeEntry = new AnimeEntry({
			name: this.episodeData.anime_title
		});
	}
	seenThisEpisode() {
		return this.animeEntry && !isNaN(this.episodeData.episodeOrMovieNumber) && this.animeEntry.seenEpisode(this.episodeData.episodeOrMovieNumber);
	}
	compareEpisodeData = (episodeData: DownloadedItem["episodeData"]) => {
		return this.animeEntry.compareAnimeName(episodeData.anime_title) && (
			this.episodeData.name === episodeData.name || this.episodeData.episodeOrMovieNumber === episodeData.episodeOrMovieNumber);
	};
	compareAnimeEntry = (downloadedItem: DownloadedItem) => {
		return this.animeEntry.malId === downloadedItem.animeEntry.malId &&
			downloadedItem.episodeData.episodeOrMovieNumber === this.episodeData.episodeOrMovieNumber;
	};
	private get isDownloaded() {
		return Consts.DOWNLOADED_ITEMS.some(ele => this.compareAnimeEntry(ele)) || Consts.DOWNLOADED_ITEMS.some(item => this.compareEpisodeData(item.episodeData));
	}
	private get isDownloading() {
		return TorrentManager.getAll().some(torrent => {
			const names = [torrent.name, ...(torrent.files || []).map(ele => ele.name)];
			return names.some(name => this.compareEpisodeData(episodeDataFromFilename(name)));
		});
	}
	get downloadStatus(): DownloadStatus {
		return (this.isDownloaded ? DownloadStatus.downloaded : this.isDownloading ? DownloadStatus.currentlyDownloading : DownloadStatus.notDownloaded);
	}
}
