import React, { Component } from 'react';
import { Alert } from 'react-bootstrap';
import { HashRouter, Route, Switch } from "react-router-dom";
import "./classes/AnimeStorage";
import "./classes/Consts";
import Consts from './classes/Consts';
import "./css/global/App.css";
import "./css/global/Forum.css";
import DownloadManager from './pages/global/DownloadManager';
import { Login } from './pages/global/Login';
import ModalViewer from './pages/global/ModalViewer';
import NavBar from './pages/global/NavBar';
import ToastMessage from './pages/global/ToastMessages';
import Watch from './pages/global/Watch';
import routerConfig from "./routerConfig";

window.addEventListener("keydown", disableAltKey);
window.addEventListener("keyup", disableAltKey);
function disableAltKey(e) {
	if (e.key === "Alt")
		e.preventDefault();
}

export function reloadPage() {
	window.reloadPage();
}

export function setAppState(state) {
	window.setAppState(state);
}

export default class App extends Component {

	state = {
		videoItem: null,
		username: "",
		password: "",
		error: undefined,
		errorInfo: undefined
	};

	router = React.createRef();

	componentDidCatch(error, errorInfo) {
		this.setState({ error, errorInfo });
	}

	componentDidMount() {
		window.setAppState = this.setState.bind(this);
		window.reloadPage = this.forceUpdate.bind(this);

		window.addEventListener("beforeunload", () => {
			sessionStorage.setItem("lastPathname", this.router.current.history.location.pathname);
		});

		window.addEventListener("click", event => {
			const pressedLink = event.target?.href;
			if (pressedLink && new URL(pressedLink).origin !== window.location.origin) {
				event.preventDefault();
				window.open(pressedLink);
			}
		});

	}

	static loadLoginModal(username, password) {
		Consts.setWantsToLogin(true);
		window.setAppState({
			username,
			password
		});
	}

	render() {
		if (this.state.error && this.state.errorInfo.componentStack)
			return (
				<Alert>
					<Alert.Heading>
						Something Went Wrong!
					</Alert.Heading>
					I encountered an error... Please contact guydht at <a href="https://gitlab.com/guydht/Weeb-Friendly">https://gitlab.com/guydht/Weeb-Friendly</a>
					<br />
					{
						this.state.error.toString()
					}
					<br />
					{this.state.errorInfo.componentStack}
				</Alert>
			);
		return (
			<HashRouter ref={this.router} initialEntries={[sessionStorage.getItem("lastPathname") || ""]} initialIndex={0}>
				<NavBar />
				<div style={{ marginTop: 70, minHeight: "100vh" }}>
					<Switch>
						{
							Object.entries(routerConfig).map(([thePath, TheComponent]) => {
								return (
									<Route path={thePath} component={TheComponent} key={thePath} />
								);
							})
						}
					</Switch>
				</div>
				<Watch downloadedItem={this.state.videoItem} />
				<Login />
				<DownloadManager />
				<ToastMessage />
				<ModalViewer />
			</HashRouter>
		);
	}
}
