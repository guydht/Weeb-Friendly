import { reloadPage, setAppState } from "../App";
import { displayToast } from "../pages/global/ToastMessages";
import Watch from "../pages/global/Watch";
import { handleRelations } from "../utils/general";
import MALUtils from "../utils/MAL";
import { CacheLocalStorage } from "../utils/OutsourcedGeneral";
import { EpisodeData, episodeDataFromFilename } from "../utils/torrents";
import AnimeEntry from "./AnimeEntry";
import Consts from "./Consts";

const SKIP_TYPES = [
	'op',
	'ed',
	'mixed-op',
	'mixed-ed',
	'recap',
] as const;

export type SkipType = typeof SKIP_TYPES[number];

export type SkipTime = {
	interval: {
		startTime: number;
		endTime: number;
	};
	skipType: SkipType;
	skipId: string;
	episodeLength: number;
};
const trash = window.require("trash"),
	path = window.require("path");


export default class DownloadedItem {
	videoSrc: string;
	constructor(absolutePath: string, fileName: string, lastUpdated: Date, animeEntry?: AnimeEntry, episodeData?: Omit<EpisodeData, 'quality'>) {
		this.absolutePath = absolutePath;
		this.fileName = fileName;
		this.lastUpdated = lastUpdated;
		this.episodeData = episodeData ?? (episodeDataFromFilename(this.fileName) || {} as any);
		if (this.episodeData.anime_title === this.episodeData.name) {
			let titleFromFolderName = episodeDataFromFilename(path.basename(path.dirname(this.absolutePath))).anime_title;
			const newName = `${titleFromFolderName} ${this.fileName.replace(/-/g, " - ").replace(/\s\s/g, " ")}`;
			const newEpisodeData = episodeDataFromFilename(newName);
			if (newEpisodeData.anime_title !== newEpisodeData.name)
				this.episodeData = newEpisodeData;
		}
		this.animeEntry = animeEntry ?? new AnimeEntry({ name: this.episodeData.anime_title });
		this.videoSrc = Consts.FILE_URL_PROTOCOL + this.absolutePath;
		handleRelations(this as any);
	}
	absolutePath: string;
	fileName: string;
	lastUpdated: Date;
	animeEntry: AnimeEntry;
	episodeData: Omit<EpisodeData, 'quality'>;
	seenThisEpisode() {
		(window as any).CacheLocalStorage = CacheLocalStorage;
		if (!isNaN(this.episodeData.episodeOrMovieNumber) && this.animeEntry.seenEpisode(this.episodeData.episodeOrMovieNumber))
			return true;
		const savedVideoProgress = new CacheLocalStorage("videoLastTime").getItem(this.episodeData.name);
		return savedVideoProgress && savedVideoProgress.progress > Watch.UPDATE_ANIME_PROGRESS_THRESHOLD;
	}
	startPlaying() {
		setAppState({
			videoItem: this
		});
	}
	async skipTimes() {
		const types = ["op", "ed", "mixed-op", "mixed-ed"];
		if (!this.animeEntry.malId) return [];
		const skipTimes = await fetch(`https://api.aniskip.com/v2/skip-times/${this.animeEntry.malId
			}/${this.episodeData.episodeOrMovieNumber
			}?types=${types.join("&types=")
			}&episodeLength=0`).then(r => r.json());
		if (skipTimes.found && skipTimes.results?.length)
			return skipTimes.results as SkipTime[];
		return [];
	}
	equals(other: DownloadedItem) {
		const sameSeries = (this.animeEntry.malId && this.animeEntry.malId === other.animeEntry.malId) ||
			this.episodeData.anime_title === other.episodeData.anime_title;
		const sameEpisode = this.episodeData.episodeOrMovieNumber === other.episodeData.episodeOrMovieNumber;
		return sameSeries && sameEpisode;
	}
	async delete(reloadDownloads = true): Promise<void> {
		await trash(this.absolutePath);
		displayToast({
			title: 'Moved file to trash',
			body: `Moved ${this.fileName} to the trash.`
		});
		if (reloadDownloads)
			Consts.reloadDownloads().then(() => reloadPage());
	}
	async searchInMal(): Promise<AnimeEntry | undefined> {
		let results = await MALUtils.searchAnime(this.animeEntry);
		const similar = results.filter(result => result.compareAnimeNames(this.animeEntry));
		if (similar.length === 1) {
			this.animeEntry.malId = similar[0].malId;
			this.animeEntry.syncPut();
		}
		return this.animeEntry;
	}
}
