import { UpdateAnimeParams } from "node-myanimelist/typings/methods/malApi/anime/index";
export enum MALStatuses {
	Watching = 1,
	Completed = 2,
	"On-Hold" = 3,
	Dropped = 4,
	"Plan To Watch" = 6
};

export const statusMapping: Record<MALStatuses, UpdateAnimeParams["status"]> = {
	[MALStatuses.Watching]: "watching",
	[MALStatuses.Completed]: "completed",
	[MALStatuses.Dropped]: "dropped",
	3: "on_hold",
	6: "plan_to_watch"
};
export const reverseStatusMapping: Record<Exclude<UpdateAnimeParams["status"], undefined>, MALStatuses> =
	Object.fromEntries(Object.entries(statusMapping).map(([a, b]) => [b, Number(a)]));
