import { compareAnimeName, normalizeAnimeName } from "../utils/OutsourcedGeneral";
import { ThumbnailManager, get, sync } from "./AnimeStorage";
import { MALStatuses } from "./MalStatuses";
const fs = window.require("fs"),
	http = window.require("http"),
	https = window.require("https");
export default class AnimeEntry {
	static SCORES = ["Appaling", "Horrible", "Very Bad", "Bad", "Average", "Fine", "Good", "Very Good", "Great", "Masterpiece"];
	constructor({
		synonyms = undefined,
		malId = undefined,
		score = undefined,
		genres = undefined,
		totalEpisodes = undefined,
		startDate = undefined,
		endDate = undefined,
		userStartDate = undefined,
		userEndDate = undefined,
		myWatchedEpisodes = undefined,
		myMalStatus = undefined,
		myMalRating = undefined,
		myRewatchAmount = undefined,
		userComments = undefined,
		imageURL = undefined,
		_imageURL = undefined,
		name = undefined,
		_name = undefined,
		sync = true
	}: {
		synonyms?: Set<string>,
		malId?: number,
		score?: number,
		genres?: Set<string>,
		synopsis?: string,
		totalEpisodes?: number,
		startDate?: Date,
		endDate?: Date,
		userStartDate?: Date,
		userEndDate?: Date,
		myWatchedEpisodes?: number,
		myMalStatus?: MALStatuses,
		myMalRating?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10,
		myRewatchAmount?: number,
		userComments?: string,
		imageURL?: string,
		_imageURL?: string,
		name?: string,
		_name?: string,
		sync?: boolean;
	}) {
		this.malId = malId;
		this.name = normalizeAnimeName(name || _name);
		this.synonyms = synonyms ? new Set(Array.from(synonyms || []).sort()) : this.synonyms;
		if (sync && (malId || this.name))
			this.syncGet();
		this.score = score ?? this.score;
		this.genres = genres ? new Set(Array.from(genres || [])) : this.genres;
		this.totalEpisodes = totalEpisodes ?? this.totalEpisodes;
		this.startDate = startDate ? new Date(startDate) : this.startDate;
		this.endDate = endDate ? new Date(endDate) : this.endDate;
		this.userStartDate = userStartDate ? new Date(userStartDate) : this.userStartDate;
		this.userEndDate = userEndDate ? new Date(userEndDate) : this.userEndDate;
		this.myWatchedEpisodes = myWatchedEpisodes ?? this.myWatchedEpisodes;
		this.myMalStatus = myMalStatus ?? this.myMalStatus;
		this.myMalRating = myMalRating ?? this.myMalRating;
		this.myRewatchAmount = myRewatchAmount ?? this.myRewatchAmount;
		this.userComments = userComments ?? this.userComments;
		this.imageURL = ((imageURL || _imageURL) ?? this.imageURL)?.replace(/\/r\/[0-9]+x[0-9]+/g, '');
	}
	_synonyms = new Set<string>();
	get synonyms() {
		return this._synonyms;
	}
	set synonyms(value: Set<string>) {
		this._synonyms = new Set([...value].map(normalizeAnimeName).filter(ele => ele));
	}
	private * getNames() {
		if (this._name)
			yield this._name;
		yield* this.synonyms;
	}
	get namesForTorrentSearch() {
		const names = this.names;
		[...names].forEach(name => {
			const parsed = parseAnimeTitle(name);
			if (parsed.season)
				names.push(...parsed.asTorrentFriendlyAnimeNames);
			// names[index] = `"${name}"|"${parsed.asTorrentFriendlyAnimeName}"`;
		});
		return names.filter((ele, index, arr) => arr.indexOf(ele) === index);
	}
	get names() { // Property for when you want to go over the names of the anime in a for loop
		return [...this.getNames()];
	}
	compareAnimeName(animeName: string) {
		if (this.name && compareAnimeName(this.name, animeName)) return true;
		for (const name of this._synonyms)
			if (compareAnimeName(name, animeName))
				return true;
		return false;
	}
	compareAnimeNames(other: AnimeEntry) {
		if (other.name && this.compareAnimeName(other.name)) return true;
		for (const name of other._synonyms)
			if (this.compareAnimeName(name))
				return true;
		return false;
	}
	malId?: number;
	score?: number;
	get malUrl(): string {
		return `https://myanimelist.net/anime/${this.malId}/${this.name}`;
	};
	genres?: Set<String>;
	totalEpisodes?: number;
	startDate?: Date;
	endDate?: Date;
	userStartDate?: Date;
	userEndDate?: Date;
	myWatchedEpisodes?: number;
	myMalStatus?: MALStatuses;
	myMalRating?: 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10;
	userComments?: string;
	myRewatchAmount?: number;
	private _imageURL?: string;
	get imageURL() {
		return ThumbnailManager.SAVED_THUMBNAILS_STATE && this.malId && ThumbnailManager.SAVED_THUMBNAILS.has(this.malId) ?
			"file://" + ThumbnailManager.SAVED_THUMBNAILS_PATH + this.malId : this._imageURL;
	}
	set imageURL(value) {
		this._imageURL = value;
		if (ThumbnailManager.SAVED_THUMBNAILS_STATE && this.malId && !ThumbnailManager.SAVED_THUMBNAILS.has(this.malId) && this._imageURL) {
			const writeStream = fs.createWriteStream(ThumbnailManager.SAVED_THUMBNAILS_PATH + this.malId);
			const protocolToUse = this._imageURL.startsWith("https") ? https : http;
			protocolToUse.get(this._imageURL, (response: any) => {
				response.pipe(writeStream);
				writeStream.on("finish", () => {
					ThumbnailManager.SAVED_THUMBNAILS.add(this.malId!);
					writeStream.close();
				});
			}).on("error", (err: any) => {
				console.error(err);
				writeStream.close(() => {
					fs.unlink(ThumbnailManager.SAVED_THUMBNAILS_PATH + this.malId, () => {});
				});
			});
		}
	}
	_name?: string;
	get name() {
		return this._name || this._synonyms.values().next().value;
	}
	set name(value: string | undefined) {
		this._name = normalizeAnimeName(value) || this._name;
	}
	syncPut(forceData: boolean = false) {
		sync(this, forceData);
		return this.syncGet();
	}
	readyForJSON() {
		let copy: Record<string, any> = { ...this };
		copy.synonyms = [...this.synonyms];
		return copy;
	}
	syncGet() {
		let inStorage = get(this);
		if (inStorage)
			Object.entries(inStorage).forEach(([key, value]) => {
				if (value !== this[key as keyof AnimeEntry])
					(this as any)[key] = value;
			});
		return this;
	}

	seenEpisode(episodeNumber: number) {
		return this.myWatchedEpisodes !== undefined && this.myWatchedEpisodes >= episodeNumber;
	}

	isUserInterested() {
		return this.myMalStatus !== undefined && (
			this.myMalStatus === MALStatuses.Completed ||
			this.myMalStatus === MALStatuses.Watching ||
			this.myMalStatus === MALStatuses["Plan To Watch"]
		);
	}

	clearUserData() {
		delete this.myMalRating;
		delete this.myMalStatus;
		delete this.myRewatchAmount;
		delete this.myWatchedEpisodes;
		delete this.userComments;
		delete this.userEndDate;
		delete this.userStartDate;
	}
}

(window as any).animeEntry = AnimeEntry;
const seasonWordToNumberMapping = {
	"first": 1,
	"second": 2,
	"third": 3,
	"fourth": 4,
	"fifth": 5,
	"sixth": 6,
	"seventh": 7,
	"eigth": 8,
	"ninth": 9,
	"one": 1,
	"two": 2,
	"three": 3,
	"four": 4,
	"five": 5,
	"six": 6,
	"seven": 7,
	"eight": 8,
	"nine": 9,
	"2nd": 2,
	"3rd": 3,
	"4th": 4,
	"5th": 5,
	"6th": 6,
	"7th": 7,
	"8th": 8,
	"9th": 9,
};
const seasonRegexToNumberMappings: Map<RegExp, number> = new Map(Object.entries(seasonWordToNumberMapping).map(([key, val]) => {
	const reg = new RegExp(`S0*${val}|(season )?${key}( season)?`, "gi");
	return [reg, val];
}));
// Returns an object containing the anime name and its season.
// For example:
//  >> matchSeasonName("aot second season")
//  ... {title: "aot", season: "2"}
class ParsedAnimeTitle {
	title: string;
	season?: number;
	constructor(title: string, season?: number) {
		this.title = title.trim();
		this.season = season;
	}
	get asTorrentFriendlyAnimeNames() {
		return [`${this.title} S${this.season}`, `${this.title} S${this.season?.toString().padStart(2, "0")}`,
		`${this.title} - S${this.season}`, `${this.title} - S${this.season?.toString().padStart(2, "0")}`, this.title];
	}
}
function parseAnimeTitle(animeName: string): ParsedAnimeTitle {
	const matchedSeasonEntry: [RegExp, number] | undefined = [...seasonRegexToNumberMappings.entries()].find(([regex]) => {
		return animeName.match(regex);
	}) as any;
	if (!matchedSeasonEntry)
		return new ParsedAnimeTitle(animeName);
	const title = animeName.replace(matchedSeasonEntry[0], "");
	return new ParsedAnimeTitle(title, matchedSeasonEntry[1]);
}
(window as any).parseAnimeTitle = parseAnimeTitle;
(window as any).animeMapping = seasonRegexToNumberMappings;