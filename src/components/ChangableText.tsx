import React, { ChangeEvent, Component, KeyboardEvent } from "react";
import FormControl from "react-bootstrap/FormControl";
import styles from "../css/components/ChangableText.module.css";
import CloseButton from "./CloseButton";
import { SmartBlur } from "./SmartBlur";

export interface ChangableTextProps {
	text: string;
	onChange?: (value: string) => void;
	removeButton?: boolean;
	removeButtonTooltipText?: string;
};


export default class ChangableText extends Component<ChangableTextProps & Omit<
	JSX.IntrinsicElements["span"],
	keyof ChangableTextProps
>> {

	state = {
		isChanging: false,
		text: this.props.text
	};

	render() {
		let props: any = { ...this.props };
		delete props.text;
		delete props.onChange;
		delete props.onClick;
		delete props.removeButton;
		delete props.removeButtonTooltipText;
		if (this.state.isChanging)
			return (
				<SmartBlur style={{ position: "relative", ...props.style }}
					onBlur={() => this.onBlur()}>
					<FormControl type="text" value={this.state.text}
						className={styles.inputElement}
						style={{ width: ChangableText.widhtOfText(this.state.text, "16px") + "px" }}
						onChange={(e: ChangeEvent<HTMLInputElement>) => this.onTextChange(e)}
						autoFocus={true}
						onKeyDown={(e: KeyboardEvent) => this.onKeyPress(e)} />
					<CloseButton style={{ right: "7px", top: "-2px", display: this.props.removeButton === false ? "none" : "" }} tooltipText={this.props.removeButtonTooltipText} toolTipPlacement="top" onClick={this.deleteText.bind(this)} />
				</SmartBlur>
			);
		return (
			<span {...props} onClick={e => {
				this.props.onClick && this.props.onClick(e);
				this.setState({ text: this.props.text });
				this.startTextChange();
			}}
				className={styles.textWrapper}>{this.state.text || this.props.text || this.props.defaultValue}</span>
		);
	}

	onTextChange(e: ChangeEvent<HTMLInputElement>) {
		this.setState({
			text: e.target.value
		});
	}

	onKeyPress(e: KeyboardEvent) {
		e.stopPropagation();
		e.nativeEvent.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
		if (e.key === "Enter")
			this.submitText();
	}

	onBlur() {
		setImmediate(() => {
			if (!this.deleted)
				this.submitText();
		});
	}

	submitText() {
		this.setState({
			isChanging: false
		});
		if (this.props.onChange && this.state.text !== this.props.text)
			this.props.onChange(this.state.text);
	}

	startTextChange() {
		this.setState({ isChanging: true });
	}

	static widhtOfText(text: string, fontSize: string = "1em") {
		let tmp = document.createElement("span");
		tmp.append(new Text(text));
		(tmp as any).style = `font-size: ${fontSize}; width: fit-content; position: fixed; white-space: pre;`;
		document.body.append(tmp);
		let width = tmp.offsetWidth;
		tmp.remove();
		return width;
	}
	deleted = false;
	deleteText() {
		// eslint-disable-next-line
		this.state.text = "";
		this.deleted = true;
		this.submitText();
	}

}
