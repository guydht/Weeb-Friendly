import React, { useCallback, useContext, useEffect, useMemo, useRef, useState } from "react";
import DownloadedItem, { SkipTime } from "../../classes/DownloadedItem";
import { waitFor } from "../../jsHelpers/jifa";
import { CacheLocalStorage, scrollSmoothly } from "../../utils/OutsourcedGeneral";
import { secondsToTimeDisplay } from "../../utils/general";
import { AnimatedSVG } from "../AnimatedSVG";
import { loadingComponent } from "../LoadingComponent";
import { addToQueue } from "../VideoThumbnail";
import styles from "./CastedPlayer.module.css";
import Comments from "./Comments";
import { SkipButton } from "./SkipButton";

const SYNC_INTERVAL_MILISECONDS = 1500;

type PlayersControls = {
    togglePlayPause: () => void;
    setVolume: (volume: number) => void;
    progressTo: (seconds: number) => void;
    changeSub: (subtitleId: string) => void;
    getSubs: () => any[];
    addSubs: (subtitleContent: string, subtitleFileName: string) => Promise<void>;
    removeSubs: () => Promise<void>;
    getActiveSubId: () => string;
    currentTime: number;
    volume: number;
    duration: number;
    paused: boolean;
};

type Callable = (...args: any[]) => any;
type PromisedCallable<T extends Callable> = (...args: Parameters<T>) => Promise<ReturnType<T>>;

type AsyncPlayerControls = {
    [K in keyof PlayersControls]: PlayersControls[K] extends Callable ? PromisedCallable<PlayersControls[K]> : Promise<PlayersControls[K]>;
};

type Controls = PlayersControls | AsyncPlayerControls;

const VideoData = React.createContext<{
    currentTime: number;
    duration: number;
    paused: boolean;
    setCurrentTime?: (currentTime: number) => void;
    setDuration?: (duration: number) => void;
    setIsPaused?: (isPaused: boolean) => void;
}>({
    currentTime: 0,
    duration: 0,
    paused: true,
});

const PlayPauseButton: React.FC<{ controls: Controls; }> = ({ controls }) => {
    const { paused, setIsPaused } = useContext(VideoData);

    const onClick = () => {
        setIsPaused!(!paused);
        controls.togglePlayPause();
    };

    return (
        <div onClick={onClick} className={styles.playPauseButton}>
            <AnimatedSVG
                from="M 12,26 16,26 16,10 12,10 z M 21,26 25,26 25,10 21,10 z"
                to="M 12,26 18.5,22 18.5,14 12,10 z M 18.5,22 25,18 25,18 18.5,14 z"
                doFlip={paused}
            />
        </div>
    );
};

const NextButton: React.FC = () => {
    return (
        <div className={styles.next} id="guydhtNextEpisodeButton" />
    );
};

const PreviousButton: React.FC = () => {
    return (
        <div className={styles.previous} id="guydhtPrevEpisodeButton" />
    );
};

function absPosition(element: HTMLElement) {
    var bound = element.getBoundingClientRect().toJSON();
    var windowHeight = Number(window.outerHeight),
        windowWidth = Number(window.outerWidth);
    var windowTop = Number(window.screenY),
        windowLeft = Number(window.screenX),
        windowRight = Number(window.screen.width - (windowLeft + windowWidth)),
        windowBottom = Number(window.screen.height - (windowTop + windowHeight));
    return {
        left: windowLeft + bound.left,
        top: windowTop + bound.top,
        right: windowRight + bound.right,
        bottom: windowBottom + bound.bottom,
    };
}


var previousPosition: DOMRect = {} as any, containerStyle: ReturnType<typeof absPosition> = {} as any, fullScreenRect: DOMRect = {} as any, fullscreenWait: ReturnType<typeof waitFor>, finishFullscreenExitTimeout: ReturnType<typeof waitFor>;
function enterFullscreenMode(wrapper: HTMLElement) {
    clearInterval(fullscreenWait);
    clearTimeout(finishFullscreenExitTimeout);
    previousPosition = wrapper.getBoundingClientRect().toJSON();
    containerStyle = absPosition(wrapper);
    wrapper.style.transition = "all 0s";
    wrapper.style.height = previousPosition.height + "px";
    wrapper.style.width = previousPosition.width + "px";
    wrapper.style.left = containerStyle.left + "px";
    wrapper.style.top = containerStyle.top + "px";
    document.body.requestFullscreen();
    fullscreenWait = waitFor(function () {
        return document.fullscreenElement !== null;
    }, function () {
        wrapper.style.transition = "all .5s";
        wrapper.style.height = "100%";
        wrapper.style.width = "100%";
        ["top", "left", "right", "bottom"].forEach(prop => wrapper.style[prop as any] = "0px");
        wrapper.style.border = "none";
        finishFullscreenExitTimeout = setTimeout(() => {
            fullScreenRect = wrapper.getBoundingClientRect().toJSON();
        }, 500);
    }, 1);
    wrapper.classList.add(styles.fullscreened);
}

function exitFullscreenMode(wrapper: HTMLElement) {
    clearInterval(fullscreenWait);
    clearTimeout(finishFullscreenExitTimeout);
    wrapper.style.position = "fixed";
    wrapper.style.transition = "all 0s";
    for (var i in containerStyle) {
        (wrapper.style as any)[i] = ((previousPosition as any)[i] - (containerStyle as any)[i]) + "px";
    }
    wrapper.style.height = fullScreenRect.height + "px";
    wrapper.style.width = fullScreenRect.width + "px";
    wrapper.style.border = "1px solid rgba(20, 80, 170, 0.7)";
    document.exitPointerLock();
    wrapper.classList.remove(styles.fullscreened);
    if (document.fullscreenElement !== null) document.exitFullscreen();
    setImmediate(function () {
        wrapper.style.transition = "all 0.5s";
        for (var i in previousPosition)
            (wrapper.style as any)[i] = (previousPosition as any)[i] + "px";
    });
    finishFullscreenExitTimeout = setTimeout(function () {
        (wrapper.style as any) = "transition: all 0s; border: 1px solid rgba(20, 80, 170, 0.7);";
    }, 500);
}
function toggleFullScreen(wrapperElement: HTMLElement) {
    if (document.fullscreenElement !== null)
        exitFullscreenMode(wrapperElement);
    else {
        enterFullscreenMode(wrapperElement);
    }
}

function useAsyncedValue<T extends Object>(valueGetter: () => (T | Promise<T>), defaultValue: T) {
    const value = valueGetter();
    const isInstant = value.constructor !== Promise;
    const [instantValue, setValue] = useState(isInstant ? value as T : defaultValue);
    const lastAwaitedTime = useRef(Date.now());

    useEffect(() => {
        if (!isInstant) {
            const doSync = async () => {
                if (Date.now() - lastAwaitedTime.current < SYNC_INTERVAL_MILISECONDS) return;
                lastAwaitedTime.current = Infinity;
                const currentValue = await valueGetter();
                lastAwaitedTime.current = Date.now();
                setValue(currentValue);
            };
            let keepIntervalGoing = true;
            const syncInIntervals = async () => {
                await doSync();
                await new Promise(resolve => setTimeout(resolve, SYNC_INTERVAL_MILISECONDS * 2));
                if (keepIntervalGoing)
                    window.requestAnimationFrame(syncInIntervals);
            };
            syncInIntervals();
            return () => {
                keepIntervalGoing = false;
            };
        }
    }, [value, isInstant, valueGetter]);
    return [instantValue, setValue] as const;
}

function useAsyncedTime(time: () => (number | Promise<number>), isStatic?: boolean) {
    const [displayTime, setDisplayTime] = useAsyncedValue(time, 0);
    const isInstant = time.constructor !== Promise;

    useEffect(() => {
        if (isInstant || isStatic)
            return;
        let startDate = Date.now();
        const visualInterval = setInterval(() => {
            const now = Date.now(),
                diff = Math.floor((now - startDate) / 10) / 100;
            startDate = now;
            setDisplayTime(currentTime => currentTime! + diff);
        }, 150);
        return () => {
            clearInterval(visualInterval);
        };
    }, [time, isInstant, setDisplayTime, isStatic]);

    return [displayTime, setDisplayTime] as const;
}

const ProgressSlider: React.FC<{ controls: Controls; setTooltipPosition: (leftAlignment: number, progressRect?: DOMRect) => void; }> = ({ controls, setTooltipPosition }) => {
    const { currentTime, duration, setCurrentTime } = useContext(VideoData);
    const progressRatio = currentTime / (duration || 1);

    const onMouseMove: React.MouseEventHandler<HTMLDivElement> = e => {
        setTooltipPosition(e.clientX, e.currentTarget.getBoundingClientRect());
    };
    const onMouseLeave: React.MouseEventHandler = () => {
        setTooltipPosition(0);
    };
    const onClick: React.MouseEventHandler<HTMLDivElement> = async (e) => {
        const rect = e.currentTarget.getBoundingClientRect();
        const timeClicked = (e.clientX - rect.left) / rect.width * duration;
        await controls.progressTo(timeClicked);
        setCurrentTime!(timeClicked);
    };

    return (
        <div className={styles.progressContainer}>
            <div className={styles.progressWrapper} onMouseMove={onMouseMove} onMouseLeave={onMouseLeave} onClick={onClick}>
                <div className={styles.progressShadow}></div>
                <div className={styles.progress} style={useMemo(() => ({ transform: `scaleX(${progressRatio})` }), [progressRatio])}></div>
                <div className={styles.progressCircle} style={useMemo(() => ({ left: `${progressRatio * 100}%` }), [progressRatio])}></div>
            </div >
        </div>
    );
};

const TimeDisplayer: React.FC = () => {
    const { currentTime, duration } = useContext(VideoData);
    const currentTimeSeconds = useMemo(() => secondsToTimeDisplay(currentTime), [currentTime]);
    const durationSeconds = useMemo(() => secondsToTimeDisplay(duration), [duration]);

    return (
        <div className={styles.timeDisplayer}>
            {currentTimeSeconds}/{durationSeconds}
        </div>
    );
};

const TOOLTIP_THUMBNAIL_CHUNK_IN_SECONDS = 10,
    TOOLTIP_THUMBNAIL_DEFAULT_HEIGHT = 100;

const TooltipThumbnail: React.FC<{ tooltipPosition: { pixels: number, progress: number; }; src?: string; }> = ({ tooltipPosition, src }) => {
    const canvasRef = useRef<HTMLCanvasElement>(null);
    const tooltipPositionChunked = Math.floor(tooltipPosition.progress / TOOLTIP_THUMBNAIL_CHUNK_IN_SECONDS) * TOOLTIP_THUMBNAIL_CHUNK_IN_SECONDS;
    const [canvasHeight, setCanvasHeight] = useState(TOOLTIP_THUMBNAIL_DEFAULT_HEIGHT);
    const canvasWidth = canvasHeight * 1.777;

    const canvasCache = useMemo(
        () => new Map<number, OffscreenCanvas>(),
        // New cache when src changes
        //eslint-disable-next-line
        [src]);
    useEffect(() => {
        const canvasContext = canvasRef.current?.getContext("2d");
        const timeToRender = tooltipPositionChunked;
        if (canvasCache.has(timeToRender)) {
            canvasContext?.drawImage(canvasCache.get(timeToRender)!, 0, 0);
        }
        else if (src) {
            canvasContext?.clearRect(0, 0, canvasWidth, canvasHeight);
            addToQueue({
                videoUrl: src,
                returnRealCanvas: false,
                renderedHeight: canvasHeight,
                renderedWidth: canvasWidth,
                snapshotTime: timeToRender,
            }, 0, "player", 10).then(offscreenCanvas => {
                canvasCache.set(timeToRender, offscreenCanvas);
                if (timeToRender === tooltipPositionChunked)
                    canvasContext?.drawImage(offscreenCanvas, 0, 0);
            });
        }
    }, [src, tooltipPositionChunked, canvasHeight, canvasWidth, canvasCache]);

    useEffect(() => {
        document.addEventListener("fullscreenchange", () => {
            // if (document.fullscreenElement)
            //     setCanvasHeight(TOOLTIP_THUMBNAIL_DEFAULT_HEIGHT * 2);
            // else
            setCanvasHeight(TOOLTIP_THUMBNAIL_DEFAULT_HEIGHT);
        });
    }, []);

    return (
        <div className={styles.tooltipThumbnail} style={{ left: tooltipPosition.pixels, display: tooltipPosition.pixels === 0 ? "none" : "" }}>
            <canvas ref={canvasRef} height={canvasHeight} width={canvasWidth} />
        </div>
    );
};

const SettingsMenu: React.FC<{ controls: Controls; }> = ({ controls }) => {
    const [menuVisible, setMenuVisibility] = useState(false);
    const [secondListContent, setSecondListContent] = useState<React.ReactNode>(null);
    const toggleMenuVisibility = () => {
        setMenuVisibility(x => !x);
    };
    const loadLocalSubs = async () => {
        const input = document.createElement("input");
        input.type = "file";
        input.click();
        input.multiple = false;
        input.accept = "text/plain";
        input.onchange = () => {
            const reader = new FileReader();
            if ((input.files?.length ?? 0) > 0)
                reader.readAsText(input.files![0], 'UTF-8');
            reader.onload = async () => {
                const content = reader.result;
                if (content) {
                    await controls.addSubs(content.toString(), input.files![0].name);
                    await gotoSubtitleChooseMenu();
                }
            };
        };
    };

    const onSubClick = (index: number) => {
        controls.changeSub(index as any);
        gotoSubtitleChooseMenu();
    };

    const gotoSubtitleChooseMenu = async () => {
        const subs = await controls.getSubs();
        const activeSubId = await controls.getActiveSubId();
        setSecondListContent(
            <>
                {
                    subs.map((sub, index) => (
                        <li onClick={() => onSubClick(index)} key={sub.name}>
                            {sub.title} {activeSubId.toString() === index.toString() && "- Active"}
                        </li>
                    ))
                }
                <li onClick={loadLocalSubs}>Load From Local File</li>
            </>

        );
    };

    return (
        <div className={styles.settings} onClick={toggleMenuVisibility}>
            <div onClick={e => e.stopPropagation()} onMouseUp={e => e.stopPropagation()} className={styles.settingsMenu} style={{ opacity: menuVisible ? 1 : 0, pointerEvents: menuVisible ? "all" : "none" }}>
                <div className={styles.settingsWindow}>
                    <ul style={{ transform: secondListContent !== null ? "translateX(-100%)" : "", maxHeight: secondListContent !== null ? 0 : undefined }}>
                        <li><div>Subs Delay (Seconds)</div><input className={styles.subsDelayChooser} type="number" step="0.5" /></li>
                        <li><div onClick={gotoSubtitleChooseMenu}>Choose Subtitle Track</div></li>
                        <li><div>Choose Audio Track</div></li>
                    </ul>
                    <ul style={{ "margin": 0, transform: secondListContent !== null ? "none" : "", maxHeight: secondListContent !== null ? undefined : 0 }} >
                        <li onClick={() => setSecondListContent(null)} className={styles.settingsListGoBack}><div>back</div></li>
                        {secondListContent}
                    </ul>
                </div>
            </div>
            <svg viewBox="0 0 36 36" height="100%" width="35px">
                <path d="m 23.94,18.78 c .03,-0.25 .05,-0.51 .05,-0.78 0,-0.27 -0.02,-0.52 -0.05,-0.78 l 1.68,-1.32 c .15,-0.12 .19,-0.33 .09,-0.51 l -1.6,-2.76 c -0.09,-0.17 -0.31,-0.24 -0.48,-0.17 l -1.99,.8 c -0.41,-0.32 -0.86,-0.58 -1.35,-0.78 l -0.30,-2.12 c -0.02,-0.19 -0.19,-0.33 -0.39,-0.33 l -3.2,0 c -0.2,0 -0.36,.14 -0.39,.33 l -0.30,2.12 c -0.48,.2 -0.93,.47 -1.35,.78 l -1.99,-0.8 c -0.18,-0.07 -0.39,0 -0.48,.17 l -1.6,2.76 c -0.10,.17 -0.05,.39 .09,.51 l 1.68,1.32 c -0.03,.25 -0.05,.52 -0.05,.78 0,.26 .02,.52 .05,.78 l -1.68,1.32 c -0.15,.12 -0.19,.33 -0.09,.51 l 1.6,2.76 c .09,.17 .31,.24 .48,.17 l 1.99,-0.8 c .41,.32 .86,.58 1.35,.78 l .30,2.12 c .02,.19 .19,.33 .39,.33 l 3.2,0 c .2,0 .36,-0.14 .39,-0.33 l .30,-2.12 c .48,-0.2 .93,-0.47 1.35,-0.78 l 1.99,.8 c .18,.07 .39,0 .48,-0.17 l 1.6,-2.76 c .09,-0.17 .05,-0.39 -0.09,-0.51 l -1.68,-1.32 0,0 z m -5.94,2.01 c -1.54,0 -2.8,-1.25 -2.8,-2.8 0,-1.54 1.25,-2.8 2.8,-2.8 1.54,0 2.8,1.25 2.8,2.8 0,1.54 -1.25,2.8 -2.8,2.8 l 0,0 z" />
            </svg>
        </div>
    );
};

const FullscreenButton: React.FC<{ wrapperRef: React.RefObject<HTMLDivElement>; }> = ({ wrapperRef }) => {
    const onClick: React.MouseEventHandler = () => {
        toggleFullScreen(wrapperRef.current!);
    };
    return (
        <div className={styles.fullScreen} onClick={onClick}>
            <svg height="100%" width="100%">
                <path d="m 10,16 2,0 0,-4 4,0 0,-2 L 10,10 l 0,6 0,0 z" />
                <path d="m 20,10 0,2 4,0 0,4 2,0 L 26,10 l -6,0 0,0 z" />
                <path d="m 24,24 -4,0 0,2 L 26,26 l 0,-6 -2,0 0,4 0,0 z" />
                <path d="M 12,20 10,20 10,26 l 6,0 0,-2 -4,0 0,-4 0,0 z" />
            </svg>
        </div>
    );
};

const CommentsContainer = React.forwardRef<HTMLDivElement, { episode?: DownloadedItem; }>(({ episode }, ref) => {
    return (
        <div className={styles.commentsContainer} ref={ref} onMouseUp={e => e.stopPropagation()}>
            {
                episode && <Comments key={episode.absolutePath} episode={episode} />
            }
        </div>
    );
});

const SkipButtonContainer = loadingComponent<{ controls: Controls; episode: DownloadedItem; }, { skipTimes: SkipTime[]; }>(({
    controls, skipTimes,
}) => {
    const { currentTime, setCurrentTime } = useContext(VideoData);
    const currentSkipTime = skipTimes?.find(result => currentTime > result.interval.startTime && currentTime < result.interval.endTime);

    const skipTo: React.ComponentProps<typeof SkipButton>["skipTo"] = async time => {
        await controls.progressTo(time);
        setCurrentTime!(time);
    };

    if (!currentSkipTime)
        return null;
    return (
        <div className={styles.skipButtonContainer}>
            <SkipButton key={currentSkipTime.skipId} skipTime={currentSkipTime} skipTo={skipTo} />
        </div>
    );
}, async ({ episode }) => ({ skipTimes: await episode.skipTimes() }));

// const SkipButtonContainer: React.FC<{ controls: Controls; episode: DownloadedItem; }> = ({ controls, episode }) => {

//     return (
//         <div>
//             <SkipButton skipTo={controls.progressTo} skipTime={} />
//         </div>
//     );
// };

const QueueContainer = React.forwardRef<HTMLDivElement>((props, ref) => {
    return (
        <div className={styles.queueContainer} ref={ref} id="guydhtVideoQueueContainer"></div>
    );
});

function queueButtonOnClick(e: Event, queueContainerRef: React.RefObject<HTMLDivElement>) {
    if (!queueContainerRef.current) return;
    e.preventDefault();
    e.stopPropagation();
    if (queueContainerRef.current!.style.width)
        queueContainerRef.current!.style.width = '';
    else
        queueContainerRef.current!.style.width = "30%";
    queueContainerRef.current!.focus();
};

const QueueButton: React.FC<{ queueContainerRef: React.RefObject<HTMLDivElement>; }> = ({ queueContainerRef }) => {
    return (
        <div className={styles.queueButton} onClick={e => queueButtonOnClick(e.nativeEvent, queueContainerRef)} onMouseUp={e => e.stopPropagation()}>
            <svg viewBox="0 0 128 128">
                <path d="M64,115.5c-13.756,0-26.689-5.357-36.416-15.084C17.856,90.689,12.5,77.756,12.5,64 s5.356-26.689,15.084-36.416C37.311,17.857,50.244,12.5,64,12.5s26.689,5.357,36.416,15.084C110.144,37.311,115.5,50.244,115.5,64 s-5.356,26.689-15.084,36.416C90.689,110.143,77.756,115.5,64,115.5z M64,15.5c-12.955,0-25.135,5.045-34.295,14.205 C20.545,38.866,15.5,51.045,15.5,64s5.045,25.134,14.205,34.295c9.16,9.16,21.34,14.205,34.295,14.205s25.135-5.045,34.295-14.205 C107.455,89.134,112.5,76.955,112.5,64s-5.045-25.134-14.205-34.295C89.135,20.545,76.955,15.5,64,15.5z" />
                <rect height="12" width="50" x="39" y="39" />
                <rect height="4" width="50" x="39" y="60.143" />
                <rect height="4" width="50" x="39" y="72.571" />
                <g><rect height="4" width="50" x="39" y="85" /></g>
            </svg>
        </div>
    );
};

const VideoName: React.FC = ({ children }) => {
    return (
        <div className={styles.videoName}>{children}</div>
    );
};

function commentsButtonOnClick(e: Event, queueContainerRef: React.RefObject<HTMLDivElement>) {
    if (!queueContainerRef.current) return;
    e.preventDefault();
    e.stopPropagation();
    if (queueContainerRef.current!.style.width)
        queueContainerRef.current!.style.width = '';
    else
        queueContainerRef.current!.style.width = "40%";
    queueContainerRef.current!.focus();
};

const ExitCastButton: React.FC<{ exitCast: () => void; }> = ({ exitCast }) => {
    const onClick: React.MouseEventHandler = e => {
        exitCast();
    };
    return (
        <div className={styles.exitCastButton} onClick={onClick} onMouseUp={e => e.stopPropagation()}>
            <svg viewBox="0 0 2000 2000">
                <svg x="500" y="500">
                    <path d="M10,410.3c134.8,0,250.2,48,346.1,143.9C452,650.1,500,765.5,500,900.3h-89.7c0-111.2-39.3-205.7-117.8-283.6C214,538.9,119.8,500,10,500V410.3z M10,589.7c86.2,0,159.9,30.2,221,90.7c61.2,60.5,91.7,133.8,91.7,220h-89.7c0-61.2-21.9-113.6-65.7-157.4c-43.8-43.8-96.3-65.7-157.4-65.7V589.7z M10,766.9c36.1,0,67.4,13.2,93.8,39.6c26.4,26.4,39.6,57.7,39.6,93.8H10V766.9z M900.3,99.7c23.6,0,44.5,9,62.6,27.1c18.1,18.1,27.1,38.9,27.1,62.6v621.4c0,23.6-9,44.5-27.1,62.6s-38.9,27.1-62.6,27.1H589.7v-89.7h310.7V189.3H99.7v133.4H10V189.3c0-23.6,9-44.5,27.1-62.6C55.2,108.7,76,99.7,99.7,99.7H900.3z" />
                </svg>
                <path d="M1000 62.5a937.5 937.5 0 1 0 937.5 937.5A937.5 937.5 0 0 0 1000 62.5zm0 218.75a715.3125 715.3125 0 0 1 424.9375 139.1875L420.40625 1424.90625A718.625 718.625 0 0 1 1000 281.25zm0 1437.5a715.3125 715.3125 0 0 1-424.9375-139.1875l1004.5312500000001-1004.46875A718.625 718.625 0 0 1 1000 1718.75z" />
            </svg>
        </div>
    );
};

const CommentsButton: React.FC<{ commentsContainerRef: React.RefObject<HTMLDivElement>; }> = ({ commentsContainerRef }) => {
    return (
        <div className={styles.commentsButton} onClick={e => commentsButtonOnClick(e.nativeEvent, commentsContainerRef)} onMouseUp={e => e.stopPropagation()}>
            <svg viewBox="0 0 64 64">
                <path d="M1.364,12.252v32.467c0,3.405,2.76,6.166,6.166,6.166h2.266l5.224,5.224   c1.157,1.157,2.725,1.806,4.359,1.806c0.025,0,0.05,0,0.076-0.001c1.661-0.02,3.244-0.709,4.389-1.912l4.874-5.116H56.47   c3.405,0,6.166-2.761,6.166-6.166V12.252c0-3.405-2.761-6.166-6.166-6.166H7.529C4.124,6.086,1.363,8.847,1.364,12.252z    M7.529,12.252H56.47v32.467H26.075l-6.696,7.03l-7.031-7.03H7.529V12.252z" />
                <path d="M16.424,31.266h31.152c1.535,0,2.78-1.244,2.78-2.78c0-1.535-1.245-2.78-2.78-2.78H16.424   c-1.536,0-2.781,1.245-2.781,2.78C13.644,30.021,14.889,31.266,16.424,31.266z" />
                <path d="M16.424,40.595h15.167c1.535,0,2.78-1.244,2.78-2.78c0-1.535-1.245-2.78-2.78-2.78H16.424   c-1.536,0-2.781,1.245-2.781,2.78C13.644,39.351,14.889,40.595,16.424,40.595z" />
                <path d="M16.424,21.937h31.152c1.535,0,2.78-1.244,2.78-2.78c0-1.535-1.245-2.78-2.78-2.78H16.424   c-1.536,0-2.781,1.245-2.781,2.78C13.644,20.692,14.889,21.937,16.424,21.937z" />
            </svg>
        </div>
    );
};

const BottomSlider = React.forwardRef<HTMLDivElement, { children: React.ReactNode; } & React.HTMLAttributes<HTMLDivElement>>((props, ref) => {
    return (
        <div {...props} ref={ref} className={styles.bottomSlider}>{props.children}</div>
    );
});

const TopSlider: React.FC<React.HTMLAttributes<HTMLDivElement>> = ({ children, ...props }) => {
    return (
        <div className={styles.topSlider} {...props}>
            {children}
        </div>
    );
};

function onCommentsContainerKeyDown(e: KeyboardEvent, commentsContainer: HTMLDivElement) {
    if (e.code === "KeyC") return;
    switch (e.code) {
        case "KeyS":
        case "ArrowDown":
            scrollSmoothly(commentsContainer, 70);
            break;
        case "KeyW":
        case "ArrowUp":
            scrollSmoothly(commentsContainer, -70);
            break;
        case "KeyO":
            [...commentsContainer.querySelectorAll('input[type="button"].button.show_button')].forEach(ele => (ele as HTMLElement).click());
            break;
        default:
            return false;
    }
    e.preventDefault();
    return true;
}

const handleKeyDown = (controls: Controls, setProgress: (progress: number) => void,
    commentsContainerRef: React.RefObject<HTMLDivElement>,
    queueContainerRef: React.RefObject<HTMLDivElement>,
    wrapperRef: React.RefObject<HTMLDivElement>) => {
    const handler = async (e: KeyboardEvent) => {
        if ((e.target as HTMLElement).tagName === "INPUT")
            return;
        const pressedCode = e.code;
        async function progressBy(amount: number) {
            const currentTime = await controls.currentTime;
            let multiplier = 0;
            if (e.ctrlKey) multiplier += 10;
            if (e.altKey) multiplier += 2.5;
            if (e.shiftKey) multiplier += 0.5;
            if (multiplier === 0) multiplier = 1;
            await controls.progressTo(currentTime + amount * multiplier);
            setProgress(currentTime + amount * multiplier);
        }
        if (commentsContainerRef.current?.style.width && onCommentsContainerKeyDown(e, commentsContainerRef.current!)) return;
        // Javascript things a space string is 0, which is a number..... goddamittttttttt
        if (e.key !== " " && !isNaN(Number(e.key)) && !e.ctrlKey && !e.shiftKey && !e.altKey) {
            const duration = await controls.duration;
            const timeToBe = parseInt(e.key) * duration / 10;
            await controls.progressTo(timeToBe);
            setProgress(timeToBe);
            return;
        }
        switch (pressedCode) {
            case "Space":
            case "KeyK":
                controls.togglePlayPause();
                break;
            case "ArrowRight":
                progressBy(4);
                break;
            case "ArrowLeft":
                progressBy(-4);
                break;
            case "KeyD":
                progressBy(5);
                break;
            case "KeyA":
                progressBy(-5);
                break;
            case "KeyL":
                progressBy(10);
                break;
            case "KeyJ":
                progressBy(-10);
                break;
            case "KeyS": {
                const volume = await controls.volume;
                if (volume === 0) return;
                controls.setVolume(volume - 0.05);
                break;
            }
            case "KeyW": {
                const volume = await controls.volume;
                if (volume === 1) return;
                controls.setVolume(volume + 0.05);
                break;
            }
            case "ArrowDown": {
                const volume = await controls.volume;
                if (volume === 0) return;
                controls.setVolume(volume - 0.1);
                break;
            }
            case "ArrowUp": {
                const volume = await controls.volume;
                if (volume === 1) return;
                controls.setVolume(volume + 0.1);
                break;
            }
            case "KeyP":
                queueButtonOnClick(e, queueContainerRef);
                break;
            case "KeyC":
                commentsButtonOnClick(e, commentsContainerRef);
                break;
            case "KeyF":
                toggleFullScreen(wrapperRef.current!);
                break;
            default:
                return;
        }
        e.preventDefault();
    };
    return handler;
};

export const CastedVideoPlayer = React.forwardRef<any, { controls: Controls; episode?: DownloadedItem; exitCast: () => void; }>(({ controls, episode, children, exitCast, ...props }, ref) => {
    const [paused, setIsPaused] = useAsyncedValue(() => controls.paused, true);
    const [currentTime, setCurrentTime] = useAsyncedTime(() => controls.currentTime, paused);
    const [duration, setDuration] = useAsyncedTime(() => controls.duration, true);

    const [tooltipPosition, setTooltipPosition] = useState({ pixels: 0, progress: 0 });
    const [tooltipText, setTooltipText] = useState("");
    const wrapperRef = React.createRef<HTMLDivElement>();

    const queueContainerRef = useRef<HTMLDivElement>(null),
        commentsContainerRef = useRef<HTMLDivElement>(null),
        bottomSliderRef = useRef<HTMLDivElement>(null);


    // When the episode changes, counts as paused!
    useEffect(() => {
        setIsPaused(true);
        // eslint-disable-next-line
    }, [episode]);

    const prevEpisode = useRef(episode);
    useEffect(() => {
        if (episode && duration && episode === prevEpisode.current)
            new CacheLocalStorage("videoLastTime").setItem(episode.episodeData.name, { currentTime: currentTime, progress: currentTime / duration });
        prevEpisode.current = episode;
    }, [currentTime, duration, episode]);

    const calculateAndSetTooltipPosition = useCallback((position: number, progressRect?: DOMRect) => {
        if (position === 0)
            return setTooltipPosition({ pixels: 0, progress: 0 });
        const wrapperRect = wrapperRef.current!.getBoundingClientRect(),
            progress = (position - progressRect!.left) / progressRect!.width * duration;
        setTooltipPosition({
            pixels: position - (wrapperRect?.left ?? 0),
            progress
        });
        setTooltipText(secondsToTimeDisplay(progress));
    }, [duration, wrapperRef]);

    useEffect(() => {
        const keyboardHandler = handleKeyDown(controls, setCurrentTime, commentsContainerRef, queueContainerRef, wrapperRef);
        document.body.addEventListener("keydown", keyboardHandler);
        return () => document.body.removeEventListener("keydown", keyboardHandler);
    }, [controls, setCurrentTime, wrapperRef]);

    let mouseupTimeout = useRef<ReturnType<typeof setTimeout> | undefined>(undefined),
        mouseupPauseFlag = useRef(false),
        mouseupLongPauseFlag = useRef(false);

    const onMouseUp: React.MouseEventHandler = useCallback(e => {
        e.persist();
        if (queueContainerRef.current?.style.width || commentsContainerRef.current?.style.width) {
            queueContainerRef.current!.style.width = '';
            return commentsContainerRef.current!.style.width = '';
        }
        if (e.button !== 0) return;
        setTimeout(function () {
            mouseupLongPauseFlag.current = false;
        }, 400);
        if (mouseupLongPauseFlag.current) toggleFullScreen(wrapperRef.current!);
        if (mouseupPauseFlag.current) {
            clearTimeout(mouseupTimeout.current);
            mouseupPauseFlag.current = false;
            return;
        }
        mouseupPauseFlag.current = mouseupLongPauseFlag.current = true;
        mouseupTimeout.current = setTimeout(function () {
            if (e.clientY < (bottomSliderRef.current?.getBoundingClientRect().top ?? Infinity))
                controls.togglePlayPause();
            mouseupPauseFlag.current = false;
        }, 175);
    }, [controls, wrapperRef]);
    const [slidersOpacity, setSlidersOpacity] = useState(1);
    let mouseMoveTimeout = useRef<ReturnType<typeof setTimeout>>(1 as any);
    const onMouseMove: React.MouseEventHandler = useCallback((e) => {
        clearTimeout(mouseMoveTimeout.current);
        if (e.nativeEvent.target === wrapperRef.current)
            mouseMoveTimeout.current = setTimeout(() => {
                setSlidersOpacity(0);
            }, 1000);
        setSlidersOpacity(1);
    }, [wrapperRef]),
        onMouseOut = setSlidersOpacity.bind(this, 0),
        onMouseEnter = setSlidersOpacity.bind(this, 1);
    useEffect(() => {
        document.addEventListener("fullscreenchange", () => {
            setSlidersOpacity(1);
            mouseMoveTimeout.current = setTimeout(() => {
                setSlidersOpacity(0);
            }, 1000);
        });
    }, []);

    // Manually see if the video has ended, then set properties accordingly
    useEffect(() => {
        if (currentTime >= duration) {
            setIsPaused(true);
            setCurrentTime(duration);
        }
        // eslint-disable-next-line
    }, [currentTime, duration]);

    const asProp = (props as any).as || "div";
    return React.createElement(asProp, { ...props, ref: ref },
        <div className={styles.wrapper} style={{ cursor: slidersOpacity === 0 ? "none" : undefined }} ref={wrapperRef} onMouseUp={onMouseUp} onMouseMove={onMouseMove} onMouseOut={onMouseOut} onMouseEnter={onMouseEnter}>
            <VideoData.Provider value={useMemo(() => ({
                currentTime,
                duration,
                paused,
                setCurrentTime,
                setDuration,
                setIsPaused,
            }), [currentTime, duration, paused, setCurrentTime, setDuration, setIsPaused])}>
                <QueueContainer ref={queueContainerRef} />
                <TopSlider style={{ opacity: slidersOpacity }}>
                    <QueueButton queueContainerRef={queueContainerRef} />
                    <VideoName>{episode?.episodeData.name}</VideoName>
                    <CommentsButton commentsContainerRef={commentsContainerRef} />
                    <ExitCastButton exitCast={exitCast} />
                </TopSlider>
                <CommentsContainer ref={commentsContainerRef} episode={episode} />
                {episode && <SkipButtonContainer controls={controls} episode={episode} />}
                <BottomSlider style={{ opacity: slidersOpacity }} ref={bottomSliderRef}>
                    <PreviousButton />
                    <PlayPauseButton controls={controls} />
                    <NextButton />
                    {/* <VolumeSlider controls={controls} /> */}
                    <TimeDisplayer />
                    <FullscreenButton wrapperRef={wrapperRef} />
                    <SettingsMenu controls={controls} />
                    <div className={styles.timeTooltip} style={{ transform: `translate(calc(${tooltipPosition.pixels}px - 50%), -50%)`, opacity: tooltipPosition.pixels === 0 ? 0 : 1 }}> {tooltipText} </div>
                    <TooltipThumbnail src={episode?.videoSrc} tooltipPosition={tooltipPosition} />
                    <ProgressSlider controls={controls} setTooltipPosition={calculateAndSetTooltipPosition} />
                </BottomSlider>
                {children}
            </VideoData.Provider>
        </div>
    );
});