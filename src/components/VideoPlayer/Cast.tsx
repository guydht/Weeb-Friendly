import { useEffect, useState } from "react";
import { Button } from "react-bootstrap";
import { reloadPage } from "../../App";
import DownloadedItem from "../../classes/DownloadedItem";
import { hashCode } from "../../utils/OutsourcedGeneral";
import { getLocalIp, promisify } from "../../utils/general";

let ChromecastAPI = window.require('chromecast-api');
let chromeCastClient = new ChromecastAPI();
(window as any).asd = chromeCastClient;
const { ipcRenderer } = window.require("electron");

export function reloadChromecastAPI() {
	// chromeCastClient = new ChromecastAPI();
	chromeCastClient.update();
}

export { chromeCastClient };

export type Device = {
	changeSubtitles: (subId: any, callback?: () => void) => void;
	changeSubtitlesSize: (fontScale: number, callback?: () => void) => void;
	close: (callback?: () => void) => void;
	getCurrentTime: (callback?: () => void) => void;
	getReceiverStatus: (callback?: () => void) => void;
	getStatus: (callback?: (err: Error, status: any) => void) => void;
	getVolume: (callback?: () => void) => void;
	pause: (callback?: () => void) => void;
	play: (...args: any) => void;
	resume: (callback?: () => void) => void;
	seek: (seconds: number, callback?: () => void) => void;
	seekTo: (newCurrentTime: number, callback?: () => void) => void;
	setVolume: (volume: number, callback?: () => void) => void;
	setVolumeMuted: (muted: boolean, callback?: () => void) => void;
	stop: (callback?: () => void) => void;
	subtitlesOff: (callback?: (...args: any) => void) => void;
	unpause: (callback?: () => void) => void;
	playEpisode: (episode: DownloadedItem, getSubtitles: () => Promise<any[]>) => void;
	name: string;
	host: string;
	friendlyName: string;
	ip: string;
};

// Filters devices based on whether or not they are hosted on an ipv4 address
function filterDevices(devices: Device[]): Device[] {
	return [...devices];
}

// For some reason when this component loads, some random iframe takes up the whole screen (?????) so I remove it here
setInterval(() => {
	for (const bodyChild of document.body.children)
		if (bodyChild.tagName === "IFRAME" &&
			(bodyChild as HTMLIFrameElement).src === "" &&
			(bodyChild as HTMLIFrameElement).style.zIndex === "2147483647")
			bodyChild.remove();
}, 100);

const Cast = ({ episode, onStartCast, getSubtitleTracks }: { episode: DownloadedItem; onStartCast: (castingDevice: Device) => void; getSubtitleTracks: () => Promise<any[]>; }) => {
	const [devices, setDevices] = useState(filterDevices(chromeCastClient.devices as Device[]));

	const onDeviceClick = async (device: Device, episode: DownloadedItem, getSubtitleTracks: () => Promise<any[]>, forcePlay = false) => {
		const videoId = hashCode(episode.absolutePath);
		(device as any).episode = episode;
		(device as any).videoId = videoId;
		const subtitleTracks = Object.values(await getSubtitleTracks());
		await ipcRenderer.invoke("startStreamingVideoFile", { filePath: episode.absolutePath, fileId: videoId });
		await ipcRenderer.invoke("startStreamingSubFiles", { filePath: episode.absolutePath, fileId: videoId });
		console.log('streaming server set up', { subtitleTracks });
		try {
			let [err, status] = await promisify(device.getStatus.bind(device));
			console.log({ status });
			let host = device.ip;
			const localIp = getLocalIp(host);
			console.log({ localIp });
			if (forcePlay || err?.message === "no session started" || !status || !status?.media?.contentId?.includes(videoId))
				await promisify(device.play.bind(device, {
					url: `http://${localIp}:24601/vidChunk?id=${videoId}`,
					subtitles: subtitleTracks.map((subsData, i) => ({
						language: subsData.language,
						url: `http://${localIp}:24601/${videoId}/${i}/vidSubs.vtt`,
						name: subsData.name,
					})),
					subtitles_style: {
						backgroundColor: '#FFFFFF00', // see http://dev.w3.org/csswg/css-color/#hex-notation
						foregroundColor: '#FFFFFFFF', // see http://dev.w3.org/csswg/css-color/#hex-notation
						edgeType: 'OUTLINE', // can be: "NONE", "OUTLINE", "DROP_SHADOW", "RAISED", "DEPRESSED"
						edgeColor: '#000000FF', // see http://dev.w3.org/csswg/css-color/#hex-notation
					},
					autoPlay: false,
				}));
			console.log('a');
			(device as any).subs = subtitleTracks;
			await promisify(device.subtitlesOff.bind(device)).catch(() => { });
			console.log('b');
			await promisify(device.changeSubtitles.bind(device, 0)).catch(() => { });
			console.log('c');
			onStartCast(device);
		}
		catch (e) {
			console.error(e);
			reloadChromecastAPI();
			reloadPage();
		}
	};

	useEffect(() => {
		chromeCastClient.on('device', (_: Device) => {
			setDevices(filterDevices(chromeCastClient.devices));
		});
		// React doesn't know having "chromeCastClient" here is important since I destroy it sometimes
		// eslint-disable-next-line 
	}, [setDevices, chromeCastClient]);

	const doReload = () => {
		reloadChromecastAPI();
		setDevices([]);
	};

	devices.forEach(device => {
		device.playEpisode = onDeviceClick.bind(device, device);
	});

	return (
		<div>
			{
				devices.length ? devices.map(device => {
					return (
						<div key={device.name} onClick={() => onDeviceClick(device, episode, getSubtitleTracks)}>
							{/* It returns special characers as an html code for some reason, so here I decode it */}
							{device.friendlyName.replace(/&#[0-9]+;/g, code => String.fromCharCode(Number(code.slice(2, -1))))}
						</div>
					);
				}) : <>
					No castable devices found :(
					<Button onClick={doReload}>⟳</Button>
				</>
			}
		</div>
	);
};

export default Cast;
export { Cast };

