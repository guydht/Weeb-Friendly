import SubtitlesOctopus from "libass-wasm";
import React, { Component } from "react";
import ReactDom from "react-dom";
import { ReactComponent as NextEpisodeIcon } from "../../assets/NextIcon.svg";
import Consts from "../../classes/Consts";
import styles from "../../css/components/VideoPlayer.module.css";
import { asd, waitForPromised } from "../../jsHelpers/jifa";
import { DisplayDownloadedAnime } from "../../pages/home/DownloadedAnime";
import { CacheLocalStorage, compareAnimeName, hashCode } from "../../utils/OutsourcedGeneral";
import { convertSrtToASS, groupBySeries, promisify, setTimeoutPromisified } from "../../utils/general";
import VideoThumbnail, { renderVideo } from "../VideoThumbnail";
import Cast, { chromeCastClient, reloadChromecastAPI } from "./Cast";
import { CastedVideoPlayer } from "./CastedPlayer";
import Comments from "./Comments";
import { CountdownToNextEpisode } from "./Countdown";
import { Playlist } from "./Playlist";
import { SkipButton } from "./SkipButton";
import TorrentManager from "../../classes/TorrentManager";

const { ipcRenderer } = window.require("electron");

class AdjacentEpisodeButton extends Component {

	static SIZE_PERCENTAGE_OF_CONTAINER = .5;

	isPrev = null;

	state = {
		showThumbnail: false,
		castingDevice: undefined
	};

	iconRef = React.createRef();

	render() {
		const hideThumbnail = () => this.setState({ showThumbnail: false }),
			showThumbnail = () => this.setState({ showThumbnail: true }),
			props = { ...this.props };
		delete props.downloadedItem;
		delete props.videoContainer;
		delete props.title;
		delete props.thumbnailMarginLeft;
		delete props.children;
		return (
			<div className={styles.nextEpisodeIcon} {...props}>
				<NextEpisodeIcon style={{ transform: this.isPrev ? "rotate(180deg)" : "" }}
					onMouseEnter={showThumbnail} onMouseLeave={hideThumbnail} ref={this.iconRef} />
				<div className={styles.thumbnailCanvasContainer + (this.state.showThumbnail ? "" : ` ${styles.hidden}`)} style={{
					height: this.props.videoContainer.clientHeight * (NextEpisodeButton.SIZE_PERCENTAGE_OF_CONTAINER / 2),
					width: this.props.videoContainer.clientWidth * NextEpisodeButton.SIZE_PERCENTAGE_OF_CONTAINER,
					marginLeft: this.props.thumbnailMarginLeft
				}}
					onMouseEnter={showThumbnail} onMouseLeave={hideThumbnail}>
					<VideoThumbnail className={styles.thumbnailCanvas} key={this.props.downloadedItem.absolutePath} videoUrl={Consts.FILE_URL_PROTOCOL + this.props.downloadedItem.absolutePath} />
					<div className={styles.thumbnailCanvasTitle}>
						<strong>
							{this.isPrev ? "Prev (Shift + B)" : "Next (Shift + N)"}
						</strong>
						<span>
							{this.props.title}
						</span>
					</div>
				</div>
			</div>
		);
	}

}

class NextEpisodeButton extends AdjacentEpisodeButton {
	isPrev = false;
}

class PrevEpisodeButton extends AdjacentEpisodeButton {
	isPrev = true;
}

export default class VideoPlayer extends Component {

	videoWrapper = React.createRef();
	subtitlesOctopus;
	videoHandler;
	keydownHandler;
	subtitlesMap = {};

	state = {
		displayFinishScreenEntries: false,
		seriesQueue: []
	};

	indexInSeriesQueue = {};

	getIndexInSeriesQueue() {
		let seriesIndex, episodeIndex;
		for (const sIndex in this.state.seriesQueue)
			for (const eIndex in this.state.seriesQueue[sIndex])
				if (this.state.seriesQueue[sIndex][eIndex].episodeData.name === this.props.downloadedItem.episodeData.name) {
					seriesIndex = sIndex;
					episodeIndex = eIndex;
				}
		this.indexInSeriesQueue = {
			series: {
				total: this.state.seriesQueue.length,
				index: Number(seriesIndex)
			},
			episode: {
				total: this.state.seriesQueue[seriesIndex]?.length,
				index: Number(episodeIndex)
			}
		};
		return this.indexInSeriesQueue;
	}

	componentWillUnmount() {
		ipcRenderer.removeListener("sub-file-processed", this.handleProcessedSubs);
		ipcRenderer.send("stop-processing-of-sub-file");
		document.body.removeEventListener("keydown", this.keydownListener);
		let video = this.videoWrapper.current?.querySelector("video");
		if (video)
			new CacheLocalStorage("videoLastTime").setItem(this.props.downloadedItem.episodeData.name, { currentTime: video.currentTime, progress: video.currentTime / video.duration });
		if (this.subtitlesOctopus)
			try {
				this.subtitlesOctopus.dispose();
			} catch (e) { }
			finally {
				clearInterval(this.subtitlesOctopus.resizeInterval);
				delete this.subtitlesOctopus;
			}
		if (this.videoHandler) {
			this.videoHandler.destroy();
		}
		if (document.fullscreenElement)
			document.exitFullscreen();
		if (this.state.castingDevice) {
			this.stopCasting();
		}
	}

	keydownListener = e => this.keydownHandler?.(e);

	componentDidMount() {
		document.body.addEventListener("keydown", this.keydownListener);
		ipcRenderer.addListener("sub-file-processed", this.handleProcessedSubs);
		this.createWatchQueue();
		this.getIndexInSeriesQueue();
		this.setupVideo();
	}

	handleProcessedSubs = (_, files) => {
		console.log('files!', files);
		this.handleSubs(files);
	};

	createWatchQueue() {
		const series = groupBySeries(Consts.FILTERED_DOWNLOADED_ITEMS).filter(ele => ele[0]?.episodeData.anime_title);
		const orderedSeries = series.sort((a, b) => {
			return Math.max(...b.map(ele => ele.lastUpdated.getTime())) -
				Math.max(...a.map(ele => ele.lastUpdated.getTime()));
		}).map(series => series.filter(episode => !episode.seenThisEpisode() || episode.episodeData.name === this.props.downloadedItem.episodeData.name).sort((a, b) => a.episodeData.episodeOrMovieNumber - b.episodeData.episodeOrMovieNumber)).filter(ele => ele.length),
			indexOfCurrentSeries = orderedSeries.findIndex(series => series[0]?.episodeData.anime_title === this.props.downloadedItem.episodeData.anime_title);
		if (indexOfCurrentSeries >= 0)
			orderedSeries.splice(0, 0, orderedSeries.splice(indexOfCurrentSeries, 1)[0]);
		//eslint-disable-next-line
		this.state.seriesQueue = orderedSeries; // Since I need it to be updated instantly
		this.setState({
			seriesQueue: orderedSeries
		});
	}


	handleSubs = async (subFiles, timeOffset = undefined) => {
		const fonts = [],
			defaultSub = subFiles[0],
			container = this.videoWrapper.current;
		const subtitlesMap = {};
		await Promise.all(subFiles.map(async (f, index) => {
			let title = f.title;
			if (title in subtitlesMap)
				title = f.name;
			if (title in subtitlesMap)
				title = `#${index}`;
			if ((f.name.endsWith(".ass") || f.name.endsWith(".ssa"))) {
				subtitlesMap[title] = f;
			}
			else if (f.name.endsWith(".ttf"))
				fonts.push(URL.createObjectURL(new Blob([f.data])));
			else if (f.name.endsWith(".srt")) {
				f.data = await convertSrtToASS(f.data);
				subtitlesMap[title] = f;
			}
		}));
		Object.assign(this.subtitlesMap, subtitlesMap);
		const subtitleNames = Object.keys(this.subtitlesMap),
			video = container.querySelector("video"),
			options = {
				canvas: container.querySelector("canvas#guydhtVideoSubsCanvas"),
				subContent: defaultSub?.data,
				fonts,
				targetFps: 24,
				video,
			};
		if (!video) return;
		if (defaultSub && !this.subtitlesOctopus) {
			this.subtitlesOctopus = new SubtitlesOctopus(options);
			this.subtitlesOctopus.resize(window.screen.width, window.screen.height);
		}
		this.subtitlesOctopus.timeOffset = timeOffset || 0;
		if (defaultSub) {
			this.subtitlesOctopus.setTrack(defaultSub.data);
			container.setSubtitleTracksNames(subtitleNames);
		}
		else if (timeOffset === undefined) {
			this.subtitlesOctopus.freeTrack();
		}
		this.subtitlesOctopus.timeOffset = -timeOffset || 0;
	};

	async setupVideo() {
		this.subtitlesMap = {};
		let container = this.videoWrapper.current;
		container.addEventListener("guydhtChangeSubs", event => {
			this.subtitlesOctopus.setTrack(this.subtitlesMap[event.detail].data);
		});
		this.keydownHandler = e => {
			if(e.target.tagName === "INPUT") return
			const [prevEpisode, nextEpisode] = this.getAdjacentDownloadedItems();
			if (e.shiftKey && !e.ctrlKey && !e.altKey && e.code === "KeyN" && nextEpisode)
				nextEpisode.startPlaying();
			else if (e.shiftKey && !e.ctrlKey && !e.altKey && e.code === "KeyP" && prevEpisode)
				prevEpisode.startPlaying();
			if (e.code === "Enter" && document.getElementById("skip-button"))
				document.getElementById("skip-button").click();
		};
		ipcRenderer.send("process-subs-of-file", this.props.src);
		if (this.state.castingDevice) {
			const fromCacheStorage = new CacheLocalStorage("videoLastTime").getItem(this.props.downloadedItem.episodeData.name, {}),
				initialCurrentTime = fromCacheStorage.currentTime || 0,
				initialDuration = initialCurrentTime / (fromCacheStorage.progress || 1),
				subs = await ipcRenderer.invoke("get-subs-of-file", this.props.downloadedItem.absolutePath);
			if (subs?.length)
				await this.handleSubs(subs);
			this.state.castingDevice.playEpisode(this.props.downloadedItem, () => Object.values(this.subtitlesMap));
			this.castedVideoStatusCache.currentTime = initialCurrentTime;
			this.castedVideoStatusCache.media.duration = initialDuration;
			return;
		}
		this.videoHandler = asd(this.props.downloadedItem.episodeData.name, container, this.props.src, (file, timeOffset) => this.handleSubs([file].filter(ele => ele), timeOffset));
		let video = container.querySelector("video");
		video.addEventListener("ended", () => {
			if (video.currentTime === video.duration)
				this.setState({ displayFinishScreenEntries: true });
		});
		if (video.src.startsWith("http")) {
			const playFromFile = () => {
				const found = Consts.DOWNLOADED_ITEMS.find(item => item.equals(this.props.downloadedItem));
				console.log(found);
				if (found)
					found.startPlaying();
			};
			TorrentManager.addEventListener(TorrentManager.Listener.torrentFinished, torrent => {
				if (torrent.files.some(file => this.props.downloadedItem.fileName === file.name))
					playFromFile();
			});
			video.addEventListener('error', playFromFile);
		}
		let removeFinishScreen = () => {
			if (video.currentTime !== video.duration)
				this.setState({ displayFinishScreenEntries: false });
		};
		video.addEventListener("playing", removeFinishScreen);
		video.addEventListener("seeking", removeFinishScreen);
		ReactDom.render(<Comments episode={this.props.downloadedItem} />, container.querySelector("#guydhtVideoCommentsContainer"));
		ReactDom.render(<Cast onStartCast={device => this.startedCasting(device, video.paused)} getSubtitleTracks={async () => await ipcRenderer.invoke("wait-for-subs-of-file", this.props.downloadedItem.absolutePath)} episode={this.props.downloadedItem} />, container.querySelector("#guydhtVideoCastContainer"));
		this.props.downloadedItem.skipTimes().then(async skipTimes => {
			await waitForPromised(() => video.duration > 0);
			video.addEventListener("timeupdate", () => {
				const currentTime = video.currentTime;
				const currentSkipTime = skipTimes.find(result => currentTime > result.interval.startTime && currentTime < result.interval.endTime);
				if (currentSkipTime)
					ReactDom.render(<SkipButton key={currentSkipTime.skipId} skipTime={currentSkipTime} skipTo={(time) => video.currentTime = time} />, container.querySelector("#guydhtVideoSkipButtonContainer"));
				else
					ReactDom.unmountComponentAtNode(container.querySelector("#guydhtVideoSkipButtonContainer"));
			});
		});
		const subs = await ipcRenderer.invoke("get-subs-of-file", this.props.downloadedItem.absolutePath);
		if (subs?.length)
			this.handleSubs(subs);
	}

	getAdjacentDownloadedItems() {
		const position = this.indexInSeriesQueue,
			prevEpisodeIndex = position.episode.index > 0 ? [position.series.index, position.episode.index - 1] :
				position.series.index > 0 ? [position.series.index - 1, 0] : [],
			nextEpisodeIndex = position.episode.index < position.episode.total - 1 ? [position.series.index, position.episode.index + 1] :
				position.series.index < position.series.total - 1 ? [position.series.index + 1, 0] : [];
		return [
			this.state.seriesQueue?.[prevEpisodeIndex[0]]?.[prevEpisodeIndex[1]],
			this.state.seriesQueue?.[nextEpisodeIndex[0]]?.[nextEpisodeIndex[1]]
		];
	}

	stopCasting = () => {
		try {
			this.state.castingDevice.stop();
		} catch (e) { console.error(e); }
		this.setState({
			castingDevice: undefined
		}, this.handleSrcChange);
	};

	reloadCastingDevice = async () => {
		reloadChromecastAPI();
		const self = this;
		async function findMatchingDevice(timeoutInSeconds) {
			const newDevice = chromeCastClient.devices.find(device => device.name === self.state.castingDevice.name &&
				device.host === self.state.castingDevice.host &&
				device.friendlyName === self.state.castingDevice.friendlyName
			);
			if (newDevice) return newDevice;
			if (timeoutInSeconds > 0) {
				await setTimeoutPromisified(0.5);
				return await findMatchingDevice(0.5);
			}
			return null;
		}
		const newDevice = await findMatchingDevice(5);
		if (newDevice) {
			//eslint-disable-next-line
			this.state.castingDevice = newDevice;
			this.setState({ castingDevice: newDevice });
		}
	};

	startedCasting = async (castingDevice, shouldBePaused) => {
		if (this.videoHandler) {
			this.videoHandler.destroy();
		}
		this.setState({ castingDevice });
		await new Promise(resolve => {
			castingDevice.seekTo(new CacheLocalStorage("videoLastTime").getItem(this.props.downloadedItem.episodeData.name, {}).currentTime || 0, resolve);
		});
		if (shouldBePaused)
			await new Promise(resolve => castingDevice.pause(resolve));
	};

	castedVideoStatusCache = { volume: { level: 1 }, media: { duration: 0 }, currentTime: 0, playerState: "PAUSED" };

	controlsForVideo = () => {
		const statusCache = [this.castedVideoStatusCache, 0],
			episodeNameHashCode = hashCode(this.props.downloadedItem.absolutePath);
		const getCachedStatus = async (forceGetNow = false) => {
			if (forceGetNow || Date.now() - statusCache[1] > 1000) {
				statusCache[1] = Date.now();
				try {
					const [error, status] = await promisify(this.state.castingDevice.getStatus.bind(this.state.castingDevice));
					statusCache[0] = status || this.castedVideoStatusCache;
					if (error || !status || !status.media.contentId.includes(episodeNameHashCode))
						return statusCache[0];
					statusCache[0] = status;
					statusCache[1] = Date.now();
				} catch (e) {
					if (e instanceof TypeError) {
						await this.reloadCastingDevice();
						return getCachedStatus(forceGetNow);
					}
				}
			}
			return statusCache[0];
		};
		let activeSubId = 0;
		const self = this;
		return {
			get currentTime() {
				return getCachedStatus().then(s => s.currentTime);
			},
			async togglePlayPause() {
				const isPaused = await getCachedStatus(true).then(s => s.playerState === "PAUSED");
				const device = self.state.castingDevice;
				const functionToRun = isPaused ? device.unpause.bind(device) : device.pause.bind(device);
				return promisify(functionToRun);
			},
			setVolume: (volume) => {
				return promisify(self.state.castingDevice.setVolume.bind(self.state.castingDevice, volume));
			},
			progressTo: (seconds) => {
				return promisify(self.state.castingDevice.seekTo.bind(self.state.castingDevice, seconds));
			},
			changeSub: (subtitleId) => {
				if (activeSubId === subtitleId) {
					self.state.castingDevice.subtitlesOff();
					activeSubId = -1;
				}
				else {
					self.state.castingDevice.changeSubtitles(subtitleId);
					activeSubId = subtitleId;
				}
			},
			addSubs: async (subsContent, subFileName) => {
				const device = self.state.castingDevice;
				await device.playEpisode(device.episode, () => device.subs.concat([{ title: subFileName.replace(/\.[^/.]+$/, ""), name: subFileName, data: subsContent }]), true);
				activeSubId = device.subs.length - 1;
				await promisify(device.changeSubtitles.bind(device, activeSubId));
			},
			removeSubs: async () => {
				await promisify(self.state.castingDevice.subtitlesOff.bind(self.state.castingDevice));
				activeSubId = -1;
			},
			getActiveSubId: () => {
				return activeSubId;
			},
			getSubs: () => {
				return self.state.castingDevice.subs;
			},
			get volume() {
				return getCachedStatus().then(s => s.volume.level);
			},
			get duration() {
				return getCachedStatus().then(s => s.media.duration);
			},
			get paused() {
				return getCachedStatus().then(s => s.playerState === "PAUSED");
			},
		};
	};

	endscreenCanvasRef = React.createRef();
	videoRef = React.createRef();

	render() {
		let props = { ...this.props };
		for (let prop of ["src", "name", "downloadedItem"])
			delete props[prop];
		props.children = React.Children.toArray(props.children);
		if (this.state.displayFinishScreenEntries) {
			const nextEpisode = this.getAdjacentDownloadedItems()[1];
			if (Consts.AUTO_PLAY && nextEpisode)
				props.children.push((
					<div className={styles.endScreenContainer}>
						<canvas alt="" ref={this.endscreenCanvasRef} />
						<CountdownToNextEpisode nextEpisode={nextEpisode} />
					</div>
				));
			else
				props.children.push((
					<div className={styles.endScreenContainer}>
						<canvas alt="" ref={this.endscreenCanvasRef} />
						<DisplayDownloadedAnime style={{ overflowY: "hidden", opacity: 0.9, "--grid-gap": '2rem' }} noDeleteButton={true} disableDoubleClick={true} downloadedItems={this.state.seriesQueue.flat().filter(ele => !ele.seenThisEpisode())} />
					</div>
				));
		}
		if (this.state.castingDevice)
			return <CastedVideoPlayer {...props} exitCast={this.stopCasting} ref={this.videoWrapper} controls={this.controlsForVideo()} episode={this.props.downloadedItem}>{this.props.children}</CastedVideoPlayer>;
		props.ref = this.videoWrapper;
		let element = this.props.as ? React.createElement(this.props.as, { ...props }) : <div {...props} />;
		return element;
	}

	handleSrcChange(prevProps) {
		let video = this.videoWrapper.current.querySelector("video");
		if (video)
			new CacheLocalStorage("videoLastTime").setItem(prevProps.downloadedItem.episodeData.name, { currentTime: video.currentTime, progress: video.currentTime / video.duration });
		if (this.subtitlesOctopus)
			try {
				this.subtitlesOctopus.dispose();
			} catch (e) {
			}
			finally {
				clearInterval(this.subtitlesOctopus.resizeInterval);
				delete this.subtitlesOctopus;
			}
		if (this.videoHandler) {
			this.videoHandler.destroy();
		}
		delete this.videoHandler;
		this.setState({ displayFinishScreenEntries: false });
		if (this.state.castingDevice)
			try {
				this.state.castingDevice.stop();
			} catch (e) { }
		this.setupVideo();
		this.renderEpisodeQueueStuff(...this.getAdjacentDownloadedItems());
	}

	refreshSeriesQueue(prevDownloadedItem) {
		let currentEpisodeExistsInPlaylist = this.state.seriesQueue.some(
			series => series.some(
				episode => compareAnimeName(episode.episodeData.name, this.props.downloadedItem.episodeData.name)
			)
		);
		if (!currentEpisodeExistsInPlaylist) {
			const prevIndex = this.state.seriesQueue.findIndex(ele => ele.some(ele => compareAnimeName(ele.episodeData.name, prevDownloadedItem.episodeData.name)));
			this.state.seriesQueue.splice(prevIndex + 1, 0, [this.props.downloadedItem]);
		}
	}

	componentDidUpdate(props) {
		this.refreshSeriesQueue(props.downloadedItem);
		if (props.src !== this.props.src)
			this.handleSrcChange(props);

		this.loadEndScreenBlurredImage();
		this.getIndexInSeriesQueue();
		this.renderEpisodeQueueStuff(...this.getAdjacentDownloadedItems());
	}

	async loadEndScreenBlurredImage() {
		if (this.endscreenCanvasRef.current) {
			let canvasWithData = await renderVideo({
				videoUrl: this.props.src,
				renderedHeight: this.videoWrapper.current?.offsetHeight,
				renderedWidth: this.videoHandler.current?.offsetWidth,
				snapshotTime: 1
			});
			if (this.endscreenCanvasRef.current) // Because await can take a while and meanwhile user might already pass on and this ref will be dead
				this.endscreenCanvasRef.current.getContext("bitmaprenderer").transferFromImageBitmap(canvasWithData.transferToImageBitmap());
		}
	}

	renderEpisodeQueueStuff(prevEpisode, nextEpisode) {
		const container = this.videoWrapper.current;
		if (!container) return;
		const nextEpisodeContainer = container.querySelector("#guydhtNextEpisodeButton"),
			prevEpisodeContainer = container.querySelector("#guydhtPrevEpisodeButton"),
			playlistContainer = container.querySelector("#guydhtVideoQueueContainer");
		if (nextEpisode && nextEpisodeContainer)
			ReactDom.render(<NextEpisodeButton thumbnailMarginLeft={prevEpisode ? -60 : -25}
				onClick={() => nextEpisode.startPlaying()}
				videoContainer={container} title={nextEpisode.episodeData.name} downloadedItem={nextEpisode} />,
				nextEpisodeContainer);
		if (prevEpisode && prevEpisodeContainer)
			ReactDom.render(<PrevEpisodeButton thumbnailMarginLeft={10}
				onClick={() => prevEpisode.startPlaying()}
				videoContainer={container} title={prevEpisode.episodeData.name} downloadedItem={prevEpisode} />,
				prevEpisodeContainer);
		if (playlistContainer)
			ReactDom.render(<Playlist setSeriesQueue={seriesQueue => this.setState({ seriesQueue })}
				seriesQueue={this.state.seriesQueue}
				currentPosition={this.indexInSeriesQueue} />,
				playlistContainer);
	}

}
