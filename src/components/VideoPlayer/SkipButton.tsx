import React, { useCallback, useEffect, useRef, useState } from "react";
import { Button, ButtonProps, OverlayTrigger, Tooltip } from "react-bootstrap";
import { SkipTime, SkipType } from "../../classes/DownloadedItem";

const SKIP_TYPES_TO_NAMES: Record<SkipType, string> = {
    op: "Opening",
    ed: "Ending",
    "mixed-op": "Mixed-Opening",
    "mixed-ed": "Mixed-Ending",
    recap: "Recap",
};
const INVISIBLE_TIMEOUT = 5000;


export const SkipButton: React.FC<{ skipTime: SkipTime, skipTo: (time: number) => void; } & ButtonProps> = ({ skipTime, skipTo, ...props }) => {
    const buttonRef = useRef<HTMLButtonElement>(null);
    const [isInvisible, setIsInvisible] = useState(false);
    const [invisibleTimeout, setInvisibleTimeout] = useState<ReturnType<typeof setTimeout>>();
    const startInvisibleTimeout = useCallback(() => {
        clearTimeout(invisibleTimeout);
        const timeout = setTimeout(setIsInvisible, INVISIBLE_TIMEOUT, true);
        setInvisibleTimeout(timeout);
        return timeout;
    }, [setIsInvisible, setInvisibleTimeout, invisibleTimeout]);

    useEffect(() => {
        const onClick: HTMLButtonElement["onclick"] = e => {
            e.stopPropagation();
            skipTo(skipTime.interval.endTime - 2);
        },
            stopEvent = (e: Event) => e.stopPropagation(),
            enterEvent = () => {
                setIsInvisible(false);
                clearTimeout(invisibleTimeout);
            },
            leaveEvent = () => {
                startInvisibleTimeout();
            };
        const button = buttonRef.current;
        button?.addEventListener("click", onClick);
        button?.addEventListener("mouseup", stopEvent);
        button?.addEventListener("mousedown", stopEvent);
        button?.addEventListener("mouseenter", enterEvent);
        button?.addEventListener("mouseleave", leaveEvent);
        return () => {
            button?.removeEventListener("click", onClick);
            button?.removeEventListener("mouseup", stopEvent);
            button?.removeEventListener("mousedown", stopEvent);
            button?.removeEventListener("mouseenter", enterEvent);
            button?.removeEventListener("mouseleave", leaveEvent);
        };
    }, [skipTime.interval.endTime, skipTo, startInvisibleTimeout, invisibleTimeout]);

    useEffect(() => {
        const invisibleTimeout = startInvisibleTimeout();
        return () => clearTimeout(invisibleTimeout);
        //eslint-disable-next-line
    }, []);

    return (
        <OverlayTrigger trigger={["hover", "focus"]} overlay={<Tooltip id="">Press ⏎ Enter</Tooltip>}>
            <Button ref={buttonRef} variant="outline-dark" id="skip-button" style={{ color: "white", opacity: isInvisible ? 0 : 1, transition: "opacity 1s" }} {...props}>
                Skip {SKIP_TYPES_TO_NAMES[skipTime.skipType]}
            </Button>
        </OverlayTrigger>
    );
};
