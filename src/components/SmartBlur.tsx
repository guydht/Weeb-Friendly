import React, { useEffect, useRef } from "react";
type BlurProps<As extends keyof JSX.IntrinsicElements> = {
    as?: As;
    onBlur?: () => void;
};
type Props<T extends keyof JSX.IntrinsicElements> = BlurProps<T> & React.PropsWithChildren<React.ComponentPropsWithoutRef<T>>;

export const SmartBlur = <T extends keyof JSX.IntrinsicElements = "span">({
    children, as: Component, onBlur, ...props
}: Props<T>) => {
    const parentRef = useRef<HTMLDivElement>(null);

    const checkFocus = React.useCallback((event: MouseEvent) => {
        if (parentRef.current && !parentRef.current.contains(event.target as Node))
            onBlur?.();
    }, [onBlur]);

    useEffect(() => {
        document.addEventListener("mousedown", checkFocus);
        return () => {
            document.removeEventListener("mousedown", checkFocus);
        };
    }, [checkFocus]);

    return React.createElement(
        Component || "span",
        { ref: parentRef, ...props },
        children,
    );
};