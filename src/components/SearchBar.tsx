import React, { Component } from "react";
import Col from "react-bootstrap/Col";
import FormControl from "react-bootstrap/FormControl";
import ListGroup from "react-bootstrap/ListGroup";
import Row from "react-bootstrap/Row";
import Spinner from "react-bootstrap/Spinner";
//@ts-ignore
import { LazyLoadImage } from "react-lazy-load-image-component";
import { Link } from "react-router-dom";
import AnimeEntry from "../classes/AnimeEntry";
import styles from "../css/components/SearchBar.module.css";
import MALUtils from "../utils/MAL";
import { SmartBlur } from "./SmartBlur";

interface SearchBarProps {
	placeholder?: string;
	defaultValue?: string;
	gotoAnimePageOnChoose?: boolean;
	onItemClick?: (animeEntry: AnimeEntry) => void;
	onInputClick?: (e: React.MouseEvent & { doSearch: () => void; }) => void;
	onInputChange?: (e: React.MouseEvent & { setResults: (resuslts: AnimeEntry[]) => void; }) => void;
	showImage?: boolean;
	style?: object;
	clickOnStart?: boolean;
	onItemVisible?: (entry: AnimeEntry) => void;
}

export default class SearchBar extends Component<SearchBarProps> {
	static SEARCH_INPUT_TIMEOUT = 250;

	state = {
		displayEntries: true,
		entries: [],
		loadingText: ""
	};

	inputRef = React.createRef<HTMLInputElement>();

	componentWillUnmount() {
		this.setState = () => { };
	}

	componentDidMount() {
		if (this.props.clickOnStart)
			this.inputRef.current?.click();
	}

	render() {
		const onFocus = () => {
			this.setState({
				displayEntries: true
			});
		},
			onBlur = () => {
				this.setState({
					loadingText: "",
					displayEntries: false
				});
			};
		return (
			<SmartBlur as="span" onBlur={onBlur} onFocus={onFocus} onSubmit={() => false} style={(this.props.style || {})}>
				<FormControl type="text" placeholder={this.props.placeholder || "Search"} className="mr-sm-2"
					ref={this.inputRef}
					onClick={(e: any) => {
						e.doSearch = this.searchAnime.bind(this, e);
						(this.props.onInputClick || e.doSearch)(e);
					}}
					onChange={(e: any) => this.searchAnime(e)}
					defaultValue={this.props.defaultValue || ""}
					onBlur={() => this.setState({ loadingText: "" })} />
				<ListGroup className={styles.wrapper}>
					{
						this.state.entries.length && this.state.displayEntries ?
							this.state.entries.map((entry: AnimeEntry) => {
								return (
									<ListGroup.Item onClick={() => this.chooseEntry(entry)}
										title={Array.from(entry.synonyms).join(", ")} key={entry.malId || entry.name}>
										<Row>
											{
												this.props.showImage && (
													<Col>
														<LazyLoadImage afterLoad={() => this.props.onItemVisible?.(entry)} className={styles.image} src={entry.imageURL} />
													</Col>
												)
											}
											<Col>
												{
													(this.props.gotoAnimePageOnChoose !== false) ?
														<Link to={{
															pathname: "/anime/" + entry.malId,
															state: {
																animeEntry: entry
															}
														}} onClick={onBlur}>
															{entry.name}
														</Link>
														: entry.name
												}
											</Col>
										</Row>
									</ListGroup.Item>
								);
							}) : !!this.state.loadingText ?
								<ListGroup.Item>
									<span>{this.state.loadingText}</span>
									<Spinner animation="border" role="status" size="sm" as="span"
										className={styles.spinner} />
								</ListGroup.Item> : null
					}
				</ListGroup>
			</SmartBlur>
		);
	}
	searchInputTimeout: any;
	async searchAnime(e: any) {
		e.persist();
		let searchName = e.target.value;
		clearTimeout(this.searchInputTimeout);
		this.setState({
			loadingText: "Loading...",
			entries: []
		});
		this.searchInputTimeout = setTimeout(
			async () => {
				if (this.inputValidForSearch(searchName)) {
					this.setState({
						loadingText: "Loading..."
					});
					if (this.props.onInputChange) {
						e.setResults = (results: AnimeEntry[]) => this.setState({ entries: results, loadingText: "" });
						return this.props.onInputChange(e);
					}
					let results = await MALUtils.searchAnime(new AnimeEntry({ name: searchName }));
					this.setState({
						entries: results,
						loadingText: ""
					});
				}
				else
					this.setState({
						loadingText: "I need a longer input..."
					});
			}, SearchBar.SEARCH_INPUT_TIMEOUT);
	}
	inputValidForSearch(value: string): boolean {
		return value.length > 1;
	}
	chooseEntry(entry: AnimeEntry) {
		if (this.props.onItemClick)
			this.props.onItemClick(entry);
	}
}
