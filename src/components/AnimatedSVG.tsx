import React, { useEffect } from "react";
import { animated, useSpring } from "react-spring";

export function useAnimate() {

}

export const AnimatedSVG: React.FC<{ from: string; to: string; doFlip: boolean; }> = ({ from, to, doFlip }) => {
    const { x } = useSpring({
        from: {
            x: 0
        },
        delay: 0,
        config: {
            duration: 0.5,
        },
    });

    useEffect(() => {
        x.start(x.get() === 0 ? 100 : 0);
    }, [doFlip, x]);

    return (
        <svg height="100%" viewBox="0 0 36 36" width="100%">
            <animated.path d={
                x.to({
                    range: [0, 100],
                    output: [
                        from,
                        to,
                    ],
                })
            } />
        </svg>
    );
};
