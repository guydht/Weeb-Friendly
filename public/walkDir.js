const fs = require("fs").promises,
	path = require("path"),
	FileType = require("file-type");

async function walkDir(dir) {
	const results = [];
	const list = await fs.readdir(dir, {
		withFileTypes: true
	});
	await Promise.all(list.map(async dirent => {
		let filename = dirent.name,
			file = path.join(dir, filename),
			extension = path.extname(filename);
		if (dirent.isDirectory())
			results.push(...await walkDir(file));
		else {
			const fileType = await FileType.fromFile(file);
			if (fileType && fileType.mime.includes("video")) {
				const withoutExtension = filename.slice(0, -extension.length),
					stat = await fs.stat(file);
				results.push([
					file,
					withoutExtension,
					stat?.birthtime
				]);
			}
		}
	}));
	return results;
};
exports.walkDir = walkDir;
