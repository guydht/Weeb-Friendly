const electron = require('electron'),
	url = require("url"),
	path = require("path"),
	isDev = require("electron-is-dev"),
	process = require("process"),
	fsPromises = require("fs").promises,
	fs = require("fs"),
	{ extractSubsFromVideoFile } = require("./mkvExtract"),
	extractionsOfSubs = {},
	{ walkDir } = require("./walkDir"),
	express = require("express"),
	{ copyFrenchDude: vttFromParsed } = require('./ass2vttStolen'),
	assParser = require("ass-compiler");

const expressApp = express(),
	filesToHost = {},
	fileToMimeType = {};
expressApp.get("/vidChunk", async (req, res) => {
	const range = req.headers.range,
		requestedId = req.query.id;
	if (!range)
		return res.status(400).send("Requires Range header");
	if (!requestedId)
		return res.status(400).send("Required a video-id parameter!");
	const videoPath = filesToHost[requestedId];
	if (!videoPath)
		return res.status(404).send("Couldn't find video with id " + requestedId);
	const videoSize = (await fsPromises.stat(videoPath)).size;
	const CHUNK_SIZE = 1024 * 1024 * 10;
	const start = Number(range.replace(/\D/g, ""));
	const end = Math.min(start + CHUNK_SIZE, videoSize - 1);
	const contentLength = end - start + 1;
	const headers = {
		"Content-Range": `bytes ${start}-${end}/${videoSize}`,
		"Accept-Ranges": "bytes",
		"Content-Length": contentLength,
		"Content-Type": fileToMimeType[requestedId] || "video/H265",
	};
	res.writeHead(206, headers);
	const videoStream = fs.createReadStream(videoPath, { start, end });
	videoStream.pipe(res);
});
const subsToServe = {};
expressApp.get("/:id/:index/vidSubs.vtt", async (req, res) => {
	if (req.params?.id && req.params.id in subsToServe && req.params.index) {
		res.setHeader("access-control-allow-origin", "*");
		const fileName = subsToServe[req.params.id];
		return await extractionsOfSubs[fileName].wait().then(() => {
			res.send(assToVtt(Object.values(extractionsOfSubs[fileName].tracks)[Number(req.params.index)].data));
		});
	}
	res.status(404).send('What?');
});
expressApp.listen('24601');

process.on('uncaughtException', () => { });

electron.ipcMain.handle("walkDir", async (_, path) => {
	return await walkDir(path);
});

electron.ipcMain.on("getPath", (event, path) => {
	event.returnValue = electron.app.getPath(path);
});

async function startSubsExtraction(path, event) {
	if (path in extractionsOfSubs) {
		const task = extractionsOfSubs[path];
		if (task.isPaused())
			task.resume();
		if (event) event.reply("sub-file-processed", task.normalizedData());
		return task;
	}
	const task = await extractSubsFromVideoFile(path, files => {
		if (event) event.reply("sub-file-processed", files);
	});
	extractionsOfSubs[path] = task;
	return task;
}

electron.ipcMain.on("stop-processing-of-sub-file", () => {
	Object.values(extractionsOfSubs).forEach(ele => ele.pause());
});

electron.ipcMain.on("process-subs-of-file", async (event, filePath) => {
	await startSubsExtraction(filePath, event);
});

electron.ipcMain.handle("wait-for-subs-of-file", async (_, filePath) => {
	const task = await startSubsExtraction("file://" + filePath);
	return task.tracks;
});

const subsFileExtensions = new Set([".ass", ".srt"]);

electron.ipcMain.handle("get-subs-of-file", async (_, filePath) => {
	if (filePath.startsWith("http://")) {
		return [];
	}
	const { dir, name: originalFileName } = path.parse(filePath),
		dirItems = await fsPromises.readdir(dir),
		parsedFiltered = dirItems.map(ele => path.parse(ele)).filter(parsed => {
			return parsed.name === originalFileName && subsFileExtensions.has(parsed.ext);
		});
	return await Promise.all(parsedFiltered.map(async parsed => ({
		name: parsed.name,
		title: parsed.name,
		data: await fsPromises.readFile(path.join(dir, parsed.base), "utf-8")
	})));
});

electron.ipcMain.handle("startStreamingVideoFile", async (_, { filePath, fileId }) => {
	filesToHost[fileId] = filePath;
	const readHeaderStream = fs.createReadStream(filePath);
	const getMimeType = (await import("stream-mime-type")).getMimeType;
	getMimeType(readHeaderStream, { filename: filePath }).then(result => {
		fileToMimeType[fileId] = result.mime;
		readHeaderStream.destroy();
		result.stream.destroy();
	});
});

electron.ipcMain.handle("startStreamingSubFiles", async (_, { filePath, fileId }) => {
	subsToServe[fileId] = "file://" + filePath;
	startSubsExtraction(subsToServe[fileId]);
});

function assToVtt(subs) {
	// Is ASS file
	if (subs.startsWith("[Script Info]"))
		return vttFromParsed(assParser.parse(subs));
	// is srt file
	return "WEBVTT FILE\r\n\r\n'" + subs.replace(/\d\d:\d\d:\d\d(,\d+)? --> \d\d:\d\d:\d\d(,\d+)?/g, srtText => srtText.replace(/,/g, "."));
}

// Module to control application life.
const app = electron.app;

app.commandLine.appendSwitch('disable-site-isolation-trials');
app.commandLine.appendSwitch('disable-features', 'OutOfBlinkCors');
app.commandLine.appendSwitch('disable-features', 'SameSiteByDefaultCookies');

app.whenReady().then(() => {
	electron.protocol.registerFileProtocol('file', (request, callback) => {
		const pathname = decodeURI(request.url.replace('file:///', ''));
		callback(pathname);
	});
});

// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

const windowStateKeeper = require('electron-window-state');

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow, webTorrentWindow;

function createWindow() {
	// Create the browser window.

	let mainWindowState = windowStateKeeper({
		defaultWidth: 800,
		defaultHeight: 600,
		fullScreen: false
	});

	mainWindow = new BrowserWindow({
		x: mainWindowState.x,
		y: mainWindowState.y,
		width: mainWindowState.width,
		height: mainWindowState.height,
		backgroundColor: "#3e3e3e",
		webPreferences: {
			webSecurity: false,
			enableRemoteModule: true,
			allowRunningInsecureContent: false,
			contextIsolation: false,
			nodeIntegration: true,
			nodeIntegrationInSubFrames: true,
			nodeIntegrationInWorker: true,
			sandbox: false,
			devTools: true,
			experimentalFeatures: true,
			plugins: true,
		},
		show: true,
		autoHideMenuBar: true,
		icon: path.join(__dirname, "icons", "icon.ico"),
	});
	mainWindow.webContents.session.webRequest.onHeadersReceived((details, callback) => {
		const cookieHeader = Object.keys(details.responseHeaders).find(ele => ele.toLowerCase() === "set-cookie");
		const cookies = details.responseHeaders[cookieHeader];
		if (cookies) {
			const newCookie = Array.from(cookies).map(cookie => cookie.concat('; SameSite=None; Secure'));
			details.responseHeaders[cookieHeader] = [...newCookie];
		}
		callback({ responseHeaders: Object.fromEntries(Object.entries(details.responseHeaders).filter(header => !/x-frame-options/i.test(header[0]))) });
	});

	mainWindowState.manage(mainWindow);

	webTorrentWindow = new BrowserWindow({
		show: false,
		webPreferences: {
			contextIsolation: false,
			nodeIntegration: true,
			nodeIntegrationInSubFrames: true,
			nodeIntegrationInWorker: true,
			sandbox: false,
			webSecurity: false,
		}
	});

	webTorrentWindow.loadURL(url.format({
		pathname: path.join(__dirname, 'webtorrent', 'webtorrent.html'),
		protocol: 'file'
	}));

	require("./webtorrent/webtorrentMain").init(mainWindow, webTorrentWindow);

	if (isDev)
		mainWindow.loadURL(url.format({
			pathname: 'localhost:3000',
			protocol: 'http'
		}));
	else {
		mainWindow.loadURL(url.format({
			pathname: path.join(__dirname, 'index.html'),
			protocol: 'file'
		}));
	}

	// Emitted when the window is closed.
	mainWindow.on('closed', function () {
		// Dereference the window object, usually you would store windows
		// in an array if your app supports multi windows, this is the time
		// when you should delete the corresponding element.
		mainWindow = null;
		if (webTorrentWindow)
			webTorrentWindow.close();
	});

	webTorrentWindow.on('closed', function () {
		webTorrentWindow = null;
		if (mainWindow)
			mainWindow.close();
	});

	mainWindow.webContents.on('new-window', function (e, url) {
		// make sure local urls stay in electron perimeter
		if ('file://' === url.substr(0, 'file://'.length)) {
			return;
		}

		// and open every other protocols on the browser      
		e.preventDefault();
		electron.shell.openExternal(url);
	});
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
	// On OS X it is common for applications and their menu bar
	// to stay active until the user quits explicitly with Cmd + Q
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
