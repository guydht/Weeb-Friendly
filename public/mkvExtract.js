"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
// Adapted from https://github.com/qgustavor/mkv-extract/ licensed under MIT
var fs = require("fs");
var got_1 = require("got");
var Matroska = require("matroska-subtitles"); // For some reason typescript does new Matroska["default"]() instead of just new Matroska - and that fucks me. So regular "require" it is.
function streamFromURL(url) {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            return [2 /*return*/, got_1["default"].stream(url)];
        });
    });
}
var SubsExtractionTask = /** @class */ (function () {
    function SubsExtractionTask(fileStream, tracks, parser, atData) {
        this.fileStream = fileStream;
        this.tracks = tracks;
        this.parser = parser;
        this.publishData = atData;
    }
    SubsExtractionTask.prototype.start = function () {
        var _this = this;
        var decoder = createDecoderFromStream(this.fileStream, function () { return _this.atData(); }, this.tracks, this.parser).decoder;
        this.parser = decoder;
    };
    SubsExtractionTask.prototype.wait = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, new Promise(function (resolve) {
                            if (_this.parser.closed || _this.parser.destroyed)
                                resolve();
                            else
                                _this.parser.once("finish", resolve);
                        })];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    SubsExtractionTask.prototype.pause = function () {
        return this.fileStream.pause();
    };
    SubsExtractionTask.prototype.resume = function () {
        return this.fileStream.resume();
    };
    SubsExtractionTask.prototype.isPaused = function () {
        return this.fileStream.isPaused();
    };
    SubsExtractionTask.prototype.atData = function () {
        var files = this.normalizedData();
        this.publishData(files);
        return files;
    };
    SubsExtractionTask.prototype.normalizedData = function () {
        var _this = this;
        var files = [];
        Object.values(this.tracks).forEach(function (track) {
            var _a, _b, _c, _d, _e;
            var heading = (_a = track.header) !== null && _a !== void 0 ? _a : "";
            var isASS = heading.includes("Format:");
            var formatFn = isASS ? formatTimestamp : formatTimestampSRT;
            var eventMatches = isASS
                ? ((_b = heading.match(/\[Events\]\s+Format:([^\r\n]*)/)) !== null && _b !== void 0 ? _b : [""])
                : [""];
            var headingParts = isASS ? heading.split(eventMatches[0]) : ["", ""];
            var fixedLines = [];
            if (track.subtitles)
                track.subtitles.forEach(function (subtitle, i) {
                    var startTimeStamp = formatFn(subtitle.time), endTimeStamp = formatFn(subtitle.time + subtitle.duration), lineParts = [subtitle.layer, startTimeStamp, endTimeStamp, subtitle.style,
                        subtitle.name, subtitle.marginL, subtitle.marginR, subtitle.marginV, subtitle.effect, subtitle.text], fixedLine = isASS ? "Dialogue: " + lineParts.join(",")
                        : i + 1 + "\r\n" + startTimeStamp.replace(".", ",") +
                            " --> " + endTimeStamp.replace(".", ",") + "\r\n" + subtitle.text + "\r\n";
                    if (fixedLines[i]) {
                        fixedLines[i] += "\r\n" + fixedLine;
                    }
                    else {
                        fixedLines[i] = fixedLine;
                    }
                });
            var data = (isASS ? headingParts[0] + eventMatches[0] + "\r\n" : "") + fixedLines.join("\r\n") + headingParts[1] + "\r\n";
            track.data = data;
            files.push({
                name: track.name,
                data: data,
                title: (_e = (_d = (_c = heading.split("\n")) === null || _c === void 0 ? void 0 : _c[1]) === null || _d === void 0 ? void 0 : _d.substring(7)) !== null && _e !== void 0 ? _e : "Unknown",
                tracks: Object.values(_this.tracks),
                subtitles: track.subtitles
            });
        });
        return files;
    };
    return SubsExtractionTask;
}());
function extractSubsFromVideoFile(file, atData) {
    return __awaiter(this, void 0, void 0, function () {
        var tracks, parser, fileStream, stat, task;
        var _a, _b;
        return __generator(this, function (_c) {
            switch (_c.label) {
                case 0:
                    if (!file.startsWith('file://')) return [3 /*break*/, 3];
                    file = file.substring(7);
                    return [4 /*yield*/, fs.promises.stat(file)];
                case 1:
                    stat = _c.sent();
                    return [4 /*yield*/, getTracksOfFile(file, fs.createReadStream.bind(this, file, { highWaterMark: (stat.blksize * stat.blocks) / 256 }))];
                case 2:
                    (_a = _c.sent(), tracks = _a.tracks, parser = _a.parser, fileStream = _a.fileStream);
                    return [3 /*break*/, 5];
                case 3: return [4 /*yield*/, getTracksOfFile(file, streamFromURL)];
                case 4:
                    (_b = _c.sent(), tracks = _b.tracks, parser = _b.parser, fileStream = _b.fileStream);
                    _c.label = 5;
                case 5:
                    task = new SubsExtractionTask(fileStream, tracks, parser, atData);
                    task.start();
                    return [2 /*return*/, task];
            }
        });
    });
}
function getTracksOfFile(file, createStreamMethod) {
    var _this = this;
    return new Promise(function (resolve) { return __awaiter(_this, void 0, void 0, function () {
        var stream, parser;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, createStreamMethod(file)];
                case 1:
                    stream = _a.sent();
                    parser = new Matroska();
                    stream.pipe(parser);
                    parser.once('tracks', function (rawTracks) { return __awaiter(_this, void 0, void 0, function () {
                        var tracks, _i, rawTracks_1, track, isASS, extName, _a;
                        var _b;
                        return __generator(this, function (_c) {
                            switch (_c.label) {
                                case 0:
                                    tracks = {};
                                    stream.unpipe(parser);
                                    for (_i = 0, rawTracks_1 = rawTracks; _i < rawTracks_1.length; _i++) {
                                        track = rawTracks_1[_i];
                                        tracks[track.number] = track;
                                        isASS = track.header.includes("Format:"), extName = isASS ? ".ass" : ".srt";
                                        track.name = track.language ? track.language + extName : "Subtitle " + track.number + extName;
                                    }
                                    _a = resolve;
                                    _b = { tracks: tracks, parser: parser };
                                    return [4 /*yield*/, createStreamMethod(file)];
                                case 1:
                                    _a.apply(void 0, [(_b.fileStream = _c.sent(), _b)]);
                                    return [2 /*return*/];
                            }
                        });
                    }); });
                    return [2 /*return*/];
            }
        });
    }); });
}
function createDecoderFromStream(stream, atData, tracks, prevDecoder) {
    var matroskaDecoder = new Matroska(prevDecoder);
    var emitTimeout, emitTimeoutStop = false, emitTimeoutMiliseconds = 5000, thereIsAnEmitWaiting = false;
    matroskaDecoder.on("subtitle", function (subtitle, trackNumber) {
        var track = tracks[trackNumber];
        if (!track.subtitles)
            track.subtitles = [];
        track.subtitles.push(subtitle);
        thereIsAnEmitWaiting = true;
        if (emitTimeoutStop)
            return;
        thereIsAnEmitWaiting = false;
        emitTimeoutStop = true;
        clearTimeout(emitTimeout);
        emitTimeout = setTimeout(function () {
            emitTimeoutStop = false;
            if (thereIsAnEmitWaiting)
                atData();
        }, emitTimeoutMiliseconds);
        atData();
    });
    stream.pipe(matroskaDecoder);
    return {
        decoder: matroskaDecoder,
        promise: new Promise(function (resolve) {
            matroskaDecoder.once("finish", function () { resolve(atData()); });
        })
    };
}
function formatTimestamp(timestamp) {
    var seconds = timestamp / 1000;
    var hh = Math.floor(seconds / 3600);
    var mm = Math.floor((seconds - hh * 3600) / 60);
    var ss = (seconds - hh * 3600 - mm * 60).toFixed(2);
    if (mm < 10)
        mm = "0".concat(mm);
    if (parseFloat(ss) < 10)
        ss = "0".concat(ss);
    return "".concat(hh, ":").concat(mm, ":").concat(ss);
}
function formatTimestampSRT(timestamp) {
    var seconds = timestamp / 1000;
    var hh = Math.floor(seconds / 3600);
    var mm = Math.floor((seconds - hh * 3600) / 60);
    var ss = (seconds - hh * 3600 - mm * 60).toFixed(3);
    if (hh < 10)
        hh = "0".concat(hh);
    if (mm < 10)
        mm = "0".concat(mm);
    if (parseFloat(ss) < 10)
        ss = "0".concat(ss);
    return "".concat(hh, ":").concat(mm, ":").concat(ss);
}
module.exports = { extractSubsFromVideoFile: extractSubsFromVideoFile };
