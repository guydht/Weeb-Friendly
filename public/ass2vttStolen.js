function copyFrenchDude(parsed) {
    const output = ["WEBVTT\n"].concat(styleFromParsed(parsed));
    const styleAlignments = alignmentFromStyle(parsed);

    const TV = {
        width: parsed.info.PlayResX,
        height: parsed.info.PlayResY,
    };

    parsed.events.dialogue.forEach((dialogue, i) => {
        output.push(i + 1);
        const start = toVTTtime(Math.round(dialogue.Start * 100) / 100);
        const end = toVTTtime(Math.round(dialogue.End * 100) / 100);
        const firstLine = `${start} --> ${end}`;
        output.push(_position(firstLine, tagsOfDialogue(dialogue), TV, dialogue.Text.combined.includes("\\N"), styleAlignments[dialogue.Style]));

        const personTalking = dialogue.Name ? `<v ${dialogue.Name.replace(/\s/g, "_")}>` : "";
        output.push(personTalking + toVTTText(_quadratins(dialogue.Text.combined), dialogue.Style.replace(/\s/g, "_"), dialogue.additionalStyle));

        output.push("");
    });
    return output.join("\n");
}

// Given a parsed ASS dialogue, return an object containing all its text's tags.
function tagsOfDialogue(dialogue) {
    return dialogue.Text.parsed.reduce((acc, p) => Object.assign(acc, p.tags.reduce((acc, tag) => Object.assign(acc, tag), {})), {});
}

function toVTTText(text, style, additionalStyle) {
    if (style && additionalStyle)
        return `<c.${style}><c.${additionalStyle}>${text}</c></c>`;
    if (style)
        return `<c.${style}>${text}</c>`;
    return text;
}


function convertColor(color) {
    if (!color) return "";
    let num = color.startsWith("&H") ? parseInt(color.slice(2), 16) : parseInt(color, 16);
    if (num < 0) {
        // some tools generates negative integer if the value >= 2 ** 31
        num += 2 ** 31;
    }
    // parse as (A)BGR
    const R = num & 0xFF;
    const G = (num & 0xFF00) >> 8;
    const B = (num & 0xFF0000) >> 16;
    // 1 by default. Note that this also converts explicit 0 value to 1
    // (It seems some real world examples uses explicit 0 values with unknown reason)
    const A = (num & 0xFF0000) >> 24 || 255;
    if (A === 255) {
        return `#${R.toString(16)}${G.toString(16)}${B.toString(16)}`;
    }
    else {
        // MSEdge 16 does not support #RGBA
        return `rgba(${R}, ${G}, ${B}, ${A / 255})`;
    }
}

function toVTTtime(number) {
    const exampleTime = new Date("1995-12-17T00:00:00");
    exampleTime.setMilliseconds(number * 1000);
    let time_tab = [
        exampleTime.getHours(),
        exampleTime.getMinutes(),
        exampleTime.getSeconds(),
        exampleTime.getMilliseconds(),
    ].map((v, i) => {
        if (i < 3) {
            v = v < 10 ? "0" + v : v;
        } else {
            v = v === 0 ? "000" : v;
            v = v < 100 && v >= 10 ? "0" + v : v;
        }
        return v;
    });
    return `${time_tab[0]}:${time_tab[1]}:${time_tab[2]}.${time_tab[3]}`;
}

const assAlignmentToVttAlign = {
    "1": "left",
    "2": "center",
    "3": "right",
    "4": "left",
    "5": "center",
    "6": "right",
    "7": "left",
    "8": "center",
    "9": "right",
    "10": "left",
    "11": "center",
    "12": "right",
};

function alignmentFromStyle(parsed) {
    let output = {};
    parsed.styles.style.forEach(s => {
        output[s[0].replace(/\s/g, "_")] = parseInt(s[18]);
    });
    return output;
}

function createStylesForInlineStyles(parsed) {
    const output = [];
    parsed.events.dialogue.forEach((dialogue, dialogueIndex) => {
        const tag = tagsOfDialogue(dialogue);
        if (!Object.keys(tag).length) return;
        const currentDialogeID = `generatedStyle${dialogueIndex}`;
        dialogue.additionalStyle = currentDialogeID;
        output.push(`::cue(.${currentDialogeID}){`);
        if (tag.i == 1)
            output.push("font-style: italic;");
        if (tag.alpha)
            output.push("opacity: " + tag.alpha + ";");
        if (tag.c || tag.c1)
            output.push("color: " + convertColor(tag.c || tag.c1) + ";");
        if (tag.b > 0)
            output.push(`font-weight: ${(tag.b == 1 ? "bold" : tag.b)};`);
        if (tag.u == 1 || tag.s == 1)
            output.push(`text-decoration: ${tag.u == 1 ? "underline" : ""} ${tag.s == 1 ? "line-through" : ""};`);
        if (tag.fs)
            output.push(`font-size: ${calculateFontSize(parsed.info.PlayResY, tag.fs, tag.fscx, tag.fscy)}vh;`);
        if (tag.fn)
            output.push(`font-family: ${tag.fn};`);
        if (tag.bord || tag.c3)
            output.push(`outline: ${tag.bord ? tag.bord + "px" : ""} ${convertColor(tag.c3)};`);
        if (tag.shad || tag.c4)
            output.push(`text-shadow: ${convertColor(tag.c4)} ${tag.shad};`);
        output.push("}");
    });
    return output;
}


function styleFromParsed(parsed) {
    let style = ["STYLE"];
    parsed.styles.style.forEach(parsedStyle => {
        style.push("::cue(." + parsedStyle[0].replace(/\s/g, "_") + "){");
        if (parsedStyle[1])
            style.push("font-family: " + parsedStyle[1] + ";");
        if (parsedStyle[3])
            style.push("color: " + convertColor(parsedStyle[3]) + ";");
        if (parsedStyle[15] === "1") {
            const outlineColor = convertColor(parsedStyle[5]),
                outlineWidth = parsedStyle[16];
            style.push(`text-shadow: -${outlineWidth}px -${outlineWidth}px 0px ${outlineColor}, ${outlineWidth}px -${outlineWidth}px 0px ${outlineColor}, -${outlineWidth}px ${outlineWidth}px 0px ${outlineColor}, ${outlineWidth}px ${outlineWidth}px 0px ${outlineColor};`);
        }
        if (parsedStyle[7] === "-1")
            style.push("font-weight: bold;");
        if (parsedStyle[7] === "-1")
            style.push("font-style: italic;");
        if (parsedStyle[9] === "-1" || parsedStyle[10] === "-1")
            style.push(`text-decoration: ${parsedStyle[9] === "-1" ? "underline" : ""} ${parsedStyle[10] === "-1" ? "line-through" : ""};`);
        style.push("font-size: " + calculateFontSize(parsed.info.PlayResY, parsedStyle[2], parsedStyle[11], parsedStyle[12]) + "vh;");
        style.push("}");
    });
    const additionalStyles = createStylesForInlineStyles(parsed);
    style.push(...additionalStyles, "");
    return style;
}

function calculateFontSize(PlayResY, fontSize, scaleX, scaleY) {
    return (parseInt(fontSize) * (100 / PlayResY) * (100 / (scaleX === scaleY && scaleX ? parseInt(scaleX) : 100)));
}

// From french dude ew
function _position(firstLine, tag, TV, multiline, styleAlignment) {
    const output = [firstLine];
    const an = tag.an || styleAlignment;
    if (an % 3 == 1) {
        output.push("align:start");
    } else if (an % 3 == 2) {
        output.push("align:center");
    } else if (an % 3 == 0) {
        output.push("align:end");
    }
    if (tag.pos) {
        let pos = tag.pos;
        const posX = pos[0],
            posY = pos[1];
        let left = Math.round((posX / TV.width) * 100);
        let top = Math.round((posY / TV.height) * 100);
        if (multiline) {
            top = Math.round((posY / TV.height) * 100 - 5);
        }
        output.push("line:" + top + "%");
        output.push("position:" + left + "%");
        output.push(`size: 100%`);
    }


    return output.join(" ");
}

function _quadratins(txt) {
    txt = (txt.substring(0, 1) === "-" && txt.replace(/-/, "–")) || txt;
    txt = txt.replace(/\\N-/g, "\r\n–").replace(/\}-/g, "}–");

    txt = txt
        .replace(/\\N/g, "\r\n")
        .replace(/\\n/g, " ")
        .replace(/\\h/g, "&nbsp;")
        .replace(/\\N/g, "\n")
        .replace(/{\\i1}/g, "<i>")
        .replace(/{\\i0}/g, "</i>")
        .replace(/{[^}]+}/g, "");
    return txt;
}
module.exports = { copyFrenchDude };




/**

[Script Info]
Title: English (US)
Original Translation: 
Original Editing: 
Original Timing: 
Synch Point: 
Script Updated By: 
Update Details: 
ScriptType: v4.00+
Collisions: Normal
PlayResX: 640
PlayResY: 360
WrapStyle: 0
ScaledBorderAndShadow: yes

[V4+ Styles]
Format: Name, Fontname, Fontsize, PrimaryColour, SecondaryColour, OutlineColour, BackColour, Bold, Italic, Underline, StrikeOut, ScaleX, ScaleY, Spacing, Angle, BorderStyle, Outline, Shadow, Alignment, MarginL, MarginR, MarginV, Encoding
Style: Default,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.3,0,2,20,20,23,0
Style: Main - Italics,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,-1,0,0,100,100,0,0,1,1.3,0,2,20,20,23,0
Style: Main - Top,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.3,0,8,20,20,23,0
Style: Main - Top + Italics,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,-1,0,0,100,100,0,0,1,1.3,0,8,20,20,23,0
Style: Flashback,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.3,0,2,20,20,23,1
Style: Flashback - Italics,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,-1,0,0,100,100,0,0,1,1.3,0,2,20,20,23,1
Style: Flashback - Top,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.3,0,8,20,20,23,1
Style: Flashback - Top + Italics,Roboto Medium,26,&H00FFFFFF,&H000000FF,&H00000000,&H00000000,0,0,0,0,100,100,0,0,1,1.3,0,8,20,20,23,1

[Events]
Format: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text
Dialogue: 0,0:00:10.74,0:00:13.45,Default,text,0,0,0,,{\fad(1,1190)\fnGeorgia\shad0\bord1\fs20\blur4\3c&H444242&\pos(496.615,136.629)\c&HFFFFFB&}Thursday, June 11th
Dialogue: 0,0:00:17.34,0:00:20.35,Flashback,Maaya,0,0,0,,Oh! And here's your big brother!
Dialogue: 0,0:00:23.21,0:00:28.29,Flashback,Saki,0,0,0,,Sorry. Maaya said she wanted \Nto come see my new home.
Dialogue: 0,0:00:28.29,0:00:33.00,Flashback,Saki,0,0,0,,I didn't have your cell number, \Nso I tried calling the home phone.
Dialogue: 0,0:00:33.40,0:00:36.05,Flashback,Yuta,0,0,0,,Ah, I was probably in the shower.
Dialogue: 0,0:00:36.65,0:00:41.60,Flashback,Saki,0,0,0,,Also, I told Maaya that \Nyou and I are siblings now.
Dialogue: 0,0:00:41.60,0:00:42.54,Flashback,Yuta,0,0,0,,I don't really min—
Dialogue: 0,0:00:42.54,0:00:48.55,Flashback,Saki,0,0,0,,It sounds like she was watching when you \Ncame over to talk to me during tennis.
Dialogue: 0,0:00:49.59,0:00:52.83,Flashback,Saki,0,0,0,,And when you gave me the umbrella.
Dialogue: 0,0:00:56.18,0:00:59.30,Flashback,Yuta,0,0,0,,Sorry. I should've been more careful.
Dialogue: 0,0:01:02.30,0:01:03.29,Flashback,Saki,0,0,0,,So...
Dialogue: 0,0:01:05.34,0:01:07.37,Flashback,Saki,0,0,0,,can we exchange numbers?
Dialogue: 0,0:01:13.97,0:01:14.88,Default,Saki,0,0,0,,Oh?
Dialogue: 0,0:01:15.39,0:01:16.91,Default,Akiko,0,0,0,,Good morning!
Dialogue: 0,0:01:16.91,0:01:17.84,Default,Saki,0,0,0,,Good morning.
Dialogue: 0,0:01:18.35,0:01:19.73,Default,Saki,0,0,0,,Why aren't you in bed?
Dialogue: 0,0:01:19.73,0:01:21.97,Default,Akiko,0,0,0,,I had trouble falling asleep.
Dialogue: 0,0:01:21.97,0:01:25.88,Default,Akiko,0,0,0,,Also, hey, Taichi-san said \Nhe'd be leaving late today,
Dialogue: 0,0:01:25.88,0:01:28.23,Default,Akiko,0,0,0,,so I thought maybe we could all eat together.
Dialogue: 0,0:01:28.90,0:01:29.90,Default,Saki,0,0,0,,I'll help.
Dialogue: 0,0:01:31.54,0:01:32.48,Default,Akiko,0,0,0,,Thank you.
Dialogue: 0,0:01:33.83,0:01:37.19,Default,Akiko,0,0,0,,It's been a while since we've \Nhad breakfast together, huh?
Dialogue: 0,0:01:37.19,0:01:38.01,Default,Saki,0,0,0,,Yeah.
Dialogue: 0,0:01:42.01,0:01:44.45,Default,Taichi,0,0,0,,Man, this is paradise.
Dialogue: 0,0:01:44.95,0:01:49.46,Default,text,0,0,0,,{\fad(730,650)\an1\fnGeorgia\fs10\shad0\bord0\c&HAEB66B&\b1\pos(48.607,223.273)}Episode 3 {\fs14}"Reflection and {\c&HAB77D7&}Re{\c&HB5D375&}vision"
Dialogue: 0,0:01:45.31,0:01:47.20,Default,Akiko,0,0,0,,Don't be so dramatic!
Dialogue: 0,0:01:47.69,0:01:48.67,Default,Yuta,0,0,0,,Yum.
Dialogue: 0,0:01:48.67,0:01:49.45,Default,Taichi,0,0,0,,Hmm?
Dialogue: 0,0:01:51.67,0:01:53.43,Default,Taichi,0,0,0,,You're right! Yum!
Dialogue: 0,0:01:53.43,0:01:55.24,Default,Yuta,0,0,0,,Is this a rolled omelet?
Dialogue: 0,0:01:55.94,0:01:57.64,Default,Saki,0,0,0,,A rolled omelet with broth.
Dialogue: 0,0:01:57.64,0:01:58.47,Default,Yuta,0,0,0,,With broth?
Dialogue: 0,0:01:59.21,0:02:01.19,Default,Saki,0,0,0,,You add dashi broth when you make it.
Dialogue: 0,0:02:02.37,0:02:03.98,Default,Yuta,0,0,0,,Like the soup for noodles?
Dialogue: 0,0:02:03.98,0:02:06.42,Default,Saki,0,0,0,,Well, we actually use white dashi.
Dialogue: 0,0:02:06.42,0:02:11.07,Default,Akiko,0,0,0,,Saki knows the recipe. Why don't \Nyou make it for him next time?
Dialogue: 0,0:02:11.07,0:02:14.40,Default,Saki,0,0,0,,I can't get mine this fluffy, though.
Dialogue: 0,0:02:16.62,0:02:19.54,Default,Yuta,0,0,0,,Oh, I like regular fried eggs, too.
Dialogue: 0,0:02:20.39,0:02:21.53,Default,Saki,0,0,0,,Really?
Dialogue: 0,0:02:22.14,0:02:24.50,Default,Saki,0,0,0,,Well, maybe I'll try if I'm in the mood.
Dialogue: 0,0:02:28.76,0:02:30.54,Default,Akiko,0,0,0,,Thank you for helping, Yuta-kun.
Dialogue: 0,0:02:31.02,0:02:32.57,Default,Yuta,0,0,0,,No trouble at all.
Dialogue: 0,0:02:33.06,0:02:38.08,Default,Yuta,0,0,0,,Oh, is it all right if I throw your \Nclothes in with this week's laundry?
Dialogue: 0,0:02:38.69,0:02:39.71,Default,Saki,0,0,0,,That's kind of...
Dialogue: 0,0:02:40.18,0:02:47.29,Default,Akiko,0,0,0,,Actually, if you don't mind, Yuta-kun, \NI'd be happy to do all the laundry myself.
Dialogue: 0,0:02:47.29,0:02:50.11,Default,Yuta,0,0,0,,What? No, I can't make you do that.
Dialogue: 0,0:02:51.06,0:02:55.77,Default,Saki,0,0,0,,A lot of the fabrics are super delicate. \NDo you know how to use a laundry bag?
Dialogue: 0,0:02:56.13,0:02:57.07,Default,Yuta,0,0,0,,Laundry bag?
Dialogue: 0,0:02:57.70,0:03:01.72,Default,Akiko,0,0,0,,It's made of mesh netting to \Nprotect the clothes from damage.
Dialogue: 0,0:03:02.22,0:03:04.69,Default,Yuta,0,0,0,,Damage? Huh?
Dialogue: 0,0:03:04.69,0:03:07.87,Default,Saki,0,0,0,,It's hard to explain, so we'll do it ourselves.
Dialogue: 0,0:03:07.87,0:03:09.19,Default,Yuta,0,0,0,,Oh, okay.
Dialogue: 0,0:03:09.65,0:03:14.94,Default,Akiko,0,0,0,,I'll wash Taichi-san's underwear. \NShould I wash yours, too, Yuta-kun?
Dialogue: 0,0:03:15.32,0:03:16.08,Default,Yuta,0,0,0,,Huh?
Dialogue: 0,0:03:24.90,0:03:25.78,Default,Saki,0,0,0,,Wait.
Dialogue: 0,0:03:27.02,0:03:28.46,Default,Saki,0,0,0,,I'll go with you.
Dialogue: 0,0:03:31.86,0:03:33.40,Default,Saki,0,0,0,,I apologize.
Dialogue: 0,0:03:33.40,0:03:34.29,Default,Yuta,0,0,0,,Huh?
Dialogue: 0,0:03:34.29,0:03:39.86,Default,Saki,0,0,0,,I realize there's a possibility that \Nyou prefer women's underwear.
Dialogue: 0,0:03:40.12,0:03:42.29,Default,Saki,0,0,0,,It's not out of the question.
Dialogue: 0,0:03:42.29,0:03:43.03,Default,Yuta,0,0,0,,Huh?
Dialogue: 0,0:03:43.75,0:03:48.10,Default,Saki,0,0,0,,Even though I've always \Nrejected rigid gender roles,
Dialogue: 0,0:03:48.10,0:03:50.54,Default,Saki,0,0,0,,I just assumed you didn't know \Nhow to do the laundry.
Dialogue: 0,0:03:50.54,0:03:51.70,Default,Yuta,0,0,0,,Ayase-san?
Dialogue: 0,0:03:51.70,0:03:54.86,Default,Saki,0,0,0,,I've never seen you with \Nlipstick or foundation on,
Dialogue: 0,0:03:54.86,0:03:58.03,Default,Saki,0,0,0,,but you might be the type who \Nonly likes to doll up in private.
Dialogue: 0,0:03:58.03,0:04:01.01,Default,Yuta,0,0,0,,Hold on. Calm down for a second, Ayase-san.
Dialogue: 0,0:04:01.01,0:04:01.88,Default,Saki,0,0,0,,Huh?
Dialogue: 0,0:04:12.46,0:04:14.14,Default,Saki,0,0,0,,I've calmed down.
Dialogue: 0,0:04:14.82,0:04:15.82,Default,Yuta,0,0,0,,Okay.
Dialogue: 0,0:04:15.82,0:04:17.81,Default,Saki,0,0,0,,Even if you're interested in cross-dressing,
Dialogue: 0,0:04:17.81,0:04:20.49,Default,Saki,0,0,0,,it doesn't have to mean you actually do it.
Dialogue: 0,0:04:22.45,0:04:26.92,Default,Yuta,0,0,0,,I don't cross-dress, and I don't put \Nmakeup on when no one's looking.
Dialogue: 0,0:04:26.92,0:04:29.91,Default,Saki,0,0,0,,But your eyebrows are shaped really nicely.
Dialogue: 0,0:04:30.50,0:04:31.36,Default,Yuta,0,0,0,,Huh?
Dialogue: 0,0:04:31.36,0:04:34.28,Default,Saki,0,0,0,,You must get them done, right? \NDo you go to a salon?
Dialogue: 0,0:04:34.28,0:04:35.63,Default,Yuta,0,0,0,,A barber shop.
Dialogue: 0,0:04:35.63,0:04:38.87,Default,Saki,0,0,0,,Huh? You mean your eyebrows \Nare totally natural?
Dialogue: 0,0:04:39.62,0:04:40.83,Default,Yuta,0,0,0,,Well, yeah.
Dialogue: 0,0:04:41.96,0:04:43.69,Default,Saki,0,0,0,,I'm kind of jealous.
Dialogue: 0,0:04:47.64,0:04:51.77,Default,Saki,0,0,0,,Don't you think it's hard to force all \Nsexual difference into just two categories?
Dialogue: 0,0:04:51.77,0:04:53.90,Default,Yuta,0,0,0,,Huh... Yeah, I guess so.
Dialogue: 0,0:04:54.66,0:05:01.34,Default,Saki,0,0,0,,But earlier, in the moment, \Nthat thought didn't even cross my mind.
Dialogue: 0,0:05:02.29,0:05:04.39,Default,Yuta,0,0,0,,I'm really not worried about it.
Dialogue: 0,0:05:05.67,0:05:07.53,Default,Saki,0,0,0,,Thanks. I appreciate it.
Dialogue: 0,0:05:08.12,0:05:11.02,Default,Saki,0,0,0,,But I can't forgive myself.
Dialogue: 0,0:05:11.37,0:05:13.18,Default,Saki,0,0,0,,That's why I wanted to apologize.
Dialogue: 0,0:05:18.25,0:05:20.31,Default,Yuta,0,0,0,,Sometimes we do things reflexively.
Dialogue: 0,0:05:20.31,0:05:22.68,Default,Saki,0,0,0,,Reflectively? Like light?
Dialogue: 0,0:05:22.68,0:05:26.00,Default,Yuta,0,0,0,,No, not that. I'm talking\N about conditioned reflexes.
Dialogue: 0,0:05:26.93,0:05:28.34,Default,Saki,0,0,0,,Oh...
Dialogue: 0,0:05:28.74,0:05:34.73,Default,Yuta,0,0,0,,I think sometimes you say or do things \Non instinct and can't really help it.
Dialogue: 0,0:05:35.14,0:05:40.19,Default,Yuta,0,0,0,,I'm sure the ability to react reflexively \Nlike that is useful in certain situations.
Dialogue: 0,0:05:40.74,0:05:43.83,Default,Saki,0,0,0,,But still, prejudice leads to discrimination.
Dialogue: 0,0:05:47.00,0:05:49.29,Default,Yuta,0,0,0,,That's why we need to reexamine things.
Dialogue: 0,0:06:00.92,0:06:05.18,Default,Yuta,0,0,0,,Ayase-san, you've already reexamined \Nand reflected on what you did earlier.
Dialogue: 0,0:06:05.63,0:06:09.93,Default,Yuta,0,0,0,,Once you've done that, I don't think \Nyou need to keep worrying about it.
Dialogue: 0,0:06:12.73,0:06:13.81,Default,Yuta,0,0,0,,Personally...
Dialogue: 0,0:06:14.53,0:06:19.39,Default,Yuta,0,0,0,,I don't think you're the type who can't \Nreflect on and learn from what you've done.
Dialogue: 0,0:06:20.19,0:06:25.11,Default,text,0,0,0,,{\fnArial\fs11\b1\shad0\an8\pos(146,186)\frz1.894\clip(m 184 304 l 176 304 171 305 170 305 166 306 163 306 160 306 155 307 153 307 150 308 148 309 137 317 139 154 185 153)\c&H205B4C&\3c&H3A9B85&}B\NI\NC\NY\NC\NL\NE\NS\N\NM\NU\NS\NT
Dialogue: 0,0:06:29.63,0:06:30.81,Default,Yuta,0,0,0,,Ayase-san?
Dialogue: 0,0:06:34.97,0:06:38.00,Default,Saki,0,0,0,,Asamura-kun, you really...
Dialogue: 0,0:06:45.50,0:06:48.51,Default,text,0,0,0,,{\fad(360,1)\fnGeorgia\shad0\bord1\fs20\blur4\3c&H727574&\pos(320,145)\c&HEDEFEF&}understand me too well.
Dialogue: 0,0:07:01.26,0:07:02.44,Default,student,0,0,0,,All right, let's go!
Dialogue: 0,0:07:23.14,0:07:24.46,Default,Saki,0,0,0,,You're already back.
Dialogue: 0,0:07:26.91,0:07:28.17,Default,Saki,0,0,0,,What?
Dialogue: 0,0:07:28.17,0:07:30.97,Default,Yuta,0,0,0,,No, it's nothing. Don't worry about it.
Dialogue: 0,0:07:30.97,0:07:31.72,Default,Saki,0,0,0,,Really?
Dialogue: 0,0:07:31.72,0:07:33.93,Default,Yuta,0,0,0,,Uh, I've got work today.
Dialogue: 0,0:07:43.56,0:07:47.07,Default,text,0,0,0,,{\fnGeorgia\fs12\shad0\bord0\c&H847F45&\b1\pos(218.777,97.54)}You were looking, weren't you?
Dialogue: 0,0:07:46.12,0:07:50.32,Default,Saki,0,0,0,,So your eyes were drawn to \Nthe hanging undergarments,
Dialogue: 0,0:07:50.32,0:07:54.43,Default,Saki,0,0,0,,which is why, when I startled you, \Nyou up and ran.
Dialogue: 0,0:07:55.10,0:07:59.60,Default,Saki,0,0,0,,Did you think I might've thought you \Nwere about to go in and steal them?
Dialogue: 0,0:07:59.60,0:08:01.99,Default,Yuta,0,0,0,,Well... I guess.
Dialogue: 0,0:08:02.90,0:08:04.98,Default,Saki,0,0,0,,Even though they're your little sister's?
Dialogue: 0,0:08:04.98,0:08:08.01,Default,Yuta,0,0,0,,Well, yeah. I'm aware of that.
Dialogue: 0,0:08:09.23,0:08:11.96,Default,Saki,0,0,0,,Sorry. That was a little unfair.
Dialogue: 0,0:08:12.31,0:08:12.97,Default,Yuta,0,0,0,,Huh?
Dialogue: 0,0:08:13.56,0:08:15.14,Default,Saki,0,0,0,,Here. Have some cocoa.
Dialogue: 0,0:08:16.15,0:08:18.97,Default,Saki,0,0,0,,Don't want it? I can take it back.
Dialogue: 0,0:08:18.97,0:08:22.22,Default,Yuta,0,0,0,,No, that's okay. I'll take it.
Dialogue: 0,0:08:24.76,0:08:27.47,Default,Saki,0,0,0,,I guess this makes us even now.
Dialogue: 0,0:08:27.47,0:08:27.94,Default,Yuta,0,0,0,,Huh?
Dialogue: 0,0:08:28.91,0:08:33.43,Default,Saki,0,0,0,,Your eyes being drawn to my underwear \Nis another kind of reflexive reaction.
Dialogue: 0,0:08:34.19,0:08:39.52,Default,Saki,0,0,0,,But I don't think you're the type who can't\N reflect on and learn from what you've done.
Dialogue: 0,0:08:43.10,0:08:45.45,Default,Yuta,0,0,0,,Well, that's a relief.
Dialogue: 0,0:08:46.42,0:08:52.19,Default,Saki,0,0,0,,So does this mean my underwear was\N tempting enough to you to catch your eye?
Dialogue: 0,0:08:52.19,0:08:54.11,Default,Yuta,0,0,0,,I didn't say that.
Dialogue: 0,0:08:54.11,0:08:57.52,Default,Saki,0,0,0,,So it wasn't tempting at all? Huh.
Dialogue: 0,0:08:58.17,0:09:00.96,Default,Yuta,0,0,0,,Are you teasing me right now?
Dialogue: 0,0:09:01.31,0:09:02.85,Default,Saki,0,0,0,,Oh, I wonder.
Dialogue: 0,0:09:03.98,0:09:08.69,Default,Saki,0,0,0,,But you can't say the thought of stealing \Nmy underwear didn't cross your mind, right?
Dialogue: 0,0:09:08.69,0:09:12.44,Default,Yuta,0,0,0,,I'd be lying if I said I didn't \Nfeel any urge to take it,
Dialogue: 0,0:09:12.44,0:09:14.59,Default,Yuta,0,0,0,,but that doesn't mean I'd act on it.
Dialogue: 0,0:09:15.42,0:09:18.49,Default,Saki,0,0,0,,Hmm. So you did feel an urge.
Dialogue: 0,0:09:19.14,0:09:23.92,Default,Yuta,0,0,0,,Having an urge to do something and acting \Non that urge are two different things.
Dialogue: 0,0:09:26.87,0:09:28.09,Default,Saki,0,0,0,,You're right.
Dialogue: 0,0:09:28.09,0:09:30.88,Default,Saki,0,0,0,,Sorry. Let's drop it.
Dialogue: 0,0:09:31.29,0:09:34.88,Default,Saki,0,0,0,,I've already eaten dinner and had my bath.
Dialogue: 0,0:09:35.42,0:09:37.60,Default,Yuta,0,0,0,,Huh? Oh, okay.
Dialogue: 0,0:09:38.35,0:09:39.01,Default,Saki,0,0,0,,Goodnight.
Dialogue: 0,0:09:40.79,0:09:41.95,Default,Yuta,0,0,0,,Goodnight.
Dialogue: 0,0:09:46.27,0:09:49.27,Default,text,0,0,0,,{\fad(1,900)\fnGeorgia\shad0\bord1\fs18\blur4\3c&H355E31&\pos(461.11,148)\c&HE6FBF4&}Friday, June 12th
Dialogue: 0,0:09:55.63,0:09:57.15,Default,Maaya,0,0,0,,Hi, Big Brother.
Dialogue: 0,0:09:58.39,0:10:00.10,Default,Yuta,0,0,0,,Hey, not at school!
Dialogue: 0,0:10:00.10,0:10:01.72,Default,Maaya,0,0,0,,Did something happen to Saki?
Dialogue: 0,0:10:01.72,0:10:02.45,Default,Yuta,0,0,0,,Huh?
Dialogue: 0,0:10:02.45,0:10:06.38,Default,Maaya,0,0,0,,She's been like that since this morning.
Dialogue: 0,0:10:11.96,0:10:14.42,Default,text,0,0,0,,{\fad(230,1)\fnGeorgia\fs14\shad0\bord0\c&H595CA4&\b1\pos(320,298)}You seem down. Did something happen?
Dialogue: 0,0:10:17.36,0:10:18.20,Default,student,0,0,0,,Here it comes!
Dialogue: 0,0:10:22.14,0:10:25.02,Default,text,0,0,0,,{\c&H728445&\fnGeorgia\fs12\shad0\bord0\t(1900,2880,\c&H060D0C&)\fad(1,980)\b1\pos(211.392,89.54)}No.
Dialogue: 0,0:10:28.42,0:10:31.66,Default,Yuta,0,0,0,,Dad's catching the last train home.
Dialogue: 0,0:10:31.66,0:10:32.42,Default,Saki,0,0,0,,Okay.
Dialogue: 0,0:11:14.02,0:11:17.43,Default,Yuta,0,0,0,,Sorry. I still haven't found a good, \Nhigh-paying job for you.
Dialogue: 0,0:11:18.19,0:11:20.82,Default,Saki,0,0,0,,I never expected you to find one immediately.
Dialogue: 0,0:11:20.82,0:11:26.84,Default,Yuta,0,0,0,,But at this rate, you're going to be \Nmaking me dinner for nothing.
Dialogue: 0,0:11:27.27,0:11:30.46,Default,Yuta,0,0,0,,If you want me to help out more, \Njust say the word.
Dialogue: 0,0:11:31.56,0:11:32.36,Default,Saki,0,0,0,,Sure.
Dialogue: 0,0:13:35.42,0:13:36.46,Default,Saki,0,0,0,,Asamura-kun...
Dialogue: 0,0:13:39.96,0:13:41.21,Default,text,0,0,0,,{\fnGeorgia\shad0\bord3\fs20\blur4\3c&H403628&\pos(320,154.223)\c&HFECDB7&\t(\c&HFECDB7&\3c&H8E3A75&)}Are you willing to buy...
Dialogue: 0,0:13:41.21,0:13:41.80,Default,text,0,0,0,,{\fnGeorgia\shad0\bord3\fs20\blur4\3c&H811F38&\t(1,190,\3c&H1C1615&)\pos(320,154.223)\c&HFECDB7&}Are you willing to buy...
Dialogue: 0,0:13:41.80,0:13:43.46,Default,text,0,0,0,,{\fnGeorgia\shad0\bord3\3c&H1C1615&\fs20\blur4\t(1,430,\3c&H3F2A8B&)\pos(320,154.223)\c&HFECDB7&}Are you willing to buy...
Dialogue: 0,0:13:47.22,0:13:48.97,Default,text,0,0,0,,{\fnGeorgia\shad0\bord3\fs20\blur4\3c&HFEFEE8&\pos(320,154.223)\c&HE5DCBD&}my body?
Dialogue: 0,0:14:31.76,0:14:34.01,Default,text,0,0,0,,{\fnGeorgia\fs18\shad0\bord0\fay-.11\c&H887B75&\move(369.167,138.5,347.667,156,1,1820)}Asamura\h\h\h\h\h\h\h\h\h\hAyase
Dialogue: 0,0:14:37.02,0:14:38.27,Default,text,0,0,0,,{\fnGeorgia\shad0\bord0\fs12\3c&H444242&\c&HB8B5C1&\pos(379.5,212.091)\b1}Sunday, June 7th
Dialogue: 0,0:14:37.34,0:14:40.30,Main - Italics,Saki,0,0,0,,Sunday, June 7th.
Dialogue: 0,0:14:44.84,0:14:48.38,Main - Italics,Saki,0,0,0,,I'm relieved, to be honest.
Dialogue: 0,0:14:48.90,0:14:53.40,Main - Italics,Saki,0,0,0,,I could tell when we were introduced \Nthat he wasn't a bad person.
Dialogue: 0,0:14:54.07,0:14:57.24,Main - Italics,Saki,0,0,0,,And also that he's considerate.
Dialogue: 0,0:14:58.66,0:15:03.83,Main - Italics,Saki,0,0,0,,He's the kind of guy who'll even draw\Na fresh bath for me when he's done.
Dialogue: 0,0:15:05.17,0:15:08.88,Main - Italics,Saki,0,0,0,,And I never expected\N that he attended Suisei, too.
Dialogue: 0,0:15:14.26,0:15:16.26,Default,text,0,0,0,,{\fnGeorgia\shad0\bord0\fs12\3c&H444242&\c&HADC2AE&\b1\pos(370.5,212.091)}Monday, June 8th
Dialogue: 0,0:15:14.97,0:15:17.66,Main - Italics,Saki,0,0,0,,Monday, June 8th.
Dialogue: 0,0:15:18.23,0:15:21.46,Main - Italics,Saki,0,0,0,,Asamura-kun came and talked to me at school.
Dialogue: 0,0:15:22.27,0:15:26.27,Main - Italics,Saki,0,0,0,,He's a lot more level-headed than I thought.
Dialogue: 0,0:15:27.72,0:15:31.91,Main - Italics,Saki,0,0,0,,I'm a little annoyed that he believed \Nthe rumors about me,
Dialogue: 0,0:15:32.58,0:15:34.82,Main - Italics,Saki,0,0,0,,but I can't really blame him.
Dialogue: 0,0:15:36.21,0:15:39.03,Main - Italics,Saki,0,0,0,,I know how people see me.
Dialogue: 0,0:15:40.14,0:15:45.79,Main - Italics,Saki,0,0,0,,But he got mad, and recognized \Nthat I was mad about it, too.
Dialogue: 0,0:15:46.60,0:15:52.33,Main - Italics,Saki,0,0,0,,This might be the first time anyone has \Nunderstood my feelings so effortlessly.
Dialogue: 0,0:15:54.18,0:15:56.18,Default,text,0,0,0,,{\fnGeorgia\shad0\bord0\fs12\3c&H444242&\c&HA0B4B6&\pos(379.5,212.091)\b1}Tuesday, June 9th
Dialogue: 0,0:15:55.07,0:15:57.62,Main - Italics,Saki,0,0,0,,Tuesday, June 9th.
Dialogue: 0,0:15:58.28,0:16:02.44,Main - Italics,Saki,0,0,0,,Memo: Asamura-kun likes \Nhis fried eggs with soy sauce.
Dialogue: 0,0:16:04.03,0:16:05.88,Main - Italics,Saki,0,0,0,,Today, I start cooking.
Dialogue: 0,0:16:05.88,0:16:10.13,Main - Italics,Saki,0,0,0,,Asamura-kun is trying to find \Na high-paying job for me,
Dialogue: 0,0:16:10.13,0:16:13.38,Main - Italics,Saki,0,0,0,,so I feel like it's the least I can do.
Dialogue: 0,0:16:15.88,0:16:20.58,Main - Italics,Saki,0,0,0,,He seemed apologetic when he told \Nme he hadn't found anything yet,
Dialogue: 0,0:16:21.77,0:16:26.76,Main - Italics,Saki,0,0,0,,but I really didn't think it \Nwould be that easy, you know?
Dialogue: 0,0:16:29.33,0:16:33.18,Main - Italics,Saki,0,0,0,,The right way to depend on people, huh?
Dialogue: 0,0:16:34.19,0:16:37.10,Main - Italics,Saki,0,0,0,,If I could do that, sure.
Dialogue: 0,0:16:39.60,0:16:41.60,Default,text,0,0,0,,{\fnGeorgia\shad0\bord0\fs12\3c&H444242&\c&HB1BAB1&\b1\pos(386.5,225.091)}Wednesday, June 10th
Dialogue: 0,0:16:53.88,0:16:56.90,Main - Italics,Saki,0,0,0,,Wednesday, June 10th.
Dialogue: 0,0:16:57.62,0:17:00.62,Default,text,0,0,0,,{\fad(60,1)\fnGeorgia\shad0\bord0\fs16\3c&H403628&\c&HBDBBB0&\pos(325.479,273.223)\b1}I was listening to music.
Dialogue: 0,0:16:59.26,0:17:00.62,Main - Italics,Saki,0,0,0,,I'm so embarrassed.
Dialogue: 0,0:17:00.62,0:17:02.37,Default,text,0,0,0,,{\fad(60,1)\fnGeorgia\shad0\bord0\fs16\3c&H403628&\c&HBDBBB0&\pos(325.479,273.223)\b1}I'm sorry I wasn't paying attention
Dialogue: 0,0:17:01.47,0:17:03.69,Main - Italics,Saki,0,0,0,,I never thought he'd ask me...
Dialogue: 0,0:17:04.66,0:17:06.62,Default,text,0,0,0,,{\fnGeorgia\shad0\bord0\fs16\3c&H403628&\c&H8588A9&\pos(320,280)\b1}English lessons?
Dialogue: 0,0:17:05.02,0:17:06.62,Main - Italics,Saki,0,0,0,,I don't want him to see
Dialogue: 0,0:17:06.62,0:17:09.13,Default,text,0,0,0,,{\fad(70,1)\fnGeorgia\shad0\bord0\fs16\3c&H403628&\c&HBDBBB0&\pos(325.479,273.223)\b1}What difference does it make?
Dialogue: 0,0:17:07.25,0:17:10.58,Main - Italics,Saki,0,0,0,,how hard I actually work. I know it's not cool.
Dialogue: 0,0:17:15.33,0:17:17.92,Main - Italics,Saki,0,0,0,,Maaya came to hang out at my new place.
Dialogue: 0,0:17:18.64,0:17:21.90,Main - Italics,Saki,0,0,0,,The three of us played \Ngames and laughed a lot.
Dialogue: 0,0:17:22.64,0:17:24.64,Main - Italics,Saki,0,0,0,,I hadn't laughed so much in a long time.
Dialogue: 0,0:17:27.11,0:17:29.36,Main - Italics,Saki,0,0,0,,We exchanged contact info.
Dialogue: 0,0:17:30.15,0:17:34.12,Main - Italics,Saki,0,0,0,,Asamura-kun uses a landscape \Nphoto as his profile picture.
Dialogue: 0,0:17:33.65,0:17:36.15,Default,text,0,0,0,,{\fnArial\fax.3\shad0\bord0\frz359.3\pos(464,169)\clip(378,133.5,524.5,194)\c&H878283&\b1}Asamura {\c&H373233&}Yuta
Dialogue: 0,0:17:34.12,0:17:35.26,Main - Italics,Saki,0,0,0,,I'm not surprised.
Dialogue: 0,0:17:36.15,0:17:38.16,Default,text,0,0,0,,{\fnGeorgia\shad0\bord0\fs12\3c&H444242&\c&H9196A6&\b1\pos(384,212.591)}Thursday, June 11th
Dialogue: 0,0:17:36.91,0:17:39.66,Main - Italics,Saki,0,0,0,,Thursday, June 11th.
Dialogue: 0,0:17:40.99,0:17:42.66,Main - Italics,Saki,0,0,0,,He said he wouldn't actually do it.
Dialogue: 0,0:17:43.48,0:17:49.23,Main - Italics,Saki,0,0,0,,He said that having an urge and \Nacting on it are two different things.
Dialogue: 0,0:17:51.42,0:17:54.08,Main - Italics,Saki,0,0,0,,I feel the exact same way.
Dialogue: 0,0:17:54.96,0:17:58.89,Main - Italics,Saki,0,0,0,,I'm starting to realize that each and every \Ntime Asamura-kun shares his thoughts,
Dialogue: 1,0:17:58.89,0:18:02.18,Main - Italics,Saki,0,0,0,,it's always something that \NI can relate to completely.
Dialogue: 0,0:18:00.05,0:18:04.97,Default,text,0,0,0,,{\fnArial\fs11\b1\shad0\an8\frz1.894\c&H142C23&\3c&H346057&\pos(116,195)}B\NI\NC\NY\NC\NL\NE\NS\N\NM\NU\NS\NT
Dialogue: 1,0:18:04.67,0:18:08.91,Main - Italics,Saki,0,0,0,,That's probably why this feels so right.
Dialogue: 0,0:18:20.27,0:18:22.99,Main - Italics,Saki,0,0,0,,Asamura-kun is dangerous.
Dialogue: 0,0:18:23.99,0:18:27.50,Main - Italics,Saki,0,0,0,,He understands me far too well.
Dialogue: 0,0:18:31.08,0:18:33.50,Default,Yuta,0,0,0,,Ayase-san, right now...
Dialogue: 0,0:18:34.06,0:18:36.97,Default,Yuta,0,0,0,,you're acting exactly like \Nthe kind of girl I hate.
Dialogue: 0,0:18:41.22,0:18:44.52,Default,Yuta,0,0,0,,Using your looks as a weapon to get ahead...
Dialogue: 0,0:18:44.52,0:18:46.59,Default,Yuta,0,0,0,,I thought you didn't want to be seen that way.
Dialogue: 0,0:18:47.91,0:18:52.02,Default,Yuta,0,0,0,,If you can't prove people wrong in a way \Nthat's not contingent on being a woman,
Dialogue: 0,0:18:52.02,0:18:53.29,Default,Yuta,0,0,0,,then what's the point?
Dialogue: 0,0:18:55.59,0:18:56.40,Default,Saki,0,0,0,,But...
Dialogue: 0,0:18:57.29,0:19:00.50,Default,Saki,0,0,0,,I felt like since you understand that much—
Dialogue: 0,0:19:00.50,0:19:02.33,Default,Yuta,0,0,0,,This isn't about me.
Dialogue: 0,0:19:04.04,0:19:04.66,Default,Yuta,0,0,0,,It's...
Dialogue: 0,0:19:07.13,0:19:08.98,Default,Yuta,0,0,0,,about you, Ayase-san.
Dialogue: 0,0:19:19.19,0:19:21.36,Default,Saki,0,0,0,,I'm sorry.
Dialogue: 0,0:19:23.93,0:19:27.68,Default,text,0,0,0,,{\fnGeorgia\fs18\shad0\bord0\fay-.11\c&H779398&\pos(417.667,161)}Asamura\h\h\h\h\h\h\h\h\h\hAyase
Dialogue: 0,0:19:27.11,0:19:31.66,Default,Saki,0,0,0,,I think my father used to be a really nice guy.
Dialogue: 0,0:19:32.29,0:19:36.39,Default,Saki,0,0,0,,But after his company failed, \Nhe stopped trusting people.
Dialogue: 0,0:19:37.61,0:19:42.81,Default,Saki,0,0,0,,My mom started working \Ntwice as hard to provide for me,
Dialogue: 0,0:19:43.66,0:19:47.80,Default,Saki,0,0,0,,but he didn't seem to like that, either.
Dialogue: 0,0:19:50.09,0:19:55.00,Default,Saki,0,0,0,,He'd say to her, \N"You're nothing but a sex worker."
Dialogue: 0,0:19:56.17,0:19:59.96,Default,Saki,0,0,0,,"You use your body to get customers." \NReally degrading stuff.
Dialogue: 0,0:20:01.30,0:20:04.22,Default,Saki,0,0,0,,I understand he was going through a lot,
Dialogue: 0,0:20:05.51,0:20:08.73,Default,Saki,0,0,0,,but that didn't make it okay \Nto take it out on my mom.
Dialogue: 0,0:20:14.18,0:20:15.97,Default,Yuta,0,0,0,,I completely agree.
Dialogue: 0,0:20:15.97,0:20:16.73,Default,Saki,0,0,0,,Huh?
Dialogue: 0,0:20:17.47,0:20:21.67,Default,Yuta,0,0,0,,Uh, well... that's kind of like \Nwhat happened to us, too.
Dialogue: 0,0:20:22.77,0:20:25.38,Default,Yuta,0,0,0,,After my mom cheated on my dad,
Dialogue: 0,0:20:25.38,0:20:28.87,Default,Yuta,0,0,0,,he became kind of paranoid \Nof women for a while.
Dialogue: 0,0:20:28.87,0:20:30.33,Default,Saki,0,0,0,,Your dad did?
Dialogue: 0,0:20:37.21,0:20:39.29,Default,Saki,0,0,0,,And maybe you did, too?
Dialogue: 0,0:20:45.75,0:20:48.26,Default,Saki,0,0,0,,You and I have a lot in common.
Dialogue: 0,0:20:50.06,0:20:52.08,Default,Saki,0,0,0,,Even the broken parts.
Dialogue: 0,0:20:54.84,0:20:56.17,Default,Yuta,0,0,0,,You might have a point.
Dialogue: 0,0:20:58.89,0:21:04.41,Default,Yuta,0,0,0,,But even with those parts, I have a feeling \Nwe'll be able to get along all right.
Dialogue: 0,0:21:08.22,0:21:10.15,Default,Yuta,0,0,0,,As brother and sister.
Dialogue: 0,0:21:17.85,0:21:23.05,Default,Yuta,0,0,0,,Oh, hey. I don't mind if you want \Nto start calling me "big brother."
Dialogue: 0,0:21:34.89,0:21:36.08,Default,Saki,0,0,0,,Thanks.
Dialogue: 0,0:21:38.83,0:21:39.81,Default,Yuta,0,0,0,,Ayase-san?
Dialogue: 0,0:21:44.10,0:21:47.45,Default,Saki,0,0,0,,Let's make this work together, Asamura-kun.
Dialogue: 0,0:21:48.95,0:21:50.45,Default,text,0,0,0,,{\fnGeorgia\shad0\bord0\fs12\3c&H444242&\c&HB8B5AC&\pos(379.5,212.091)\b1}Saturday, June 13th
Dialogue: 0,0:21:51.75,0:21:54.72,Main - Italics,Saki,0,0,0,,Saturday, June 13th.
Dialogue: 0,0:21:55.35,0:21:59.05,Main - Italics,Saki,0,0,0,,Asamura-kun and I ate dinner together. \NJust the two of us.
Dialogue: 0,0:21:59.61,0:22:04.21,Main - Italics,Saki,0,0,0,,We were able to convince our parents\Nto go out to eat for once.
Dialogue: 0,0:22:06.27,0:22:08.49,Main - Italics,Saki,0,0,0,,It was actually Asamura-kun's idea.
Dialogue: 0,0:22:09.21,0:22:13.22,Main - Italics,Saki,0,0,0,,He really is considerate of others.
Dialogue: 0,0:22:24.70,0:22:29.24,Main - Italics,Saki,0,0,0,,That's exactly why I shouldn't \Ncall him "big brother."
Dialogue: 0,0:22:31.07,0:22:33.12,Main - Italics,Saki,0,0,0,,The moment I start calling him that,
Dialogue: 0,0:22:33.95,0:22:38.23,Main - Italics,Saki,0,0,0,,I'll probably start depending \Non him for everything.
Dialogue: 0,0:22:38.23,0:22:40.12,Main - Italics,Saki,0,0,0,,I absolutely can't let myself do that.
Dialogue: 0,0:22:41.88,0:22:44.38,Default,text,0,0,0,,{\fnGeorgia\shad0\bord1\blur4\fs12\3c&HE2E0DF&\c&H6B684D&\pos(336,296.5)}I know it's strange to have toast with your rolled omelet with broth.
Dialogue: 0,0:22:42.33,0:22:44.07,Main - Top + Italics,Saki,0,0,0,,I'm sorry, Asamura-kun.
Dialogue: 0,0:22:44.63,0:22:46.38,Default,text,0,0,0,,{\fnGeorgia\shad0\bord1\blur4\fs12\3c&HE2E0DF&\c&H5A4D6B&\pos(336,296.5)}It isn't strange at all, Ayase-san.
Dialogue: 0,0:22:46.46,0:22:47.51,Default,text,0,0,0,,{\fnGeorgia\shad0\bord1\blur4\fs12\3c&HD0CFD2&\c&H7C7AAE&\pos(315,280.5)}I'm so glad.
Dialogue: 0,0:22:47.16,0:22:51.10,Main - Italics,Saki,0,0,0,,But every time I call him "Asamura-kun,"
Dialogue: 0,0:22:47.76,0:22:50.26,Default,text,0,0,0,,{\fnGeorgia\shad0\bord1\blur4\fs12\3c&HD0CFD2&\c&H696799&\pos(315,280.5)}That looks great, Yuta.
Dialogue: 0,0:22:50.26,0:22:51.39,Default,text,0,0,0,,{\fnGeorgia\shad0\bord1\blur4\fs12\3c&HD0CFD2&\c&H696799&\pos(315,280.5)}Mind sharing with your old man?
Dialogue: 0,0:22:51.43,0:22:53.37,Main - Italics,Saki,0,0,0,,I get this indescribable feeling in my chest.
Dialogue: 0,0:22:53.37,0:22:58.55,Main - Italics,Saki,0,0,0,,One very different from the way\N I'd feel calling him my brother.
Dialogue: 0,0:23:00.19,0:23:02.97,Main - Italics,Saki,0,0,0,,I've never felt this way before.
Dialogue: 0,0:23:03.50,0:23:07.18,Main - Italics,Saki,0,0,0,,I don't even know what to call it.
Dialogue: 0,0:23:07.95,0:23:11.32,Main - Italics,Saki,0,0,0,,Before I knew it, I started to \Nhave feelings for Asamura-kun.
Dialogue: 0,0:23:11.78,0:23:13.16,Main - Italics,Saki,0,0,0,,My mind is in a haze.
Dialogue: 0,0:23:13.89,0:23:17.37,Main - Italics,Saki,0,0,0,,Recently, even when I'm in bed, \NI have trouble falling asleep.
Dialogue: 0,0:23:17.91,0:23:23.76,Main - Italics,Saki,0,0,0,,And here I want live independently. \NI'm getting so sick of myself.
Dialogue: 0,0:23:24.58,0:23:27.44,Main - Italics,Saki,0,0,0,,What's happening to me?
Dialogue: 0,0:23:31.53,0:23:32.67,Main - Italics,Saki,0,0,0,,Seriously...
Dialogue: 0,0:23:46.48,0:23:49.99,Default,text,0,0,0,,{\fnGeorgia\fs10\shad0\bord0\c&H8FCEAB&\b1\pos(320,162.606)}Episode 4 {\fs14}"Tendenc{\c&H80B7E9&}ies {\c&H8FCEAB&}and Strategies




 */