// Adapted from https://github.com/qgustavor/mkv-extract/ licensed under MIT
import * as fs from "fs";
import got from "got";
import MatroskaType from "matroska-subtitles";
const Matroska = require("matroska-subtitles"); // For some reason typescript does new Matroska["default"]() instead of just new Matroska - and that fucks me. So regular "require" it is.

async function streamFromURL(url: string) {
	return got.stream(url);
}

type Track = {
	name: string;
	number: number;
	language: string;
	type: string;
	header?: string;
	data: string; // The final string formatted as ASS or SRT
	subtitles: {
		text: string;
		time: number;
		duration: number;
		style: string;
		layer: string;
		marginL: string;
		name: string;
		marginR: string;
		marginV: string;
		effect: string;
	}[];
};

class SubsExtractionTask {
	private fileStream: NodeJS.ReadableStream;
	private tracks: Record<number, Track>;
	private parser: MatroskaType;
	private publishData: (_: {
		name: string,
		data: string,
		title: string,
		tracks: Track[],
		subtitles: Track["subtitles"];
	}[]) => void;
	constructor(fileStream: NodeJS.ReadableStream, tracks: Record<number, Track>, parser: MatroskaType, atData: SubsExtractionTask["publishData"]) {
		this.fileStream = fileStream;
		this.tracks = tracks;
		this.parser = parser;
		this.publishData = atData;
	}
	start() {
		const { decoder } = createDecoderFromStream(this.fileStream, () => this.atData(), this.tracks, this.parser);
		this.parser = decoder;
	}
	async wait() {
		return await new Promise<void>(resolve => {
			if (this.parser.closed || this.parser.destroyed)
				resolve();
			else
				this.parser.once("finish", resolve);
		});
	}
	pause() {
		return this.fileStream.pause();
	}
	resume() {
		return this.fileStream.resume();
	}
	isPaused() {
		return this.fileStream.isPaused();
	}
	atData() {
		const files = this.normalizedData();
		this.publishData(files);
		return files;
	}
	normalizedData() {
		let files: {
			name: string,
			data: string,
			title: string,
			tracks: Track[],
			subtitles: Track["subtitles"];
		}[] = [];
		Object.values(this.tracks).forEach(track => {
			const heading = track.header ?? "";
			const isASS = heading.includes("Format:");
			const formatFn = isASS ? formatTimestamp : formatTimestampSRT;
			const eventMatches = isASS
				? (heading.match(/\[Events\]\s+Format:([^\r\n]*)/) ?? [""])
				: [""];
			const headingParts = isASS ? heading.split(eventMatches[0]) : ["", ""];
			const fixedLines: string[] = [];
			if (track.subtitles)
				track.subtitles.forEach((subtitle, i) => {
					let startTimeStamp = formatFn(subtitle.time),
						endTimeStamp = formatFn(subtitle.time + subtitle.duration),
						lineParts = [subtitle.layer, startTimeStamp, endTimeStamp, subtitle.style,
						subtitle.name, subtitle.marginL, subtitle.marginR, subtitle.marginV, subtitle.effect, subtitle.text],
						fixedLine = isASS ? "Dialogue: " + lineParts.join(",")
							: i + 1 + "\r\n" + startTimeStamp.replace(".", ",") +
							" --> " + endTimeStamp.replace(".", ",") + "\r\n" + subtitle.text + "\r\n";
					if (fixedLines[i]) {
						fixedLines[i] += "\r\n" + fixedLine;
					} else {
						fixedLines[i] = fixedLine;
					}
				});
			let data = (isASS ? headingParts[0] + eventMatches[0] + "\r\n" : "") + fixedLines.join("\r\n") + headingParts[1] + "\r\n";
			track.data = data;
			files.push({
				name: track.name,
				data,
				title: heading.split("\n")?.[1]?.substring(7) ?? "Unknown",
				tracks: Object.values(this.tracks),
				subtitles: track.subtitles
			});
		});
		return files;
	}
}

async function extractSubsFromVideoFile(file: string, atData: () => void) {
	let tracks: Record<number, Track>, parser: MatroskaType, fileStream: NodeJS.ReadableStream;
	if (file.startsWith('file://')) {
		file = file.substring(7);
		const stat = await fs.promises.stat(file);
		({ tracks, parser, fileStream } = await getTracksOfFile(
			file,
			fs.createReadStream.bind(this, file, { highWaterMark: (stat.blksize * stat.blocks) / 256 }),
		));
	}
	else
		({ tracks, parser, fileStream } = await getTracksOfFile(file, streamFromURL));
	const task = new SubsExtractionTask(fileStream, tracks, parser, atData);
	task.start();
	return task;
}

function getTracksOfFile(file: string, createStreamMethod: (file: string) => NodeJS.ReadableStream | Promise<NodeJS.ReadableStream>) {
	return new Promise<{ tracks: Record<number, Track>, parser: MatroskaType, fileStream: NodeJS.ReadableStream; }>(async resolve => {
		const stream = await createStreamMethod(file);
		const parser: MatroskaType = new Matroska();
		stream.pipe(parser);
		parser.once('tracks', async rawTracks => {
			const tracks = {};
			stream.unpipe(parser);
			for (const track of rawTracks) {
				tracks[track.number] = track;
				const isASS = track.header.includes("Format:"),
					extName = isASS ? ".ass" : ".srt";
				track.name = track.language ? track.language + extName : "Subtitle " + track.number + extName;
			}
			resolve({ tracks, parser, fileStream: await createStreamMethod(file) });
		});
	});
}

function createDecoderFromStream(stream: NodeJS.ReadableStream, atData: SubsExtractionTask["atData"], tracks: Record<number, Track>, prevDecoder: MatroskaType) {
	const matroskaDecoder = new Matroska(prevDecoder);
	let emitTimeout: NodeJS.Timeout,
		emitTimeoutStop = false,
		emitTimeoutMiliseconds = 5000,
		thereIsAnEmitWaiting = false;
	matroskaDecoder.on("subtitle", (subtitle, trackNumber) => {
		let track = tracks[trackNumber];
		if (!track.subtitles)
			track.subtitles = [];
		track.subtitles.push(subtitle);
		thereIsAnEmitWaiting = true;
		if (emitTimeoutStop) return;
		thereIsAnEmitWaiting = false;
		emitTimeoutStop = true;
		clearTimeout(emitTimeout);
		emitTimeout = setTimeout(() => {
			emitTimeoutStop = false;
			if (thereIsAnEmitWaiting)
				atData();
		}, emitTimeoutMiliseconds);
		atData();
	});
	stream.pipe(matroskaDecoder);
	return {
		decoder: matroskaDecoder,
		promise: new Promise(resolve => {
			matroskaDecoder.once("finish", () => { resolve(atData()); });
		})
	};
}

function formatTimestamp(timestamp: number) {
	const seconds = timestamp / 1000;
	const hh = Math.floor(seconds / 3600);
	let mm: string | number = Math.floor((seconds - hh * 3600) / 60);
	let ss: string = (seconds - hh * 3600 - mm * 60).toFixed(2);

	if (mm < 10) mm = `0${mm}`;
	if (parseFloat(ss) < 10) ss = `0${ss}`;

	return `${hh}:${mm}:${ss}`;
}

function formatTimestampSRT(timestamp: number) {
	const seconds = timestamp / 1000;
	let hh: string | number = Math.floor(seconds / 3600);
	let mm: string | number = Math.floor((seconds - hh * 3600) / 60);
	let ss: string = (seconds - hh * 3600 - mm * 60).toFixed(3);

	if (hh < 10) hh = `0${hh}`;
	if (mm < 10) mm = `0${mm}`;
	if (parseFloat(ss) < 10) ss = `0${ss}`;

	return `${hh}:${mm}:${ss}`;
}

module.exports = { extractSubsFromVideoFile };
