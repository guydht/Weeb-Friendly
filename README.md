# Weeb-Friendly
Desktop app for downloading/managing anime in torrents/myanimelist.

Currently supports:
* Multiple themes (just for shits & giggles) - default is dark.
* Logging in to MAL.
* Updating MAL user upon finishing episode.
* Downloading Anime from HorribleSubs / Erai-raws.
* Auto downloading new episodes of shows you watch.
* Auto updating episodes when you finish watching them (upon reaching 95% of video length).


Some random screenshots of the app:

![Screendhot1](readme-pictures/Screenshot from 2020-07-17 02-27-15.png)

![Screendhot2](readme-pictures/Screenshot from 2020-07-17 02-27-59.png)
